%% utf_shoebox_modes %%

% Autor: Luis Henrique Sant'Ana; baseada na rotina do prof. Dr. Eric
% Brandão

% inputs (obrigatórios)
% fmax: frequência até a qual deseja-se criar modos (Hz) (escalar numérico)
% L: dimensões da sala (metros) (vetor numérico de comprimento 3)
% inputs (pares)
% c: velocidade do som (m/s) (escalar numérico)
% histFlag: habilita plots se true (escalar lógico)

function [f_table,N,od] = utf_shoebox_modes(fmax,L,varargin)
%% parse
defaultC = 343;
defaultHistFlag = false;

islength3 = @(x) isvector(x) && isnumeric(x) && isequal(length(x),3)...
              && all(x>0);
isposNumScalar = @(x) isnumeric(x) && isscalar(x) && x>0;
p = inputParser;
addRequired(p,'fmax',isposNumScalar);
addRequired(p,'L',islength3);
addParameter(p,'c',defaultC,isposNumScalar);
addParameter(p,'histFlag',defaultHistFlag,@(x) islogical(x) && isscalar(x));
parse(p,fmax,L,varargin{:})

c = p.Results.c;
histFlag = p.Results.histFlag;
clearvars p
%% setup
L = sort(L,'descend');
Lx = L(1); Ly = L(2); Lz = L(3);
V = Lx*Ly*Lz; % room volume
% highest order of modes
% prof. Eric:
% od = 2 * ceil(fmax * V / c * ...
%               sqrt(1 / ((Lx*Ly)^2 + (Lx*Lz)^2 + (Ly*Lz)^2) ) );
% Luis:
% da equação f_lmn = c/2 * sqrt((1/Lx)^2 + (1/Ly)^2 + (1/Lz)^2);
% a maior ordem para atingir a frequência será quando atingida por um modo
% axial da maior dimensão
od = ceil(2 * fmax * Lx / c);

%% modes calculation
K = c/2; % constant
f = nan((od+1)^3,1); % aloca vetor para frequências
f_table = nan(length(f),3); % aloca matriz que receberá ordens dos modos

n = 1; % inicializa índice de frequências
for nx = 0 : od
    for ny = 0 : od
        for nz = 0 : od
            f(n) = K*sqrt((nx/Lx)^2 + (ny/Ly)^2 + (nz/Lz)^2);
            f_table(n,:)=[nx ny nz];
            n = n + 1;
        end
    end
end

% método matricial (ainda tem que verificar os índices, daí)
% nx = ones([od+1 1 1]);
% ny = ones([1 od+1 1]);
% nz = ones([1 1 od+1]);
% nx(:,1,1) = (0:od);
% ny(1,:,1) = (0:od);
% nz(1,1,:) = (0:od);
% f = c/2 * sqrt((nx/Lx).^2 + (ny/Ly).^2 + (nz/Lz).^2);

% Modes amplitudes
A = ones([length(f) 1]) * sqrt(2/8);  % amplitude dos modos oblíquos
A(f_table(:,2)==0 & f_table(:,3)==0) = sqrt(2/2); % amplitude dos modos axiais em x
A(f_table(:,1)==0 & f_table(:,3)==0) = sqrt(2/2); % amplitude dos modos axiais em y
A(f_table(:,1)==0 & f_table(:,2)==0) = sqrt(2/2); % amplitude dos modos axiais em z
A(f_table(:,1)~=0 & f_table(:,2)~=0 & f_table(:,3)==0) = sqrt(2/4); % modos xy
A(f_table(:,1)~=0 & f_table(:,2)==0 & f_table(:,3)~=0) = sqrt(2/4); % modos xz
A(f_table(:,1)==0 & f_table(:,2)~=0 & f_table(:,3)~=0) = sqrt(2/4); % modos yz

% cria tabela, de fato, com frequências e ordens
f_table = array2table([f(2:end) A(2:end) f_table(2:end,:)],...
                      'VariableNames',["frequency" "energy" "nx" "ny" "nz"]);
f_table = sortrows(f_table,'frequency','ascend'); % ordena por frequência

% o espectro está bem preenchido com modos axiais, tangenciais e oblíquos
% até a frequência fmax.
% Porém, no processo, são gerados modos "colaterais", muito acima de fmax:
if(histFlag)
    histogram(f_table{:,1},length(unique(f_table{:,1})));
end
% eles são removidos
toDelete = f_table.frequency > fmax;
f_table(toDelete,:) = [];

%% previsão da quantidade de modos
S = 2*Lx*Ly + 2*Lx*Lz + 2*Ly*Lz;
P = 4*(Lx+Ly+Lz);
N = round((fmax/c)^3*4*pi*V/3) + round((fmax/c)^2*pi*S/4) + round((fmax/c)*P/8);
disp("Percentage deviated from prediction: " + (height(f_table)/N - 1)*100);
end