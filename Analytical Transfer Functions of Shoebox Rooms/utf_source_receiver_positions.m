%% utf_source_receiver_positions %%

% Gera nMics posições de microfone para cada uma das nSources posições de
% fonte sonora para uma sala retangular de dimnesões L.

% Luis H. Sant'Ana

function [x0,xr] = utf_source_receiver_positions(L,T60,nSources,nMics,varargin)
%% parse
if(~isequal(size(L),[3 1]))
    L = L.';
    if(~isequal(size(L),[3 1]))
        error("Verify L dimensions");
    end
end
if(~(L(1)>=L(2) && L(2)>=L(3)))
    error("L dimensions must be in descending order."); % easier for plots
    % and to not cause confusion
end

defaultC = 343;

isPosNumericScalar = @(x) isnumeric(x) && isscalar(x) && all(x>0);
p = inputParser;
addRequired(p,'L',@(x) isnumeric(x) && all(x>0));
addRequired(p,'T60',@(x) isnumeric(x) && isvector(x) && all(x>0));
addRequired(p,'nSources',isPosNumericScalar);
addRequired(p,'nMics',isPosNumericScalar);
addOptional(p,'c',defaultC,isPosNumericScalar)
parse(p,L,T60,nSources,nMics,varargin{:});

c = p.Results.c;
%% Distâncias
% Fator de escala e ajustes dos limites de acordo com a ISO 354
N = (200 / prod(L))^(1/3); % fator de escala
inMicD = 1.5 / N; % distância entre microfones
surfMicD = 1 / N; % distância entre microfones e paredes
sourceMicD = 2 / N; % distância entre microfone e fonte sonora
inSourceD = 3 / N; % distância entre fontes
% distância crítica
r_c = utf_critical_distance(T60,L,c);
if(r_c > surfMicD || r_c > sourceMicD)
    disp("Utilizando distância crítica!");
    surfMicD = r_c;
    sourceMicD = r_c;
end
%% posições de fontes sonoras
% a primeira posição é próxima à origem:
x0 = repmat(0.1, [3 1]);
% as demais são calculadas de modo a ficarem a 1 metro de distância uma da
% outra
% dimensões efetivas
minimum = 0;
maximum = L;
for j = 2 : nSources
    flag = true;
    while(flag)
        % blindly generate random position
        x0(:,j) = minimum + (maximum-minimum) .* rand(3,1);
        % verify it's at least inSourceD meters away from other sources
        if(all(sqrt( sum( (x0(:,j) - x0(:,1:j-1)).^2 ,1 ) ) > inSourceD))
            flag = false;
        end
    end
end

% deslocando um percentual referente a cada dimensão
% p = ceil(sqrt(1/sum(L.^2))*100)/100;
% x0 = [x0 p*L+x0(:,1)];
% acrésicmo de 30% na porcentagem para evitar excitar modos semelhantes
% x0 = [x0 p*1.3*L+x0(:,2)];
%% posições de microfones

% dimensões efetivas
minimum = surfMicD;
maximum = L-surfMicD;
xr = nan([3 nMics*nSources]);
for k = 1 : nSources
for j = 1 : nMics
    idx = nMics*k-nMics+j;
    flag = true;
    while(flag)
        % blindly generate random position
        xr(:,idx) = minimum + (maximum-minimum) .* rand(3,1);
        % verify it's at least sourceMicD meters away from the respective source
        if(all(sqrt( sum( (xr(:,idx) - x0(:,k)).^2 ,1 ) ) > sourceMicD))
            % verify it's at least inMicD meters away from other mics
            if(j>=2)
                if(all(sqrt( sum( (xr(:,idx) - xr(:,idx-j+1:idx-1)).^2 ,1 ) ) > inMicD))
                    flag = false;
                end
            else
                flag = false;
            end
        end
    end
end
end
end