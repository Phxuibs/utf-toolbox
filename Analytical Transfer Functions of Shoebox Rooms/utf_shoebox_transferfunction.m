%%%% utf_shoebox_transferfunction v2 %%
%
% Retorna uma função de transferência para uma sala retangular que possui
% tempos de reverberação semelhantes para toda a faixa de frequências.
%
% v2: aceita mais posições de fonte-receptor
%
%
% inputs (obrigatórios):
% x0: posição da fonte sonora (metros) (array numérico com 3 linhas)
% xr: posição do receptor (metros) (array numérico com 3 linhas)
% fmax: frequência até a qual deseja-se criar modos (Hz) (escalar numérico)
% L: dimensões da sala (metros) (vetor numérico de comprimento 3)
% T60: tempo de reverberação para todas as frequências (segundos) (escalar
% numérico)
% inputs (pares)
% time_length: comprimento mínimo do que seria resposta ao impulso (s);
% Importante para calcular a ordem de FFT: n = ceil(log2(time_length*fs)).
% Padrão é time_length = T60 (escalar numérico)
% c: velocidade do som (m/s) (escalar numérico)
% rho: densidade do ar (kg/m^3) (escalar numérico)
% plotsFlag: habilita plots se true (escalar lógico)
% parPoolFlag: habilita uso parallel pool se true (escalar lógico)
%
%
% Autor: Luis H. Sant'Ana; baseada na rotina do prof. Dr. Eric Brandão

function [TF] = utf_shoebox_transferfunction(x0,xr,fmax,L,T60,varargin)
%% parse
if(~isequal(size(L),[3 1]))
    L = L.';
    if(~isequal(size(L),[3 1]))
        error("Verify L dimensions");
    end
end
if(~(L(1)>=L(2) && L(2)>=L(3)))
    error("L dimensions must be in descending order. "+...
          "Please verify x0 and xr sorting accordingly.");
end
if(~(all(all(x0<L,2)) && all(all(xr<L,2))))
    error("Verify source and receiver positions, as well room dimensions!");
end
if(~isequal(size(x0),size(xr)))
    error("Arguments x0 and xr must have the same size.");
end

defaultTimeLength = T60;
defaultC = utf_speed_of_sound();
defaultRho = utf_air_density();
defaultPlotsFlag = false;
defaultParPoolFlag = false;

has3rows = @(x) isnumeric(x) && isequal(size(x,1),3) && all(x>0,'all');
isposNumScalar = @(x) isnumeric(x) && isscalar(x) && x>0;
p = inputParser;
addRequired(p,'x0',has3rows);
addRequired(p,'xr',has3rows);
addRequired(p,'fmax',isposNumScalar);
addRequired(p,'L',has3rows);
addRequired(p,'T60',isposNumScalar);
addParameter(p,'timeLength',defaultTimeLength,isposNumScalar);
addParameter(p,'c',defaultC,isposNumScalar);
addParameter(p,'rho',defaultRho,isposNumScalar);
addParameter(p,'plotsFlag',defaultPlotsFlag,@(x)islogical(x)&&isscalar(x));
addParameter(p,'parPoolFlag',defaultParPoolFlag,@(x)islogical(x)&&isscalar(x));
parse(p,x0,xr,fmax,L,T60,varargin{:});

timeLength = p.Results.timeLength;
c = p.Results.c;
rho = p.Results.rho;
plotsFlag = p.Results.plotsFlag;
parPoolFlag = p.Results.parPoolFlag;
clearvars p
%% Calculate modes
% para calcular a resposta até fmax, utiliza-se modos até, pelo menos,
% 2*fmax
[f_table,N] = utf_shoebox_modes(2*fmax,L,'c',c,'histFlag',plotsFlag);
%% setup for TF calculation
V = prod(L); % room volume
fs = 2 * fmax; % sampling frequency
% ordem da NFFT
n = ceil(log2(timeLength*fs)) + 1; % + 1 para ficar como o ITA, apenas
% corta na metade depois (deconvolução linear)
NFFT = 2^n; % comprimento do sinal (número de amostras) no tempo
f = transpose((0:NFFT/2)*fs/NFFT); % frequency vector
k = 2 * pi * f / c; % wavenumber
kn = 2 * pi * f_table{:,1} / c; % modes wavenumber
eta = (3*log(10)) / T60; % damping
nPos = size(x0,2);
%% TF calculation
psi = @(x,f_table,L) f_table{:,2} .* cos(f_table{:,3}*pi*x(1)/L(1)) .* ...
                                     cos(f_table{:,4}*pi*x(2)/L(2)) .* ...
                                     cos(f_table{:,5}*pi*x(3)/L(3));
psi_0 = nan([nPos height(f_table)]);
psi_r = psi_0;
for j = 1 : nPos
    psi_0(j,:) = psi(x0(:,j),f_table,L).';
    psi_r(j,:) = psi(xr(:,j),f_table,L).';
end
% método loop (lento, mas reduz uso de memória)
psi_mult = psi_0 .* psi_r; % adianta a multiplicação, pra não ser feita a
% cada iteração
% igualmente com a constante e uma parte do denominador
K = 1j*2*eta/c;
denominador_1 = k.^2 - K*k;
kn_2 = transpose(kn).^2; % e com kn^2
soma = nan(nPos,length(k)); % aloca vetor da soma
if(~parPoolFlag)
    % wBar = waitbar(0,"Realizando somatório...");
    for i = 1 : length(k)
        % waitbar(i/length(k),wBar,"Realizando somatório...");
        soma(:,i) = sum( psi_mult ./ ( denominador_1(i) - kn_2 ) ,2 );
    end
    % close(wBar);
else
    parfor i = 1 : length(k)
        soma(:,i) = sum( psi_mult ./ ( denominador_1(i) - kn_2 ) ,2 );
    end
end
% função de Green escrita na foram de frequências (f):
% (c/(2*pi))^2 * sum( psi_mult ./ ( f*( f - 1j*eta/pi ) - f_n^2 ) )

Q0 = 1; % velocidade de volume da fonte sonora
P = (1j*2*pi*f*rho*Q0/V) .* transpose(soma);
%% ajuste de resíduos
RI = ifft(P,NFFT,'symmetric'); % resposta ao impulso;
% 'symmetric' compensa a energia das frequências não calculadas
RI = RI(1:end/2,:); % corta na metade, eliminando resíduos
new_NFFT = size(RI,1);
timeLength = new_NFFT/fs; % comprimento do sinal, em segundos
t = transpose((0:new_NFFT-1)*1/fs); % cria vetor de tempo
P = fft(RI); % atualiza função de transferência
P = P(1:end/2+1,:);
f = transpose((0:new_NFFT/2)*fs/new_NFFT); % e vetor de frequência
%% preparar saída
% retorna todos os valores de entrada
TF.x0 = x0;
TF.xr = xr;
TF.fmax = fmax;
TF.L = L;
TF.T60 = T60;
TF.time_length = timeLength;
TF.c = c;
TF.rho = rho;
%  e os produzidos
TF.P = P;
TF.f = f;
TF.RI = RI;
TF.t = t;
TF.fs = fs;
TF.eta = eta;
TF.f_table = f_table;
TF.N = N;
TF.n = n;
TF.beforeCutNFFT = NFFT;
TF.nPos = nPos;
end