%% utf_s_base_limits %%
%
% Returns the limits established by ISO 17497-1 for the baseplate
% scattering
%
% output:
% s_base_limits: the limits themselves (numeric vector)
% f: the corresponding frequencies (numeric vector)
%
% Author: Luis H. Sant'Ana
%
function [s_base_limits, f] = utf_s_base_limits()
s_base_limits = [0.05 0.05 0.05 0.05 0.05 0.05 0.05 0.05 0.1 0.1 0.1...
                 0.15 0.15 0.15 0.2 0.2 0.2 0.25];
f = utf_center_third_octave_bands([100 5000]);
end