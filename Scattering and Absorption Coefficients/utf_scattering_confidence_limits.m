%% utf_scattering_confidence_limits %%

% Calcula qual a frequência mínima ou número mínimo de posições para
% desejada incerteza máxima

% Autor: Luis Henrique Sant'Ana

% input:
% M_f = número de posições fonte-receptor ou frequência; se inserido como
% número de posições, o resultado é uma frequência mínima; se inserido como
% frequênca, o resultado é um número de posições mínimo. Pode ser escalar
% ou array. [.]/[Hz]
% T = tempo de reverberação da câmara vazia [s]
% s = coeficiente de espalhamento de incidência aleatória [.]
% alpha_s = coeficeinte de absorção de incidência aleatória [.]
% u_T_factor = fator do desvio padrão do tempo de reverberação (i.e.
% desvio padrão do tempo de reverberação multiplicado por \sqrt(f_m/T); em
% outras palavras, 2.334 da Eq.13 do artigo [1]).
% u_max = incerteza máxima aceitável
% c = velocidade do som [m/s]
% V = volume [m^3]
% S = área da amostra [m^2]
% f = frequências de análise: semelhante a 'M_f', mas deve ser usado
% conjuntamente com 'M_f', este no caso deve ser o número de posições, para
% então retornar a incerteza no segundo termo do tuplet. Usar vetor vazio
% ([]) caso não desejado ou se utilizar 'M_f' como frequência

% [1] Müller-Trapet, M; Vorländer, M. Uncertainty analysis of standardized
% measurements of random-incidence absorption and scattering coefficients,
% Journal of the Acoustical Society of America, 137 (1), 63--74, 2015.

% obs.: será utilizado 1.96 como fator de abrangência, isto é, 95% de
% confiabilidade (assim como no artigo [1]) declarando a distribuição como
% normal. Pode ser avaliado se não deve ser utilizado o grau de liberdade
% nu=M-1, ou então utilização de outros fatores de abrangência para
% diferentes confiabilidades.

function [M_f_min,u] = utf_scattering_confidence_limits(M_f,T,s,alpha_s,...
                                                  u_T_factor,u_max,c,V,S,f)
% cálculo do termo K
K = 24*log(10)*V/c/S;
% "termo dentro da raiz"
termo_raiz = (1-s).^2 .* ( 1./T.^3+(1./T+alpha_s/K).^3 ) + 1./T.^3 + ...
             (1./T+(alpha_s+s.*(1-alpha_s))/K).^3 - ...
             2*(1-s) .* ( 1./T.^3 + (1-s).*(1-alpha_s) .* ...
             sqrt((1./T+alpha_s/K).^3 .* ...
                  (1./T+(alpha_s+s.*(1-alpha_s))/K).^3) );
% frequência mínima, ou número mínimo para posições de fonte-receptor, para
% incerteza máxima desejada
M_f_min = 1./M_f .* (1.96*u_T_factor*K./abs(1-alpha_s)/u_max).^2 .* ...
          termo_raiz;
% incerteza
if(~isempty(f))
    u = 1./sqrt(M_f.*f) * 1.96*u_T_factor*K./abs(1-alpha_s) .*...
        sqrt(termo_raiz);
end
end