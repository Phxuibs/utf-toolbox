%% utf_N_passos %%
%
% Calcula a quantidade N de passos necessários para determinados valores de
% s, alpha_s, A_1/S, R (decaimento), Delta_s (erro estimado).
%
% input:
% s = coeficiente de espalhamento (.) (numeric vector)
% alpha_s = coeficiente de absorção (.) (numeric vector)
% A_1_S = relação entre área de absorção equivalente e área da amostra
% (A_1 / S) (.) (numeric vector)
% inputs opcionais (name-value pairs):
% Delta_s = erro no coeficiente de espalhamento (numeric scalar;
% Delta_s < 0; default: -0.05) (.)
% R = valor de decaimento a ser utilizado (numeric scalar; |R|>=10;
% default: 20) (dB)
% output:
% N_s = número de passos/medições de acordo com a equação de Sant'Ana
% N_sl = número de passos/medições de acordo com a equação de Sakuma e Lee
% ([1], Equação 19)
%
% [1] SAKUMA, T.; LEE, H. Validation of Sample Rotation Scheme in the 
% measurement of Random-Incidence Scattering Coefficients. Acta Acustica
% united with Acustica. Vol. 99, 737--750, 2013. DOI: 10.3813/AAA.918652
%
% Autor: Luis Henrique Sant'Ana
%
function [N_s,N_sl] = utf_N_passos(s,alpha_s,A_1_S,varargin)
%% parse arguments
defaultDelta_s = -0.05;
defaultR = 20;
%
p = inputParser;
addRequired(p,'s',@(x) isnumeric(x) && isvector(x));
addRequired(p,'alpha_s',@(x) isnumeric(x) && isvector(x));
addRequired(p,'A_1_S',@(x) isnumeric(x) && isvector(x));
addOptional(p,'Delta_s',defaultDelta_s,@(x) isnumeric(x) && isscalar(x) && x < 0);
addOptional(p,'R',defaultR,@(x) isnumeric(x) && isscalar(x) && abs(x) >= 10);
parse(p,s,alpha_s,A_1_S,varargin{:});
%
Delta_s = p.Results.Delta_s;
R = p.Results.R;
%% cálculos
alpha_spec = s.*(1-alpha_s) + alpha_s;
mu = (alpha_s + A_1_S) ./ (alpha_spec + A_1_S);
nu = (1 - alpha_s) ./ (alpha_spec + A_1_S);
% N (Sakuma e Lee)
N_sl = ( 10.^( ( mu.*(1-nu*Delta_s)-1 ) * R/10 ) - 1 ) ./...
       ( mu.*(1-10.^(-nu*Delta_s*R/10) ) ) + 1;
% N (Sant'Ana)
N_s =  ( 10.^( ( mu.*(1-1./(1+(Delta_s*nu).^(-1))) -1 ) * R/10 ) - 1) ./...
       ( mu.*(1-10.^(-R./ (10*(1+(Delta_s*nu).^(-1)) ) ) ) ) + 1;
end