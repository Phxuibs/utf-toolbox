%% utf_u_T_factor %%

% Calcula o fator independente de frequência e tempo de reverberação para a
% estimativa de desvio padrão espacial do tempo de reverberação; o termo
% então é dependente da banda, base e ordem do filtro (Butterworth), e da 
% faixa de decaimento D utilizada para análise do tempo de reverberação, ou
% seja, é equivalente ao termo '2,334' na Eq. 13 do artigo [1]. A rotina é
% baseada, entre outros artigos de Davy, em [1]. 

% Autor: Luis Henrique Sant'Ana

% input:
% n = ordem do filtro (o padrão do ITA é n=10; Müller-Trapet e Vorländer
% [2] usam n=5).
% base = 10 ou '10' ou 'norma' ou 'standard' para filtro base 10; 2 ou '2'
% ou 'paper' ou 'ITA' ou 'ita' para filtro base 2.
% D = faixa de decaimento analisada (i.e. D=15 para T_15).
% b = denominador do designador de banda (i.e. para filtros 1/3 oitava,
% b=3).

% [1] Müller-Trapet, M; Vorländer, M. Uncertainty analysis of standardized
% measurements of random-incidence absorption and scattering coefficients,
% Journal of the Acoustical Society of America, 137 (1), 63--74, 2015.
% [2] J. L. Davy. The Variance of Decay Rates at Low Frequencies, Applied
% Acoustics, 23, p. 63--79, 1988.

function [u_T_factor] = utf_u_T_factor(n,base,D,b)

% Largura de banda
if(isequal(base,10) || isequal(base,'10') || isequal(base,'norma') ||...
   isequal(base,'standard'))
    G = 10^(3/10);
    B = G^(0.5/b) - G^(-0.5/b);
else
    if(isequal(base,2) || isequal(base,'2') || isequal(base,'paper') || ...
       isequal(base,'ITA') || isequal(base,'ita'))
        B = 2^(0.5/b) - 2^(-0.5/b);
    end
end

% Largura de banda esatística
B_s = pi / ( (2*n-1) * sin(pi/(2*n)) ) * B;

% Entrada da função F
D = abs(D); % caso D tenha sido colocado como -15, por exemplo, para T_15
x = D*log(10)/10;
% Função F
F = 1 - 3 * (1+exp(-x))/x - 12 * exp(-x) / x^2 + 12 * (1-exp(-x)) / x^3;

% fator da incerteza do tempo de reverberação
u_T_factor = 10/D^(1.5)/log(10) * sqrt( 720 * F / B_s );
end