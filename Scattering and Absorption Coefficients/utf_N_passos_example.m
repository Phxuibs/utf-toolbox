%% utf_N_passos_example %%

% O objetivo é recriar a Tabela III do artigo de Sakuma e Lee [1] através
% da função utf_N_passos.

% [1] SAKUMA, T.; LEE, H. Validation of Sample Rotation Scheme in the 
% measurement of Random-Incidence Scattering Coefficients. Acta Acustica
% united with Acustica. Vol. 99, 737--750, 2013. DOI: 10.3813/AAA.918652

% Autor: Luis Henrique Sant'Ana

%% valores de entrada
% colunas
alpha_s = [0 0.2 0.5];
% linhas
A_1_S = [0.5 1 2]';
% níveis (ou páginas)
s = [0.2 0.5 1];
s = reshape(s,1,1,numel(s));
%% cálculo
% calcula o número de passos
[~,N_sl] = N_passos(s,alpha_s,A_1_S);
% arredonda o número de passos
N_sl = round(N_sl);