%% utf_coef %%
%
% Calcula coeficientes energéticos (absorção e espalhamento)
%
% Sintax: [coef] = utf_coef(T,s_T,c,S,M,V)
%
% input:
% T: valor médio de T_60 por superfícies (já compensada a atenuação do ar);
% etapas de medição na primeira dimensão e frequências na segunda (s)
% (array numérico)
% s_T: desvio padrão de T (s) (array numérico)
% c: velocidade do som média para cada etapa (m/s) (vetor numérico)
% S: área da amostra (m^2) (escalar numérico)
% M: quantidade de posições (escalar numérico)
% V: volume da câmara (m^3) (escalar numérico)
%
% output; struct coef com:
% A_1: área de aborção equivalente de câmara vazia (m^2)
% alpha_s: coeficiente de absorção
% alpha_spec: coeficiente de absorção especular aparente
% s_base: coeficiente de espalhamento da base
% s: coeficiente de espalhamento
% retorna também todas as incertezas combinadas u_c_[nome do coeficiente]
%
% Autor: Luis Henrique Sant'Ana

function [coef] = utf_coef(T,s_T,c,S,M,V)
%% parseamento

% quantidade de etapas
E = size(T,1);
if(~isequal(E,2) && ~isequal(E,4))
    error("A quantidade de etapas deve ser 2 (coeficiente de absorção) "+...
          "ou 4 (coeficiente de espalhamento)");
end

if(~isequal(size(T),size(s_T)))
    error("Tamanhos de 'T' e 's_T' devem ser semelhantes!");
end

isPosNumScalar = @(x) isnumeric(x) && isscalar(x) && all((x>0));
p = inputParser;
addRequired(p,'T',@(x) isnumeric(x));
addRequired(p,'s_T',@(x) isnumeric(x));
addRequired(p,'c',@(x) isnumeric(x) && isvector(x) && isequal(length(x),E) && all(x>0));
addRequired(p,'S',isPosNumScalar);
addRequired(p,'M',@(x) isnumeric(x) && isvector(x));
addRequired(p,'V',isPosNumScalar);
parse(p,T,s_T,c,S,M,V);
%% cálculo dos coeficientes

% constante K (aqui, K não é igual ao de Trapet e Vorländer)
K = 24 * log(10) * V;
% função para cálculo de absorção
A = @(T,c) K ./ (c * T);

A_1 = A(T(1,:),c(1)); % área absorção equivalente para câmara vazia
alpha_s = ( A(T(2,:),c(2)) - A(T(1,:),c(1)) ) / S;
if(isequal(E,4))
    alpha_spec = ( A(T(4,:),c(4)) - A(T(3,:),c(3)) ) / S;
    s_base = ( A(T(3,:),c(3)) - A(T(1,:),c(1)) ) / S;
    % as for experiments with equivalent aborption areas of samples,
    % alpha_s may be negative.
    alpha_s_for_s = alpha_s;
    alpha_s_for_s(alpha_s<0) = 0;
    s = 1 - (1-alpha_spec)./(1-alpha_s_for_s);
end

%% incertezas padrão (ou incertezas combinadas)

% função para cálculo de incerteza relativa ao quadrado do valor médio
rel = @(s_T,T,i) s_T(i,:) ./ T(i,:).^2;

u_A_1 = K./(c(1)*T(1,:).^2) .* s_T(1,:)./sqrt(M);

u_c_alpha_s = 1./sqrt(M) .* K/S .* ...
              ( rel(s_T,T,1).^2/c(1)^2 + rel(s_T,T,2).^2/c(2)^2 ).^(1/2);

if(isequal(E,4))
    u_c_alpha_spec = 1./sqrt(M) .* K/S .* ...
                  ( rel(s_T,T,3).^2/c(3)^2 + rel(s_T,T,4).^2/c(4)^2 ).^(1/2);
    
    u_c_s_base = 1./sqrt(M) .* K/S .* ...
                  ( rel(s_T,T,1).^2/c(1)^2 + rel(s_T,T,3).^2/c(3)^2 -...
                2 * rel(s_T,T,1)/c(1) .* rel(s_T,T,3)/c(3) ).^(1/2);
    
    u_c_s = 1./sqrt(M) .* K/S .* 1./abs(1-alpha_s) .* (...
            (1-s).^2 .* ( rel(s_T,T,1).^2/c(1)^2 + rel(s_T,T,2).^2/c(2)^2 ) + ...
                        ( rel(s_T,T,3).^2/c(3)^2 + rel(s_T,T,4).^2/c(4)^2 ) - ...
            (1-s)*2 .*  ( rel(s_T,T,1)/c(1) .* rel(s_T,T,3)/c(3) + ...
        (1-alpha_spec) .* rel(s_T,T,2)/c(2) .* rel(s_T,T,4)/c(4) )).^(1/2);
end
%% prepara struct de saída
coef.A_1 = A_1;
coef.u_A_1 = u_A_1;
coef.alpha_s = alpha_s;
coef.u_c_alpha_s = u_c_alpha_s;
if(isequal(E,4))
    coef.alpha_spec = alpha_spec;
    coef.u_c_alpha_spec = u_c_alpha_spec;
    coef.s_base = s_base;
    coef.u_c_s_base = u_c_s_base;
    coef.s = s;
    coef.u_c_s = u_c_s;
end