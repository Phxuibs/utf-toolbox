%% utf_61260_figure_1 %%

% figura 1 da norma 61260

% Autor: Luis Henrique Sant'Ana

% input:
% Delta_b: intervalos positivos, podendo conter 0, de deslocamentos
% relativos de larguras de bandas
% A: limites de atenução relativa

% output:
% h1: gráfico 1 (-1 a 1 largura de banda)
% h2: gráfico 2 (-Delta_b(end) até Delta_b(end) larguras de banda).

function [h1,h2] = utf_61260_figure_1(Delta_b,A)

% ajustar para pltos
A_axis = [flip(A); A(2:end,:)];
b_axis = [flip(Delta_b)*-1 Delta_b(2:end)];

% primeiro gráfico
h1 = figure(1);
hold on
for j = 1 : size(A_axis,2)
    plot(b_axis,A_axis(:,j),'-','LineWidth',2,'Color',utf_color("azul"));
end
hold off
% ajustes gerais
ax = gca;
ax.FontSize = 22;
ax.YDir = 'reverse';
grid on
xlabel("$\Delta b$",'interpreter','latex');
ylabel("$\Delta A\ [dB]$",'interpreter','latex');
% ajustes específicos
ylim([-2 10]);
xlim([-1 1]);
ax.XTick = unique(b_axis);
ticklabel = unique(b_axis);
ticklabel(2:2:end-1) = nan;
ax.XTickLabel = string(sym(ticklabel));
% segundo gráfico
h2 = figure(2);
hold on
for j = 1 : size(A_axis,2)
    plot(b_axis,A_axis(:,j),'-','LineWidth',2,'Color',utf_color("azul"));
end
hold off
% ajustes gerais
ax = gca;
ax.FontSize = 24;
ax.YDir = 'reverse';
grid on
xlabel("$\Delta b$",'interpreter','latex');
ylabel("$\Delta A\ [dB]$",'interpreter','latex');
% ajsutes específicos
ylim([-10 80]);
ax.YTick = -10:10:80;
ax.YTickLabel = ax.YTick;
ax.XTick = b_axis(1):b_axis(end);
ax.LineWidth = 1;
ax.GridColor = repmat(0.5,[1 3]);
ax.GridAlpha = 0.5;
end