%% utf_octave_frequency_ratio %%

% simplesmente retorna o valor da relação de oitava

% Autor: Luis Henrique Sant'Ana

% input:
% base: base do filtro, podendo ser 2 ou 10 (10 é recomendado, mas ITA e,
% aparentemente, matlab, usam 2). Para mais detalhes, ver norma IEC
% 61260-1:2014.

% output:
% G: relação de oitava

function [G] = utf_octave_frequency_ratio(base)
    if(isequal(base,10))
        G = 10^(3/10);
    else
        if(isequal(base,2))
            G = 2;
        end
    end
end