%% utf_filter_mid_frequency %%

% calula a frequência central de banda (exact mid-frequency)

% Autor: Luis Henrique Sant'Ana

% input:
% x: valor inteiro de deslocamentos de banda em relação à frequência de
% referência
% b: denominador do designador de banda fracional
% G: relação de oitava (função utf_octave_frequency_ratio(.))

% output:
% fm: frequência central de banda

function [fm] = utf_filter_mid_frequency(x,b,G)

    fr = 1000;
    fm = fr*G.^(x/b);
    
end