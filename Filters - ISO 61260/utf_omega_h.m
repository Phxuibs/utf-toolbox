%% utf_omega_h %%

% cálculo de Omega_h (frequência relativa)

% Autor: Luis Henrique Sant'Ana

% input:
% Delta_b: intervalos de largura de banda
% b: denominador do designador de bandas fracionais
% G: relação de frequência de oitava (função utf_octave_frequency_ratio(.))

% output:
% Omega_h: frequências normalizadas acima da frequência central de banda
% Omega: frequências normalizadas acima e abaixo da frequência central de
% banda

function [Omega_h,Omega] = utf_omega_h(Delta_b,b,G)

Omega_h = 1 + ( ( G^(1/(2*b)) -1 ) / ( G^(1/2) -1 ) ) * (G.^(Delta_b) - 1);

% todas as frequências normalizadas de interesse
Omega = [flip(1./Omega_h) Omega_h(2:end)];

% frequências absolutas
% f = Omega * fm;

end