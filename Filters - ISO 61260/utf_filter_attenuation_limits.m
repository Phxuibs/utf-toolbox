%% utf_attenuation_limits %%

% cálculo de limites de atenuação relativa com interpolação

% Autor: Luis Henrique Sant'Ana

% input:
% Delta_b: larguras de banda deslocadas da frequência central
% (utilizar apenas as partes acima da frequência central para depois
% fazer o 'folding', se desejado)
% b: denominador do designador de bandas fracionais

% output:
% Ax: limites de atenuação

function [Ax] = utf_filter_attenuation_limits(Delta_b,b)
% setup
% não é pra ter valores negativos
if(~isequal(sum(Delta_b < 0),0))
    error("Valroes em Delta_b não podem ser negativos!");
else
    % não é pra ser maior do que 4 (porque a atenuação é tabelada, e até
    % nem teria como interpolar)
    if(~isequal(sum(Delta_b > 4),0))
        error("Valores em Delta_b não podem ser maiores que 4!");
    end
end
% já é pra vir em ordem, mas...
Delta_b = sort(Delta_b);
%% idxA, idxB e 0.5
% basicamente, essa seção retorna idxA e idxB, o último índice antes de 0.5
% ou igual, e o primeiro índice depois de 0.5 ou igual. Se houver apenas um
% 0.5, idxB retorna vazio. Se não houver valores antes ou igual a 0.5, idxA
% retorna vazio. Se não houver valores depois ou dois iguais a 0.5, idxB
% retorna vazio.

% verifica se tem 0.5
if(~isequal(sum(Delta_b==0.5),0))
    % verifica se não há apenas 1 índice com 0.5
    if(~isequal(sum(Delta_b==0.5),1))
        % verifica se há dois índices para 0.5
        if(isequal(sum(Delta_b==0.5),2))
            % índice que corresponde ao 0.5 vindo das baixas
            idx1 = find(Delta_b==0.5);
            idx1 = idx1(1);
            % índice que corresponde ao 0.5 vindo das altas
            idx2 = idx1 + 1; 
        else
            % se não forem 1 ou 2, acusa erro
            error("Não pode haver mais de dois índices com valor "+...
                  "igual a 0.5 em Delta_b!");
        end
    else
        % se tem apenas um índice com 0.5
        idx1 = find(Delta_b==0.5);
        idx2 = [];
    end
else
    % se não tem
    idx1 = find(Delta_b < 0.5);
    if(~isempty(idx1))
        idx1 = idx1(end);
    end
    idx2 = find(Delta_b > 0.5);
    if(~isempty(idx2))
        idx2 = idx2(1);
    end
end
%% interpolação em si
% Referência (tabelado)
[Delta_b_ref,A_ref] = utf_tabled_filter_attenuation_limits();
Omega_ref = utf_omega_h(Delta_b_ref,b);
% e Omega de interesse
Omega = utf_omega_h(Delta_b,b);
% vetor para atenuações interpoladas
Ax = [];
if(~isempty(idx1))
    A = utf_filter_attenuation_interpolation(Omega(1:idx1),...
        Omega_ref(1:5),A_ref(1:5,:));
    Ax = [Ax; A];
end
if(~isempty(idx2))
    A = utf_filter_attenuation_interpolation(Omega(idx2:end),...
        Omega_ref(6:end),A_ref(6:end,:));
    Ax = [Ax; A];
end
end