%% utf_tabled_filter_attenuation_limits %%

% limites tabelados para atenuações relativas na norma 61260

% Autor: Luis Henrique Sant'Ana

% output:
% Delta_b: intervalos de largura de banda que correspondem às frequências
% normalizadas (tabelado)
% A: limites para atenuções relativas (tabelado)

function [Delta_b, A] = utf_tabled_filter_attenuation_limits()

% Delta_b
Delta_b = [0:1/8:1/2 1/2 1:4];
% atenuações mínimas e máximas para Omega_h, como na tabela
Amin = [repmat(-0.4,5,1); [1.2 16.6 40.5 60 70]' ];
Amax = [ [0.4 0.5 0.7 1.4 5.3]'; repmat(100,5,1)]; %inf substituído por 100
% juntando tudo
A = [ Amin Amax ];

end