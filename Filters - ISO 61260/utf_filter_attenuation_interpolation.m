%% utf_filter_attenuation_interpolation %%

% rotina para calular limites de atenuação de filtros para valores de
% frequência não tabelados na norma 61260 por interpolação

% essa rotina é chamada pela função 'utf_attenuation_limits';
% os valores de referência são carregados pela função
% 'utf_attenuation_limits', através da função
% 'utf_tabled_attenuation_limits';

% Autor: Luis Henrique Sant'Ana

% input:
% Omega: frequências normalizadas de interesse
% Omega_ref: frequências normalizadas de acordo com a norma (tabeladas)
% A_ref: atenuações de referência, para as frquências dereferências
% (tabeladas)

% output:
% Ax: atenuações interpoladas para as frequências de interesse

function [Ax] = utf_filter_attenuation_interpolation(Omega,Omega_ref,A_ref)

% vetor com as atenuações interpoladas
Ax = nan(length(Omega),2);

for j = 1 : 2
    for i = 1 : length(Omega)
        [~,idxA] = min(abs(Omega_ref - Omega(i)));
        if(isequal(Omega(i),Omega_ref(idxA)))
            Ax(i,j) = A_ref(idxA,j);
        else
            if(Omega(i) < Omega_ref(idxA))
                idxB = idxA - 1;
            else
                idxB = idxA + 1;
            end
            Ax(i,j) = A_ref(idxA,j) + (A_ref(idxB,j) - A_ref(idxA,j)) *(...
                      log10(Omega(i) / Omega_ref(idxA)) /...
                      log10(Omega_ref(idxB)/Omega_ref(idxA))        );
        end
    end
end

end