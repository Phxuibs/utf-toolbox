%% utf__filter_half_band_frequencies %%

% rotina para calcular as frequências de meia banda

% Autor: Luis Henrique Sant'Ana

% input:
% fm: frequência central de banda
% b: denominador do designador de bandas fracionais
% G: relação de oitava (função utf_octave_frequency_ratio(.))

% output:
% f1: frequência de meia banda inferior
% f2: frequência de meia banda superior

function [f1,f2] = utf_filter_half_band_frequencies(fm,b,G)

f1 = fm * G^(-1/(2*b));
f2 = fm * G^(+1/(2*b));

end