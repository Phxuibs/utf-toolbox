%% utf_PL_to_ITA %%

% Calculate impulse responses from a group of measurements from a
% parametric loudspeaker to ITA objects

% input (all are name-value pairs - check the parse section for the default
% values!):
% foldername: relative folder (from current folder) containing the
% measurements and calibration file (string)
% measurementFilePrefix: prefix for the measurement .mat files in folder
% 'foldername' (string)
% calibrationFile: calibration .mat file name (without ".mat") (string)
% itaFilename: name to save the ITA audio object containing the IRs - it
% will be saved on the same folder containing the measurement and
% calibration files (without ".ita") (string)

% output:
% ITA: ITA object with impulse responses

% Attention! There is no Analog to Digital Conversion configuration!

% Author: Luis H. Sant'Ana (with base code from La Salle)

function ITA = utf_PL_to_ITA(varargin)
%% parse
defaultFoldername = "";
defaultMeasurementFilePrefix = "Medida_Directividad";
defaultCalibrationFile = "Calibracio_cal";
defaultItaFilename = "ITA_object";

p = inputParser;
addParameter(p,"foldername",defaultFoldername,@(x) isstring(x));
addParameter(p,"measurementFilePrefix",defaultMeasurementFilePrefix,...
                                      @(x) isstring(x));
addParameter(p,"calibrationFile",defaultCalibrationFile,@(x) isstring(x));
addParameter(p,"itaFilename",defaultItaFilename,@(x) isstring(x));
parse(p,varargin{:});

foldername = p.Results.foldername;
measurementFilePrefix = p.Results.measurementFilePrefix;
calibrationFile = p.Results.calibrationFile;
itaFilename = p.Results.itaFilename;
%% setup
% Get foldername to include full previous path and setup to just add the
% file name
foldername = string(pwd) + "/" + foldername + "/";
% Calibration file name (just add .mat)
calibrationFile = calibrationFile + ".mat";
% ITA audio object file name (just add .ita);
itaFilename = itaFilename + ".ita";
% Get all measurement file names
files = dir(foldername);
% Get the indexes that corresepond to measurement files
measurementFilesIdx = find(contains({files.name},measurementFilePrefix));
% Time up and down
% tempsUp = 0.008; % OPL
% tempsDown = 0.002; % OPL
tempsUp = 2; % CESVA
tempsDown = 0.05; % CESVA
%% Check if calibration is possible and set it up
% Check if calibration file exists
if exist(foldername + calibrationFile,'file')==2
    % if so, set flag to true for posterior calibration and
    calibrationFlag = true;
    % load the calibration value dB1k
    load(foldername + calibrationFile,'dB1k');
else
    % else, set calibration flag to false
    calibrationFlag = false;
end
%% Load measurements, calibrate (if flag is up), calculate IRs
% create empty array for concatenation
hout = [];
% for the indxes in files.name that correspond to measurement files
for i = measurementFilesIdx
    % load the file (we want altaveu, Tsweep, Tzeros and fs variables)
    load(foldername + string(files(i).name),'altaveu','Tsweep',...
                                                  'Tzeros','fs');
    % Calibrate
    if(calibrationFlag)
        altaveu = altaveu.*10^((94-dB1k)/20);
    end
    % Generate inverse sweep
    [~,invSweep] = utf_generate_exp_sweep(Tsweep,Tzeros,80,12000,20,14000,fs);
    % Compute Impulse Response
    h2 = utf_fft_conv(altaveu,invSweep,'same');
    [~,I] = max(abs(h2));
    hout = [hout h2(I-fs*tempsDown : I+fs*tempsUp)];
end
%% Writes impulse responses in ITA format
% Creates empty ITA audio object
ITA = itaAudio;
% We assume all sampling frequencies are the same, so we use the last one
ITA.samplingRate = fs;
% Pass in the impulse responses
ITA.time = hout;
% save ITA object on measurement and calibration files folder
ita_write(ITA,foldername + itaFilename);
end
