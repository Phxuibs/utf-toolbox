% Code from La Salle University

function [sweep,invsweep]=generate_exp_sweep(T,Tzeros,f1,f2,fin,fout,fs) % v2

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate sine sweep with exponential frequency dependent energy decay
% over time and time/frequency inverse for impulse response calculation
% inputs:
% T = sweep duration in seconds
% Tzeros = zeros after sweep in seconds
% f1 = start frequency in Hz
% f2 = end frequency in Hz
% fin = start frequency in Hz for fade in (fin<f1)
% fout = end frequency in Hz for fade out (fout>f2)
% fs = sampling frequency

% Author: Marc Arnela, 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) Sweep
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t=0:1/fs:(T*fs-1)/fs;
t=t';

w1=2*pi*fin;
w2=2*pi*fout;
K=T*w1/log(w2/w1);
L=T./log(w2/w1);
sweep=sin(K.*(exp(t/L) - 1) );

w=K/L.*exp(t/L);
f=w/2/pi;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2) Fade in/out 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ind1=find(f>f1,1,'first');
ind1=ind1-1;
ind2=find(f<f2,1,'last');
ind2=ind2+1;

Lin=ind1;
Lout=length(sweep)-ind2+1;

Fadein=(0:Lin-1)/(Lin-1);
Fadeout=(Lout-1:-1:0)/(Lout-1);

sweep(1:ind1)=sweep(1:ind1).*Fadein.';
sweep(ind2:end)=sweep(ind2:end).*Fadeout.';

%T=T+Tzeros;
%t=0:1/fs:(T*fs-1)/fs;
%t=t';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3) Inverse sweep
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invsweep=sweep(length(sweep):-1:1).*(w2/w1).^(-t/T);
% invsweep=f1/L.*sweep(length(sweep):-1:1).*exp(-t/L);
% m=w1./w;
% invsweep=sweep(length(sweep):-1:1).*m;

sweep=[sweep; zeros(Tzeros*fs,1)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4) Scaling factor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h=fft_conv(sweep,invsweep);

[C,I]=max(abs(h));
tempsUp=0.01;
tempsDown=0.01;
h2=h(I-fs*tempsDown:1:I+fs*tempsUp);
h=h2;

%NFFT=length(h);
NFFT=fs;
H=fft(h,NFFT);
Hmag=abs(H)/(NFFT/2);
fmax=fout;
maxmostra = round(fmax * NFFT/fs);
factor = (maxmostra)/fmax;
f = [0:maxmostra]./factor;
ind=find(f>((f1+f2)/2),1,'first');
% C=abs(H(ind));

% % % % % % % % % ref=20*10^(-6);
% % % % % % % % % Gmic=1; %microphone sensitivity 4.13mV/Pa
% % % % % % % % % Gamp=0.1; %!0x amplification on the preamp
% % % % % % % % % Gadj=1.2;
% % % % % % % % % % C=Hmag(ind)./(ref*Gadj*Gamp*Gmic);
C=Hmag(ind);

invsweep=invsweep/C;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5) Plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargout==0
  
   %Figure 1
    figure;
    t=0:1/fs:((T+Tzeros)*fs-1)/fs;
   subplot(211);plot(t,sweep);xlabel('Time [s]')
   subplot(212);spectrogram (sweep,256,250,250,fs);xlabel('Frequency [Hz]')
    
   %Figure 2
   figure;
    
   %a) Impulse response 
   h=fft_conv(sweep,invsweep);
   subplot(211);plot(h);xlabel('n'); ylabel('h[n]');
   
   %b) Frequency response
   NFFT=length(h);
   fmax=fs/2;
   maxmostra = round(fmax * NFFT/fs);
   factor = (maxmostra)/fmax;
   f = [0:maxmostra]./factor;
   H=10*log10(abs(fft(h,NFFT)));
   H=H(1:(maxmostra+1));
   subplot(212);plot(f/1000,H);xlabel('Frequency [kHz]');ylabel('dB');
end

