%% utf_string_adjustment %%

% ajustar string para pular linha a cada n caracteres

% Autor: Luis Henrique Sant'Ana

% input:
% str = number, string ou array de caracteres para serem ajustados;
% n = limite de caracteres até que haja quebra de linha (se vazio, utiliza
% o default n = 80)

function [output] = utf_string_adjustment(str,n)

    % ter certea de que é char
    if(isstring(str))
        str = char(str);
    else
        if(isnumeric(str))
            str = num2str(str);
        end
    end
    
    if(isempty(n))
        n = 80;
    end
    
    output = [];
    
    while(length(str) > n)
        % se o caracter logo após o limite for ' ', concatena o trecho
        % inteiro em 'output' e cria nova linha, removendo o ' '
        if(strcmp(str(n+1),' '))
            output = [output str(1:n) '\n'];
            % ajusta 'str' para próxima análise
            str = str(n+2:end);
        else
            % senão, verifica índices que contenham ' ' num trecho com o
            % tamanho limite
            idx = strfind(str(1:n),' ');
            % caso não haja ' ', concatena o trecho inteiro em 'output' e
            % cria nova linha
            if(isequal(length(idx),0))
                output = [output str(1:n) '\n'];
                % ajusta 'str' para próxima análise
                str = str(n+1:end);
            else
                % caso haja ' ', o trecho até o último ' ' é concatenado em
                % 'output' substituindo o último ' ' por '\' e adiconando
                % 'n'
                output = [output str(1:idx(end)-1) '\n'];
                % ajusta 'str' para próxima análise
                str = str(idx(end)+1:end);
            end
        end
    end

    % a saída 'output' recebe a string 'str' inteira, caso esta esteja com
    % comprimento menor do que o limite 'n' de caracteres
    output = [output str(1:end)];
    
end