%% utf_adjust_plot_coarse %%

% ajustar tamanho do plot e salvar em PDF "the quick and easy way"

% Autor: Luis Henrique Sant'Ana

% input
% h = figure;
% filename = string com nome para para o arquivo (sem '.pdf')
% (inserir vetor vazio para não salvar, i.e filename = []);
% width = largura do papel (PDF)
% (inserir vetor vazio para default, i.e width = [])

function [] = utf_adjust_plot_coarse(h,filename,width)
%% ajustar tamanho do plot (plot no matlab, sua janela, etc.)
% a unidade padrão é pixels;
% pegar a resolução do monitor
screen = get(0, 'Screensize');
% ajustar tamanho da figura, contando com bordas, toolbar, etc., por não
% fazer diferença, nesse script, no resultado do PDF - isso resulta em
% janela do matlab melhor enquadrada
% h.OuterPosition = [left bottom width height];
h.OuterPosition = [0, 0, screen(end-1:end)];

%% ajustar tamanho do papel (quando for salvar em pdf, por exemplo)
if(~isempty(filename))
% uma folha A4 tem 21cm de largura;
% a largura padrão, então, será de 21cm;
% a altura é calculada a partir da relação de ouro com a largura
h.PaperUnits = 'centimeters';
if(isempty(width))
width = 21;
end
h.PaperSize = [width width/1.61803398875];
% posicionar plot na página;
% utilizar espaço mínimo entre o canto inferior esquerdo da página e o
% canto inferior esquerdo da  figura;
% utilizar o espaço máximo para a figura (que imagina-se ser h.PaperSize);
% percebe-se uma margem desnecessária a esquera e direita, mas um corte
% justo em cima e embaixo; de qualquer forma, parece genericamente o
% melhor;
% h.PaperPosition = [left bottom width height];
h.PaperPosition = [0 0 h.PaperSize];
% print() apresentou-se mais rápido do que saveas();
% o parâmetro '-fillpage' apresentou margens desnecessárias
print(h,filename + ".pdf",'-dpdf');
end
end