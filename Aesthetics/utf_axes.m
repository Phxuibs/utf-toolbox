%% utf_axes %%

% Apenas alguns ajustes no 'axes' de uma figura

% Autor: Luis Henrique Sant'Ana

% input:
% ax = objeto 'axes';
% fontSize = tamanho da fonte (double);
% output:
% ax = axes (retorna o 'axes' ajustado);

function [ax] = utf_axes(ax, fontSize)

% ajustar fonte
ax.FontSize = fontSize;
% grids
ax.XGrid = 'on';
ax.YGrid = 'on';
ax.XMinorGrid = 'off';
ax.YMinorGrid = 'off';
% ticks
ax.XMinorTick = 'off';
ax.YMinorTick = 'off';
% interpretadores latex
ax.XLabel.Interpreter = 'latex';
ax.YLabel.Interpreter = 'latex';
ax.TickLabelInterpreter = 'latex';
ax.Title.Interpreter = 'latex';

end