%% utf_adjust_plot_fine %%

% ajustar tamanho do plot e salvar em PDF - corte manual (ajuste fino)

% Autor: Luis Henrique Sant'Ana

% input
% fig = figure;
% filename = string com nome para para o arquivo (sem '.pdf')
% (inserir vetor vazio para não salvar, i.e filename = []);
% size = [largura, altura], em centímetros, do papel (PDF);
% ax (parâmetro) para receber dois eixos, quando houver (e.g. ax={ax1,ax2})
% % % bgcolor (parâmetro) para cor de fundo

function [] = utf_adjust_plot_fine(fig,filename,size,varargin)
%% parseamento
% p = inputParser;
% addParameter(p,'ax',nan,@(x) iscell(x) && isvector(x) && isequal(length(x),2));
% addParameter(p,'bgcolor',nan,@(x) isvector(x) && isequal(length(x),3));
% parse(p,varargin{:});

% ax = p.Results.ax;
% bgcolor = p.Results.bgcolor;
%% força filename como string
if(ischar(filename))
    filename = string(filename);
end
%% ajustar tamanho do plot (plot no matlab, sua janela, etc.)
% a unidade padrão é pixels;
% pegar a resolução do monitor
screen = get(0, 'Screensize');
drawnow
% ajustar tamanho da figura;
% fig.Position = [left bottom width height];
fig.Position = [0, 0, screen(end-1:end)];
% drawnow
% if(~isnan(ax))
%     ax1 = ax{1};
%     ax2 = ax{2};
%     ax2.Position = ax1.Position;
% end
% ax = get(gcf,'CurrentAxes');
% ax.XTickLabel = utf_format(ax.XTick);
% ax.YTickLabel = utf_format(ax.YTick);
%% ajustar tamanho do papel (quando for salvar em pdf, por exemplo)
if(~isempty(filename))
fig.PaperUnits = 'centimeters';
fig.PaperSize = size;
drawnow
% print() apresentou-se mais rápido do que saveas();
print(fig,filename + ".pdf",'-dpdf');

% conversor de hex para RBG:
% hex = '#94b2d1';
% bgcolor = sscanf(hex(2:end),'%2x%2x%2x',[1 3])/255;
% colcoar a cor como background color:
% set(gcf,'Color',bgcolor);
% só colocar cor não é suficiente; quando salva, sai branca. precisa do
% comando set(gcf,'InvertHardCopy','off');

% exportgraphics é uma versão mais moderna do prit e do saveas;
% aqui ela salva com o contorno (o que seria colorido acima) transparente;
% porém corta pontinhas
% exportgraphics(h,filename+".pdf",'ContentType','vector',...
%                'BackgroundColor','none')
end
end