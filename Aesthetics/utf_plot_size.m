%% utf_plot_size %%

% aquisição de dimensões para corte de plot de acordo com monitor (tela)
% utilizado

% Autor: Luis Henrique Sant'Ana

% input
% screen = string com nome do monitor (e.g. "luis_desktop")

function [output] = utf_plot_size(screen)
    
    monitor = ["lab";...
               "luis_desktop";...
               "luis_desktop_large"];
    plot_size = [43 27;...
                 31 18;...
                 33 19];
    
    output = plot_size(monitor == screen,:);
end