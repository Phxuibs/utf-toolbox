%% utf_adjust_plot_example %%

% exemplificar as rotinas utf_adjust_plot_coarse() e utf_adjust_plot_fine()

% Autor: Luis Henrique Sant'Ana

function [] = utf_adjust_plot_example()

% ajsute grosso, padrão
h = utf_sample_plot;
utf_adjust_plot_coarse(h,"plot_default",[]);
close;

% ajuste grosso, com alteração para aproveitar melhor a altura disponível
% (no monitor utilizado para criar este exemplo)
h = utf_sample_plot;
utf_adjust_plot_coarse(h,"plot_24",24);
close;

% ajuste fino;
% (dimensões devem ser ajustadas manualmente - "ajuste fino")
h = utf_sample_plot;
utf_adjust_plot_fine(h,"plot_fine",[31,18]);
close;
end