%% utf_minor_grid %%

% Habilita minor grid e minor ticks de X e Y de um objeto axes.

% input:
% ax = eixos da figura (e.g. ax = gca)
% opcionais:
% g_alpha = grid alpha value
% mg_alpha = minor grid alpha value
% output:
% ax = retorna os eixos ajustados

% Autor: Luis Henrique Sant'Ana

function [ax] = utf_minor_grid(ax,varargin)
%% parser
default_g_alpha = 0.25;
default_mg_alpha = 0.4;

p = inputParser;
validation_fcn = @(x) (x>=0) && (x<=1) && isnumeric(x) && isscalar(x);
addOptional(p,'g_alpha',default_g_alpha,validation_fcn);
addOptional(p,'mg_alpha',default_mg_alpha,validation_fcn);
parse(p,varargin{:});

g_alpha = p.Results.g_alpha;
mg_alpha = p.Results.mg_alpha;
%% adjust ticks and grid

ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';
ax.XMinorTick = 'on';
ax.YMinorTick = 'on';
ax.GridAlpha = g_alpha;
ax.MinorGridAlpha = mg_alpha;

end