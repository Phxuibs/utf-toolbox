%% utf_color %%
%
% cores!!!
%
% input:
% color: número ou nome da cor (numeric ou string ou char - scalar)
%
% output:
% RGB_triplet: - (numeric column vector)
%
% Autor: Luis Henrique Sant'Ana

function [RGB_triplet] = utf_color(color)
%% setup
% RGBs
colors = [...
             [0.00; 0.45; 0.74],... % azul
             [0.85; 0.33; 0.10],... % laranja
             [0.93; 0.69; 0.13],... % amarelo
             [0.49; 0.18; 0.56],... % roxo
             [0.47; 0.67; 0.19],... % verde
             [0.00; 0.50; 0.00],... % verde_escuro
             [0.30; 0.75; 0.93],... % azul_claro
             [0.64; 0.08; 0.18],... % vermelho
             [0.40; 0.40; 0.40],... % cinza
             [0.00; 0.70; 0.50],... % verde_azul_escuro
             [63/255; 0; 1],...     % indigo
             [69/255; 0; 100/255],... % vinho
             [1; 0; 80/255],...     % framboesa
             [1; 160/255; 0],...    % amarelo_queimado
             [0.722; 0.451; 0.2],...% cobre
             [0.1; 0; 1],...        % azul_escuro
             [0; 0; 0]];            % preto

%  nomes correspondentes
names = ["azul","laranja","amarelo","roxo","verde",...
         "verde_escuro","azul_claro","vermelho","cinza","verde_azul_escuro",...
         "indigo","vinho","framboesa","amarelo_queimado","cobre",...
         "azul_escuro","preto"];
%% pegar RGB
% se 'color' for número, pega o RGB de acordo com o array 'ordem'
if(isnumeric(color))
    ordem = ["azul_claro", "verde", "laranja", "vermelho", "azul",...
             "roxo", "verde_azul_escuro","indigo","vinho","verde_escuro",...
             "cinza","framboesa","amarelo_queimado","azul_escuro","amarelo","cobre","preto"];
    % enquanto o número for maior do que a quantidade de cores, "volta"
    % pro começo
    while(color > length(ordem))
        color = color - length(ordem);
    end
    RGB_triplet = colors(:,names == ordem(color));
else % senão, pega pelo nome
    % se for char converte pra string
    if(ischar(color))
        color = string(color);
    end
    RGB_triplet = colors(:,names == color);
end
end