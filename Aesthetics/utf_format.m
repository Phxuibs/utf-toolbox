%% formatação de números %%

% Autor: Luis Henrique Sant'Ana
% referência: https://undocumentedmatlab.com/articles/formatting-numbers
% (agradecimentos ao professor Marcelo Rosa pela indicação)

% input: x (matrix numérica)
% output: y (string)

function y = utf_format(x)
%% setup
dfs = java.text.DecimalFormatSymbols(java.util.Locale('pt'));
% dfs.setGroupingSeparator(' '); % substitui ponto por espaço
nf = java.text.DecimalFormat;
nf.setDecimalFormatSymbols(dfs)
nf.setGroupingUsed(false); % remove ponto separador
%% formatação
% formatação em si
y = arrayfun(@(x) nf.format(x),x,'UniformOutput',false);
% converte para string matlab
y = string([y{:,:}]);
% dimensiona como o original
y = reshape(y,size(x));
end