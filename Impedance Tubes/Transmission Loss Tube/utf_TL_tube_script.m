%% utf_TL_tube_measurements %%

% This script will guide you through measurement of transfer functions and
% then transmission loss coefficient calculation using the "isolation tube"
% at UTFPR.

% Author: Luis H. Sant'Ana
%% Setup measurement
% Setup transfer function measurement object.
% Recommended frequency range: 400Hz - 4000Hz.
% This is set in combination with the tube parameters from function
% utf_TL_tube_setup - see code below.
% For example, latency samples is set to 0, but time windowing is set to
% start at 0.013 s.
MS = utf_ITA_MS('freqRange', [400 4000], 'numAverages', 3, ...
                'samplingRate', 48000, 'latencySamples', 0);
% Environmental parameters: temperature and pressure.
% Quick check on temperature and pressure on Curitiba:
% http://www.simepar.br/prognozweb/simepar/dados_estacoes/25264916
% Temperature (degrees Celsius)
env.T = 20;
% Atmospheric pressure (kPa)
env.P = 90.8;
% Path to save files
path = "./";
% Sample name
sample = "sampleA";
% Sample thickness (meters)
d = 0.02;
%% Run measurements
% It'll be expected files to be displayed in the form:
% The 4 measurements with absorptive ending on top, mics 1-4, followed by
% the other four measurements with the reflective ending.
% For example:
% A_M1.ita
% A_M2.ita
% ...
% R_M3.ita
% R_M4.ita
% (ita_merge not being used to give more flexibility to the user when
% measuring).
TF = MS.run;
ita_write(path + sample + "_A_M1");
clearvars TF
%% Calculate coefficients
% (for the use of absorptive and reflective endings, i.e. 8 measurements,
% vargout is TL for two loads).
[TL_one,freqVector,TL_two] = utf_TL_tube_setup(path+sample,...
                             'freqRange',MS.freqRange,'d',d,'T',T,'P');
% Save full workspace
save(path+sample);