%% cálculo de TL de acordo com o documento %%

% revisar! isso veio da primeira rotina

% Amplitudes
Amp = @(f1,a,f2,b,c,d) 1i * ( ( f1.*exp(1i*k*a) - f2.*exp(1i*k*b) ) ./ ...
                            ( 2*sin( k*(c-d) )) );
A(:,1) = Amp(f(:,1),x(2),f(:,2),x(1),x(1),x(2)); % absorptive A
A(:,2) = Amp(f(:,2),x(1),f(:,1),x(2),x(1),x(2)); % absorptive B
A(:,3) = Amp(f(:,3),x(4),f(:,4),x(3),x(3),x(4)); % absorptive C
A(:,4) = Amp(f(:,4),x(3),f(:,3),x(4),x(3),x(4)); % absorptive D
A(:,5) = Amp(f(:,5),x(2),f(:,6),x(1),x(1),x(2)); % reflective A 
A(:,6) = Amp(f(:,6),x(1),f(:,5),x(2),x(1),x(2)); % reflective B
A(:,7) = Amp(f(:,7),x(4),f(:,8),x(3),x(3),x(4)); % reflective C
A(:,8) = Amp(f(:,8),x(3),f(:,7),x(4),x(3),x(4)); % reflective D
% pressures before and after the sample
p(:,1) = A(:,1)+A(:,2); 							 % absorptive x=0
p(:,2) = A(:,3).*exp(-1i*k*d) + A(:,4).*exp(1i*k*d); % absorptive x = d
p(:,3) = A(:,5)+A(:,6); 							 % reflective x = 0
p(:,4) = A(:,7).*exp(-1i*k*d) + A(:,8).*exp(1i*k*d); % reflective x = d
% velocities before and after the sample
u(:,1) = (A(:,1)-A(:,2))/(rho*c); 							   % absorptive x = 0
u(:,2) = (A(:,3).*exp(-1i*k*d) - A(:,4).*exp(1i*k*d))/(rho*c); % absorptive x = d
u(:,3) = (A(:,5)-A(:,6))/(rho*c); 							   % reflective x = 0
u(:,4) = (A(:,7).*exp(-1i*k*d) - A(:,8).*exp(1i*k*d))/(rho*c); % reflective x = d
% Matriz de Transferência
% de acordo com o trablaho escrito do José e do Matheus:
T_11 = (p(:,2).*u(:,4)-p(:,3).*u(:,2)) ./ (p(:,1).*u(:,4)-p(:,4).*u(:,2));
T_12 = (p(:,1).*p(:,4)-p(:,3).*p(:,2)) ./ (p(:,2).*u(:,4)-p(:,4).*u(:,2));
T_21 = (u(:,2).*u(:,4)-u(:,3).*u(:,2)) ./ (p(:,2).*u(:,4)-p(:,4).*u(:,2)); % mesmo denominador que T_12
T_22 = (p(:,2).*u(:,4)-p(:,3).*u(:,1)) ./ (p(:,1).*u(:,4)-p(:,4).*u(:,2)); % quarto termo é a única diferença para o T_11
% de acordo com a rotina do José e do Matheus:
% T_11 = (p(:,1).*u(:,4)-p(:,3).*u(:,2)) ./ (p(:,2).*u(:,4)-p(:,4).*u(:,2));
% T_12 = (p(:,3).*p(:,2)-p(:,1).*p(:,4)) ./ (p(:,2).*u(:,4)-p(:,4).*u(:,2));
% T_21 = (u(:,1).*u(:,4)-u(:,3).*u(:,2)) ./ (p(:,2).*u(:,4)-p(:,4).*u(:,2));
% T_22 = (p(:,2).*u(:,3)-p(:,4).*u(:,1)) ./ (p(:,2).*u(:,4)-p(:,4).*u(:,2));
% Coeficiente de Transmissão e Perda de Transmissibilidade
tau = 2*exp(1i*k*d)./(T_11+(T_12/(rho*c))+rho*c*T_21+T_22);
TL = -20*log10(tau);