% Algumas coisas foram modificadas, mas o cálculo de SRI em si é o mesmo!

clear
clc
snip = './T./M1_AA_*.ita';
freqRange = [400 4000];
t_crop = [0.013 4.013];
%% Tubo
% Largura da amostra [m]
d = 2;
l1 = 15/100;
s1 = 3/100;
l2 = (15+d)/100;
s2=s1;
%% Ambiente
%Temperatura do ar em ºC
T = 25;
%Pressão Atmosférica em Curitiba (considerado 914m de altitude)[kPa]
P = 90.8;
% Velocidade do som no ar [m/s]
c = 20.047*(273.15+T)^(1/2);
% Densidade do ar [kg/m³]
rho = 1.290*(P/101.325)*(273.15/(273.15+T));
%% Carrega dados das medições na faixa de frequências de interesse
% nomes dos arquivos de interesse na pasta de interesse
files = dir(snip);
% junta caminho absoluto com nome dos arquivos de interesse
files = string({files.folder}) + "\" + string({files.name});
% carrega os arquivos .ita, junta em um, corta no tempo
RI = ita_time_crop(ita_merge(ita_read(files)),t_crop);
% índices de interesse
idx = freqRange(1)<=RI.freqVector & RI.freqVector<=freqRange(2);
% não aceita selecionar faixa com valores lógicos em apenas um passo para o
% freqVector (vetor de frequências)
f = RI.freqVector;
f = f(idx);
% número de onda
k = 2*pi*f*c^(-1); % (rad/m)
% coeficientes espectrais
h1_a = RI.freq(idx,1);
h2_a = RI.freq(idx,2);
h3_a = RI.freq(idx,3);
h4_a = RI.freq(idx,4);
h1_b = RI.freq(idx,5);
h2_b = RI.freq(idx,6);
h3_b = RI.freq(idx,7);
h4_b = RI.freq(idx,8);
% apaga objeto ita
clearvars RI
%% Calculation

% Amplitudes
A_a = 1i * ((h1_a.*exp(-1i*k*l1)-h2_a.*exp(-1i*k*(l1+s1)))./(2*sin(k*s1)));
B_a = 1i * ((h2_a.*exp(+1i*k*(l1+s1))-h1_a.*exp(+1i*k*l1))./(2*sin(k*s1)));
C_a = 1i * ((h3_a.*exp(+1i*k*(l2+s2))-h4_a.*exp(+1i*k*l2))./(2*sin(k*s2)));
D_a = 1i * ((h4_a.*exp(-1i*k*l2)-h3_a.*exp(-1i*k*(l2+s2)))./(2*sin(k*s2)));

A_b = 1i * ((h1_b.*exp(-1i*k*l1)-h2_b.*exp(-1i*k*(l1+s1)))./(2*sin(k*s1)));
B_b = 1i * ((h2_b.*exp(+1i*k*(l1+s1))-h1_b.*exp(+1i*k*l1))./(2*sin(k*s1)));
C_b = 1i * ((h3_b.*exp(+1i*k*(l2+s2))-h4_b.*exp(+1i*k*l2))./(2*sin(k*s2)));
D_b = 1i * ((h4_b.*exp(-1i*k*l2)-h3_b.*exp(-1i*k*(l2+s2)))./(2*sin(k*s2)));

% Pressão e velocidade normal incidente antes e depois da amostra
p0_a = A_a+B_a;
u0_a = (A_a-B_a)/(rho*c);
pd_a = C_a.*exp(-1i*k*d) + D_a.*exp(1i*k*d);
ud_a = (C_a.*exp(-1i*k*d) - D_a.*exp(1i*k*d))/(rho*c);

p0_b = A_b+B_b;
u0_b = (A_b-B_b)/(rho*c);
pd_b = C_b.*exp(-1i*k*d) + D_b.*exp(1i*k*d);
ud_b = (C_b.*exp(-1i*k*d) - D_b.*exp(1i*k*d))/(rho*c);

% Matriz de Transferência
T_11 = (p0_a.*ud_b-p0_b.*ud_a) ./ (pd_a.*ud_b-pd_b.*ud_a);
T_12 = (p0_b.*pd_a-p0_a.*pd_b) ./ (pd_a.*ud_b-pd_b.*ud_a);
T_21 = (u0_a.*ud_b-u0_b.*ud_a) ./ (pd_a.*ud_b-pd_b.*ud_a);
T_22 = (pd_a.*u0_b-pd_b.*u0_a) ./ (pd_a.*ud_b-pd_b.*ud_a);

% Coeficiente de Transmissão e Perda de Transmissibilidade
tau = 2*exp(1i*k*d)./(T_11+(T_12/(rho*c))+rho*c*T_21+T_22);
SRI = -20*log10(tau);
% plot
semilogx(f,real(SRI))