%% utf_TL_tube_calc %%

% Realiza o cálculo de Perda de Transmissão (Transmission Loss - TL) em si,
% de acordo com a norma ASTM E2611-09 [1] para utilização de apenas um
% microfone e referência de sinal no falante (i.e. não são realizadas
% correções das funções de transferência)

% Autor: Luis H. Sant'Ana, com base no script original de José Larcher e
% Matheus Wachholtz e revisado com a norma [1].

% Segundo o trabalho de Larcher e Wachholtz [2], o tubo responde de maneira
% mais consistente entre 400 Hz e 4000 kHz.

% [1] ASTM E2611-09. Standard Test Method for Normal Incidence
% Determination of Porous Material Acoustical Properties Based on the
% Transfer Matrix Method. 2009.
% [2] LARCHER, J. K.; WACHHOLTZ, M. H. Construção e teste de um tubo de
% impedância para medição de isolamento. Trabalho de Conlusão de Curso,
% Universidade Tecnológica Federal do Paraná - UTFPR. 2018.
% [3] BISTAFA, S. R. Acústica aplicada ao controle do ruído. 2 ed. São
% Paulo: Blucher, 2011.

% Esquema:
% distâncias:          |<s1>|<l1>|<   l2   >|< s2 >|
% --------------------|M1|--|M2|------------|M3|--|M4|---------------
% FALANTE =)=)=)    ->A     B<-   |AMOSTRA| ->C     D<-        FUNDO
% -------------------------------------------------------------------

% input (todos obrigatórios):
% f: coeficientes espectrais (funções de transferência) ordenados em oito
% colunas na seguinte forma: microfone 1-4 com fundo absorvente (anecoico)
% seguido de microfone 1-4 com fundo reflexivo ou aberto se desejado o
% cálculo com duas cargas. Apresentar apenas as 4 primeiras colunas, também
% com fundo absorvente, se desejado apenas o cálculo com uma carga.
% (array complexo)
% k: número de onda (wavenumber) (metros recíprocos) (vetor numérico)
% d: espessura da amostra (metros) (escalar numérico)
% c: velocidade do som (metros por segundo) (escalar numérico)
% rho: densidade do ar (quilogramas por metro cúbico) (escalar numérico)
% LS: distâncias, em ordem: [l1 l2 s1 s2]; (metros) (vetor numérico)

function [TL_one,varargout] = utf_TL_tube_calc(f,k,d,c,rho,LS)
%% parseamento

isPosNumScalar = @(x) isnumeric(x) && isscalar(x) && x>0;
p = inputParser;
addRequired(p,'f',@(x) isnumeric(x) && ...
                      (isequal(size(x,2),8) || isequal(size(x,2),4)) );
addRequired(p,'k',@(x) isnumeric(x) && isvector(x) && all(x>0));
addRequired(p,'d',isPosNumScalar);
addRequired(p,'c',isPosNumScalar);
addRequired(p,'rho',isPosNumScalar);
addRequired(p,'LS',@(x) isvector(x) && isequal(length(x),4) && all(x>0));
parse(p,f,k,d,c,rho,LS);
if(~isequal(size(f,1),length(k)))
    error("Dimensões de 'f' e 'k' não correspondem!");
end
% to facilitate names:
l1 = LS(1);
l2 = LS(2);
s1 = LS(3);
s2 = LS(4);
% check if two load calculation is desired
twoFlag = isequal(size(f,2),8);
clearvars p;
%% Amplitudes das ondas
% To facilitate visualisation between calculations, identation inside 'if'
% statements was ommited and comments were written on the side.
Amp = @(f1,a,f2,b,c) 1i * ( ( f1.*exp(1i*k*a) - f2.*exp(1i*k*b) ) ./ ...
                            ( 2*sin( k*(c) ) ) );
A(:,1) = Amp(f(:,1),-l1,f(:,2),-(l1+s1),s1);     % absorptive A
A(:,2) = Amp(f(:,2),l1+s1,f(:,1),l1,s1);         % absorptive B
A(:,3) = Amp(f(:,3),l2+s2,f(:,4),l2,s2);         % absorptive C
A(:,4) = Amp(f(:,4),-l2,f(:,3),-(l2+s2),s2);     % absorptive D
if(twoFlag)
A(:,5) = Amp(f(:,1+4),-l1,f(:,2+4),-(l1+s1),s1); % reflective A 
A(:,6) = Amp(f(:,2+4),l1+s1,f(:,1+4),l1,s1);     % reflective B
A(:,7) = Amp(f(:,3+4),l2+s2,f(:,4+4),l2,s2);     % reflective C
A(:,8) = Amp(f(:,4+4),-l2,f(:,3+4),-(l2+s2),s2); % reflective D
end
%% pressures before and after the sample
p(:,1) = A(:,1)+A(:,2); 							 % absorptive x = 0
p(:,2) = A(:,3).*exp(-1i*k*d) + A(:,4).*exp(1i*k*d); % absorptive x = d
if(twoFlag)
p(:,3) = A(:,5)+A(:,6); 							 % reflective x = 0
p(:,4) = A(:,7).*exp(-1i*k*d) + A(:,8).*exp(1i*k*d); % reflective x = d
end
%% velocities before and after the sample
u(:,1) = (A(:,1)-A(:,2))/(rho*c); 							   % absorptive x = 0
u(:,2) = (A(:,3).*exp(-1i*k*d) - A(:,4).*exp(1i*k*d))/(rho*c); % absorptive x = d
if(twoFlag)
u(:,3) = (A(:,5)-A(:,6))/(rho*c); 							   % reflective x = 0
u(:,4) = (A(:,7).*exp(-1i*k*d) - A(:,8).*exp(1i*k*d))/(rho*c); % reflective x = d
end
%% Transfer Matrix (single load - preferably anechoic)
den = p(:,1).*u(:,2)+p(:,2).*u(:,1);
T_11 = (p(:,2).*u(:,2) + p(:,1).*u(:,1)) ./ den;
T_12 = (p(:,1).^2 - p(:,2).^2) ./ den;
T_21 = (u(:,1).^2 - u(:,2).^2) ./ den;
T_22 = T_11;
T_one = {T_11 T_12; T_21 T_22};
%% Transfer Matrix (two loads)
if(twoFlag)
den = p(:,2).*u(:,4)-p(:,4).*u(:,2);
T_11 = (p(:,1).*u(:,4)-p(:,3).*u(:,2)) ./ den;
T_12 = (p(:,3).*p(:,2)-p(:,1).*p(:,4)) ./ den;
T_21 = (u(:,1).*u(:,4)-u(:,3).*u(:,2)) ./ den;
T_22 = (p(:,2).*u(:,3)-p(:,4).*u(:,1)) ./ den;
T_two = {T_11 T_12; T_21 T_22};
end
%% Transmission Coefficient
tau = @(T,k,d,c,rho) 2*exp(1i*k*d)./ ...
                     (T{1,1}+(T{1,2}/(rho*c))+rho*c*T{2,1}+T{2,2});
%% Trasmission Loss
% The standard first states TL = 10log(1/tau) and later, in the procedures
% section, says that TL = 20log(|1/tau|). The first one is correct,
% according to Bistafa [3] (p. 279) and since the transmission coefficient
% is a power quantity:
% https://en.wikipedia.org/wiki/Level_(logarithmic_quantity)
% The absolute value calculation is maintained due to the coefficient's 
% complex nature
TL = @(tau) 10*log10(abs(1./tau));
TL_one = TL(tau(T_one,k,d,c,rho));
if(twoFlag)
TL_two = TL(tau(T_two,k,d,c,rho));
varargout{1} = TL_two;
end
end