%% utf_TL_tube %%

% Carrega medições ITA, agrega dados necessários e calcula a Perda de
% Transmissão (Transmission Loss - TL) de acordo com a norma ASTM E2611-09
% [1] para utilização de apenas um microfone e referência de sinal no
% falante (i.e. não são realizadas correções das funções de transferência).

% Autor: Luis H. Sant'Ana, com base no script original de José Larcher e
% Matheus Wachholtz e revisado com a norma [1].

% Segundo o trabalho de Larcher e Wachholtz [2], o tubo responde de maneira
% mais consistente entre 400 Hz e 4000 kHz.

% Segundo a norma, a tolerância de temperatura é de +-1.0 °C; tolerância de
% pressão de +-0.5 kPa; tolerância de umidade do ar de +-5 %.

% [1] ASTM E2611-09. Standard Test Method for Normal Incidence
% Determination of Porous Material Acoustical Properties Based on the
% Transfer Matrix Method. 2009.
% [2] LARCHER, J. K.; WACHHOLTZ, M. H. Construção e teste de um tubo de
% impedância para medição de isolamento. Trabalho de Conlusão de Curso,
% Universidade Tecnológica Federal do Paraná - UTFPR. 2018.

% Esquema:
% distâncias:          |<s1>|<l1>|<   l2   >|< s2 >|
% --------------------|M1|--|M2|------------|M3|--|M4|---------------
% FALANTE =)=)=)    ->A     B<-   |AMOSTRA| ->C     D<-        FUNDO
% -------------------------------------------------------------------

% The files must be ordered according to the Microphones, and grouped first
% by the Absorptive ending and then with the Reflective or Open ending.
% This is easily attained with the coding 'M1-4' and 'A', 'R' or 'O'.
% For example:
% A_M1.ita
% A_M2.ita
% ...
% R_M3.ita
% R_M4.ita

% input:
% (mandatory)
% snip: path + snip to search for .ita files from the same sample (string
% or char)
% (optional parameters - name-value pairs)
% d: sample thickness (m) (scalar)
% freqRange: tuplet with lower and upper frequency limits for analysis (Hz)
% (vector)
% tcrop: time interval from the impulse response to be analysed. Remember
% to consider hardware latency. (vector)
% T: room temperature (grau Celsius) (scalar)
% P: static atmospheric presure (kPa) (scalar)

function [TL_one,freqVector,varargout] = utf_TL_tube_setup(snip,varargin)
%% parse
defaultD = 0.02;
defaultFreqRange = [400 4000];
defaultTCrop = [0.013 1.013]; % (a latência de 623 amostras da placa para
% uma taxa de amostragem de 48 kHz corresponde a 0.013 s)
defaultT = 25;
defaultP = 90.8; % considerando 914 m de altitude

p = inputParser;
isNumericScalar = @(x) isnumeric(x) && isscalar(x);
isNumericVectorLength2 = @(x) isnumeric(x) && isvector(x) && ...
                              isequal(length(x),2);
addRequired(p,'snip',@(x) isstring(x) || ischar(x));
addParameter(p,'d',defaultD,isNumericScalar);
addParameter(p,'freqRange',defaultFreqRange,isNumericVectorLength2);
addParameter(p,'tcrop',defaultTCrop,isNumericVectorLength2);
addParameter(p,'T',defaultT,isNumericScalar);
addParameter(p,'P',defaultP,isNumericScalar);
parse(p,snip,varargin{:});

d = p.Results.d;
freqRange = p.Results.freqRange;
tcrop = p.Results.tcrop;
T = p.Results.T;
P = p.Results.P;
clearvars p
%% Tube dimensions (m)
% reference on distance is the sample's face closest to sound source.
% distance between the microphone (source side) closest to the sample and 
% e sample itself.
l1 = 0.15;
% distance between the microphone closest to the sample and the sample
% itself (ending side, same reference)
l2 = 0.15+d;
% distance between microphones themselves, on each sample side
s1 = 0.03;
s2 = s1;
% group distances on a vector
LS = [l1 l2 s1 s2];
%% Fatores ambientais
% Velocidade do som no ar (m/s)
c = utf_speed_of_sound(T);
% Densidade do ar (kg/m^3)
rho = utf_air_density('T',T,'P',P);
%% Carrega dados das medições na faixa de frequências de interesse
% nomes dos arquivos de interesse na pasta de interesse
files = dir(snip);
% junta caminho absoluto com nome dos arquivos de interesse
files = string({files.folder}) + "\" + string({files.name});
% carrega os arquivos .ita, junta em um, corta no tempo
RI = ita_time_crop(ita_merge(ita_read(files)),tcrop);
% índices das frequências de interesse
idx = ( freqRange(1) <= RI.freqVector ) & ( RI.freqVector <= freqRange(2) );
% não aceita selecionar faixa com valores lógicos em apenas um passo para o
% freqVector (vetor de frequências)
freqVector = RI.freqVector;
freqVector = freqVector(idx);
% número de onda
k = 2*pi*freqVector*c^(-1); % (rad/m)
% coeficientes espectrais
f = RI.freq(idx,:);
% apaga objeto ita
clearvars RI
%% TL calculation
if(isequal(size(f,2),8))
    [TL_one,TL_two] = utf_TL_tube_calc(f,k,d,c,rho,LS);
    varargout{1} = TL_two;
else
    if(isequal(size(f,2),4))
        TL_one = utf_TL_tube_calc(f,k,d,c,rho,LS);
    else
        error("São necessárias quatro medições para cálculo com uma carga "+...
               "ou 8 medições para cálculo também com duas cargas.")
    end
end
end