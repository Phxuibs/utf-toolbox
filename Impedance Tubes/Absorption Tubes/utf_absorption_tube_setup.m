%% utf_tube_setup %%

% Setup parameters for the absorption tube.

% This code is used in combination with utf_absorption_tube_measurements!

% Author: Luis H. Sant'Ana

% input (optional - in order):
% tube: the tube being used. May be "Busolo" or "Garron"; default:
% "Busulo".
% It's for the tube's microphones positioning and automatic default
% freqRange for each tube.
% freqRange: tuplet with frequency limits for analysis; default: [200 2700]

function UTFPar = utf_absorption_tube_setup(varargin)
%% parse
defaultTube = "Busulo";
defaultFreqRange = [200 2700];
p = inputParser;
p.addOptional('tube',defaultTube,@(x) ( strcmpi(x,"Busulo") || strcmpi(x,"Garron") ) && isscalar(x));
p.addOptional('freqRange',defaultFreqRange,@(x) isnumeric(x) && isvector(x) && isequal(length(x),2));
parse(p,varargin{:});
tube = p.Results.tube;
freqRange = p.Results.freqRange;
%% setup tube
% Take a look at ITA's tube default parameters from the one that's closest
% to ours:
ITAPar = ita_impedance_tube_setup('Small Kundt''s Tube at ITA Mics123');
% Now we'll do our own. Notice there are parameters we don't know how to
% setup, then we're leaving the default ones!
UTFPar = ITAPar;
UTFPar.name = 'Absorption tube at UTFPR';
% looking into ita_impedance_tube_calculation, we have the following
% distances scheme: [probe-mic1 mic1-mic2 mic1-mic3] (in metres)
switch tube
case "Busulo"
    UTFPar.micPositions = [0.25 0.05 0.09];
    UTFPar.freqRange = [200 2700];
case "Garron"
    UTFPar.micPositions = [0.25 0.02 0.045];
    UTFPar.freqRange = [500 6300];
otherwise
    error("This statement could not happen!");
end
% frequency range analysis (Hz)
if(isequal(nargin,2))
    UTFPar.freqRange = freqRange;
end
% for a hardware latency of 623 samples at 48 kHz, the IR starts at 0.013 s
UTFPar.windowTime = [0.013 1.013];
end