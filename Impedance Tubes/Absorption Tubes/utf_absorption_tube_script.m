%% utf_absorption_tube_measurements %%

% This script will guide you through transfer function measurements and
% absorption coefficient calculation using the "absorption tube" at UTFPR.

% Author: Luis H. Sant'Ana
%% Setup
% Setup tube parameters for coefficient calculation (using default options)
par = utf_absorption_tube_setup;
% Setup transfer function measurement object.
% This is set in combination with the tube parameters from function
% utf_absorption_tube_setup
% For example, latency samples is set to 0, but time windowing is set to
% start at 0.013 s.
MS = utf_ITA_MS('freqRange', par.freqRange, 'numAverages', 3, ...
                'samplingRate', 48000, 'latencySamples', 0);
% Environmental parameters: temperature, pressure, speed of sound, air
% density.
% Quick check on temperature and pressure on Curitiba:
% http://www.simepar.br/prognozweb/simepar/dados_estacoes/25264916
% Temperature (degrees Celsius)
env.T = 20;
% Atmospheric pressure (kPa)
env.P = 90.8;
% Speed of sound
env.c = utf_speed_of_sound(env.T);
% Air density
env.rho = utf_air_density('T',env.T,'P',env.P);
% Path to save files
path = "./";
% Sample name
sample = "sampleA";
%% Run measurements - mic 1 (closest to sample)
mic1 = MS.run;
ita_write(path + sample + "_mic1");
%% mic 2 (middle)
mic2 = MS.run;
ita_write(path + sample + "_mic2");
%% mic 3 (closest to speaker)
mic3 = MS.run;
ita_write(path + sample + "_mic3");
%% Calculations
% Merge results
rawMeasurements = ita_merge(mic1,mic2,mic3);
clearvars mic1 mic2 mic3
% Calculate coefficients
% from ita_impedance_tube_gui:
% (an options object is also set, just like on the ITA code, to avoid any
% possible conflict with the par object)
options.doSineMerge = par.doSineMerge;
[Z, R] = ita_impedance_tube_calculation(rawMeasurements, env.c, env.rho,...
           par.micPositions, par.freqRange, par.dampingDimension, ...
           par.crossingFreq, par.windowTime, par.timeShift, ...
           options);
alpha = 1 - abs(R)^2;
% Save full workspace
save(path+sample);