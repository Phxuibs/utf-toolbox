/* Utilização do módulo MPU6050 para aquisição de dados de aceleração com integração com MATLAB. */
// Programa desenvolvido por alunos de pós-graduação da Universidade Tecnológica Federal do Paraná - UTFPR
// Autores:
// Luis Henrique Sant'Ana;
// José Henrique Kleinübig Larcher
// Orientador:
// Márcio Henrique de Avelar Gomes
// Baseado no programa de FILIPEFLOP, que fora baseado no programa original de JohnChi

// Documentos importantes referenciados aqui (válido lembrar que o MPU6050 analisa também temperatura e acelerações de rotação):
// Especificação do produto, revisão 3.4 (abreviado aqui para "PS");
// Mapa de registros, revisão 4.0 (abreviado aqui para "RM");

/* Pinagem */
// SDA => pino A4; SCL => pino A5.

/* Bibliotecas */
// Carrega a biblioteca Wire, para comunicação I2C
#include<Wire.h>
// Carrega a biblioteca EEPROM, para armazenar os valores de taxa de amostragem
#include<EEPROM.h>
// printf, scanf, NULL
#include <stdio.h>
// malloc, free, rand
#include <stdlib.h>

/* Setup do Operador */
// Faixa de medição (0 significa range de |2|g; 8, range de |4|g; 16, |8|g; 24, |16|g. Se não for um desses valores, é ajustado pra 0)
#define RANGE 0
// O número de amostras a serem adquiridas é calculado a partir do tempo desejado de medição e da taxa de amostragem da última medição:
// n_samples = tempo de medição * taxa de amostragem da última medição;
// Divide-se em duas medições: uma de "setup" e outra de "interesse".
// Tempo para a medição de setup (segundos)
#define SETUP_MEASUREMENT_LENGTH 1
// Tempo para a medição de interesse (segundos)
#define MEASUREMENT_LENGTH 3

/* Defines Gerais */
// Endereço I2C do MPU6050
#define MPU 0x68
// Baud rate para comunicação com MATLAB
#define BAUD_RATE 38400
// Delay entre aquisições, em microssegundos, que é determinante para a taxa de amostragem
// Aparentemente, há outra forma de retardar a aquisição e evitar leitura de mesmo dado (a taxa de comunicação é maior do que a taxa de medição do acelerômetro) utilizando o registro I2C_MST_DELAY_CTRL (p. 37 do RM).
#define DELAY 1000 // microssegundos
// Endereço na EEPROM que armazena a taxa de amostragem da última medição
#define eeAddress 0

/* A seguir, são declarados os registradores que serão configurados e utilizados.
Os valores para cada registrador são declarados em seguida.
Note ainda que variáveis correspondentes aos registradores são declaradas depois.
Isso é feito, usualmente, para ler os valores diretamente dos registradores por motivos de confirmação de configuração. */

/* Registros do MPU */
// Sincronização externa e frequência de corte do Digital Low-Pass Filter (DLPF), que configura também o clock base (clock do acelerômetro é sempre 1kHz).
// bit7 | bit6 | bit5-bit3 (EXT_SYNC_SET) | bit2-bit0 (DLPF_CFG)
// A possibilidade de valor 7 do DLPF_CFG (3 LSB do registro CONFIG) foi descartada, por ser "reservado" (página 13 do RM).
// Como não está sendo utilizada a sincronização externa, considerou-se então os únicos casos possíveis para valores de registro DLPF_CFG maiores ou iguais a 0 e menores do que 7;
#define CONFIG 0x1A
// Valor a ser registrado em CONFIG, que regula sincronização externa e DLPF
#define CONFIG_VALUE 0
// Registrador para o divisor do clock base para "taxa de amostragem do MPU"
// Se DLPF_CFG for 0 ou 7 (veja registro CONFIG), clock base = 8kHz; se adverso, clock base = 1kHz (pg. 12 do RM).
// A taxa de amostragem do registrador dos eixos, independentemente dessa taxa de amostragem selecionada, equivale a 1kHz (pg. 12 do RM).
// Caso a "taxa de amostragem do MPU" seja maior do que 1kHz, pode ocorrer de valores repetidos do acelerômetro serem enviados.
#define SMPLRT_DIV 0x19
// Divisor de clock para operações do MPU; será válido quando o DLPF for setado na sua frequência máxima (260Hz)
#define SMPLRT_DIV_VALUE 7 // soma 1 no chip
// Registrador que habilita self-test também é o que seleciona a faixa de medição ("STE" aqui é "self-test enable"). Quanto à faixa de medição de aceleração, seu valor é o RANGE, presente nas configurações do usuário.
#define ACCEL_CONFIG 0x1C
// 4 LSB são o clock do I2C; para 400kHz, deve ser 13 (B1101)
#define I2C_MST_CTRL 0x24
// Endereço dos registradores dos eixos do acelerômetro para medição
#define X_REG 0x3B
#define Y_REG 0x3D // não necessário, presente apenas para explicitação
#define Z_REG 0x3F // não necessário, presente apenas para explicitação

/* Variáveis */
// Variáveis que armazenam os valores dos registradores correspondentes
byte smplrt_div = 0, config_reg = 0, accel_config = 0, acc_reg = 0, i2c_mst_ctrl = 0;
// Indica o tipo de medição: 0 = setup; 1 = medição de interessese.
char measurement_type = '0';
// Aceleração do eixo de interesse (aceleração medida), aceleração do eixo de interesse (self-test), sensibilidade (página 13 do PS), variáveis para iterações, número de amostras necessárias na medição;
int acc = 0, sensitivity = 0, i = 0, j = 0, n_samples = 0;
// Variáveis utilizadas para calcular o tempo de medição
unsigned long start_time = 0, end_time = 0;
// Separador de seção para o display
char section[] = "%%%%%%%%%%%%%%%%%%%%";
// Taxa de amostragem medida, tempo de medição, range (float, porque é retorno de uma exponenciação - simplesmente fazer cast não pareceu funcionar)
float sampling_rate = 0.0, elapsed_time = 0.0, range = 0.0;

void setup()
{
  // Inicia comunicação serial com MATLAB
  Serial.begin(BAUD_RATE);
  // Mensagem de descarte
  Serial.println(F("Discard this first message."));

  /* INÍCIO DO RECEBIMENTO DE SETUP DE USUÁRIO VIA MATLAB */
  // Espera mensagem com tipo de medição para continuar
	Serial.println(section);
  Serial.println("State the measurement type ('0' = setup; '1' = measurement of interest)");
  while(Serial.available() == 0)
  {
  }
  // Então lê o tipo de medição
  measurement_type = Serial.read();
  /* FIM DO RECEBIMENTO DE SETUP DO USUÁRIO VIA MATLAB */
  
  // Inicializa a Wire
  Wire.begin();
  // Inicializa o MPU6050
  Wire.beginTransmission(MPU);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

  // Realiza configuração dos registradores do MPU
  get_config_reg();

  // Envia variáveis de interesse ao MATLAB
  Serial.println(range);
  Serial.println(sensitivity);
  Serial.println(n_samples);

  // Espera um milissegundo e inicia a medição
  delay(1);
  start_time = micros();
  for(i = 0; i < n_samples; i++)
  {
		acc_reg = X_REG;
		for(j = 1; j <= 3; j++)
		{
			get_acc();
			acc_reg = acc_reg + 2;
			Serial.println(acc);
		}
		delayMicroseconds(DELAY);
  }
  end_time = micros();

  // Calcula tempo de duração da medição e apresenta ao operador
  elapsed_time = float(end_time-start_time)/1000000;
  Serial.print(F("Measurement time length (s) = "));Serial.println(elapsed_time,6);
  // Calcula taxa de amostragem da medição e apresenta ao operador
  sampling_rate = float(n_samples)/elapsed_time;
  Serial.print(F("New measured sampling rate (Hz) = "));Serial.println(sampling_rate);
  // Armazena o valor de taxa de amostragem na memória não volátil
  EEPROM.put(eeAddress,sampling_rate);
  // Indica fim de mensagens para display
  Serial.println(F("fim"));
  // Envia variáveis de interesse ao MATLAB
  Serial.println(elapsed_time,6);
  Serial.println(sampling_rate);
}

void loop()
{}

void get_acc()
{
  Wire.beginTransmission(MPU);
  // Iniciando com registrador acc_reg (target);
  Wire.write(acc_reg);
  Wire.endTransmission(false);
  // Solicita os dados do sensor
  // Wire.requestFrom(address, number of bytes, stop ('true' to send "stop" after message, ending connection; 'false' to continue the connection))
  Wire.requestFrom(MPU,2,true);
  // Armazena o valor dos sensores nas variáveis correspondentes
  acc = Wire.read()<<8|Wire.read(); // lê os dois bytes correspondentes, primeiro o alto, depois o baixo.
}

/* Configuração dos Registradores do MPU */
void get_config_reg()
{
	// Serial.print(F("Baud_rate = "));Serial.println(BAUD_RATE);
	
  // CONFIG
  Serial.println(section);
  Serial.println(F("CONFIG => external synchronisation and Digital Low-Pass Filter"));
  Serial.println(F("0 states the filter is adjusted for 260Hz (pg. 13 of RM)"));
  Wire.beginTransmission(MPU);
  Wire.write(CONFIG);
  Wire.write(CONFIG_VALUE);
  Wire.endTransmission(true);
  Wire.beginTransmission(MPU);
  Wire.write(CONFIG);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,1,true);
  config_reg = Wire.read();
  Serial.print(F("CONFIG (BIN): "));Serial.println(config_reg,BIN); // BIN apresenta o byte inteiro

  // Taxa de Amostragem
  // Serial.println(section);
  Serial.println(F("SMPLRT_DIV => sample rate divider"));
  Serial.println(F("sampling_rate = clock_base / (1 + SMPLRT_DIV)"));
  Serial.println(F("If DLPF_CFG is 0 or 7 (as part of CONFIG), base clock is 8kHz; otherwise, base clock is 1kHz (pg. 12 of RM)"));
  Serial.println(F("The sampling rate of the axes registers, independently from this (MPU) sampling rate, is equal to 1kHz (pg. 12 of RM)"));
  if(config_reg == 0)
  {
    Wire.beginTransmission(MPU);
    Wire.write(SMPLRT_DIV);
    // Seleciona a divisão de clock por SMPLRT_DIV_VALUE
    Wire.write(SMPLRT_DIV_VALUE); // é somado a 1 no chip;
    Wire.endTransmission(true);
  }
  else
  {
    Wire.beginTransmission(MPU);
    Wire.write(SMPLRT_DIV);
    // Seleciona a divisão de clock por 1, resultando em um sampling rate de 1kHz;
    Wire.write(0); // é somado a 1 no chip;
    Wire.endTransmission(true);
  }
  Wire.beginTransmission(MPU);
  Wire.write(SMPLRT_DIV);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,1,true);
  smplrt_div = Wire.read();
  Serial.print(F("SMPLRT_DIV (BIN): "));Serial.println(smplrt_div,BIN);
  Serial.print(F("Subsequent sampling rate: "));
  if(config_reg == 0)
  {
    Serial.print(float(8/(1+smplrt_div)));
  }
  else
  {
    Serial.print(float(1/(1+smplrt_div)));
  }Serial.println(F("kHz"));

  // I2C Master Control
  // Serial.println(section);
  Serial.println(F("I2C_MST_CTRL => 4 LSB select I2C clock; for 400kHz, should be 13 (B1101)"));
  // Lê o registrador I2C_MST_CTRL
  Wire.beginTransmission(MPU);
  Wire.write(I2C_MST_CTRL);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,1,true);
  i2c_mst_ctrl = Wire.read();
  // Escreve 1101 nos 4 LSB do registrador de clock do I2C
  i2c_mst_ctrl = (i2c_mst_ctrl & B11110000) | B00001101;
  Wire.beginTransmission(MPU);
  Wire.write(I2C_MST_CTRL);
  Wire.write(i2c_mst_ctrl);
  Wire.endTransmission(true);
  // Lê e apresenta os valores do registrador
  Wire.beginTransmission(MPU);
  Wire.write(I2C_MST_CTRL);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,1,true);
  i2c_mst_ctrl = Wire.read();
  Serial.print(F("I2C_MST_CTRL (BIN): "));Serial.println(i2c_mst_ctrl, BIN);

  // RANGE e ACCEL_CONFIG
  // Serial.println(section);
	// ACCEL_CONFIG => STE_Z STE_Y STE_X RANGE_MSB RANGE_LSB N/A N/A N/A
  Serial.println(F("ACCEL_CONFIG => self-test triggers and range;"));
  Serial.println(F("0 implies in a range of |2|g; 8, range of |4|g; 16, |8|g; 24, |16|g;"));
  Serial.println(F("other values configure self-test triggers"));
  // Se for um desses valores, significa que os bits que habilitam self-test já serão desabilitados
  if(!( (RANGE == 0) || (RANGE == 8) || (RANGE == 16) || (RANGE == 24) ))
  {
    accel_config = 0;
  }
  else
  {
    accel_config = RANGE;
  }
  // Escreve valor no registrador ACCEL_CONFIG
  Wire.beginTransmission(MPU);
  Wire.write(ACCEL_CONFIG);
  Wire.write(accel_config);
  Wire.endTransmission(true);
  // Lê e apresenta valor do registrador ACCEL_CONFIG, juntamente com o range selecionado
  Wire.beginTransmission(MPU);
  Wire.write(ACCEL_CONFIG);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,1,true);
  accel_config = Wire.read();
  Serial.print(F("ACCEL_CONFIG (BIN): "));Serial.println(accel_config, BIN);
  range = pow(2,((accel_config & B00011000) >> 3)+1);
  Serial.print(F("Range subsequente: |"));Serial.print(range);Serial.println(F("|g"));

  // Terminam as seções que manipulam registradores do MPU;
  // Começam as seções que calculam e ajustam alguns parâmetros, mas sem mexer nos registradores do MPU.

  // Sensibilidade
  Serial.println(section);
  Serial.println(F("Sensitivity"));
  if((accel_config & B00011000) >> 3 == 0)
  {
    sensitivity = pow(2,14);
  }
  else
  {
    sensitivity = pow(2,14-((accel_config & B00011000)/8));
  }
  Serial.print(F("sensitivity: |"));Serial.print(sensitivity);Serial.println(F("|LSB/g"));

  // n_samples - determinado a partir da taxa de amostragem da última medição, que fora salvo na EEPROM
  // Serial.println(section);
  EEPROM.get(eeAddress, sampling_rate);
  Serial.print(F("Previous sampling rate (Hz): "));Serial.println(sampling_rate);
  Serial.print("Stated type of measurement: ");
  Serial.println(measurement_type);
  switch (measurement_type)
  {
    case '0':
      n_samples = SETUP_MEASUREMENT_LENGTH * sampling_rate;
      Serial.print(F("Therefore, number of samples for the setup: "));
    break;
    case '1':
      n_samples = MEASUREMENT_LENGTH * sampling_rate;
      Serial.print(F("Therefore, number of samples for the measurement of interest: "));
    break;
    default:
      Serial.print(F("Error setting up measurement type. Its value must be either '0' or '1'."));
      while(1)
    break;
  }
  Serial.println(n_samples);

  // Indica fim de mensagens para display
  Serial.println(F("fim"));
}
