%% utf_MPU_script %%
% Leitura de dados de acelera��o com Arduino e m�dulo MPU6050
% Programa desenvolvido por alunos de p�s-gradua��o da Universidade
% Tecnol�gica Federal do Paran� - UTFPR
% Autores:
% Luis Henrique Sant'Ana;
% Jos� Henrique Klein�big Larcher
% Orientador:
% M�rcio Henrique de Avelar Gomes
%% Setup
clear;
close all;
clc;
% Tamanho do plot de acordo com tamanho da tela, em cent�metros
% [largura altura]
plotSize = [35 19];
% Caminho para a pasta para salvar os arquivos de sa�da .mat, .xls e .pdf
path = "./Measurements/";
% Nome para salvar os arquivos .mat, .xls e .pdf
filename = "Exemplo";
% Cria um objeto anexado a uma porta serial
% (para saber qual porta est� sendo utilizada, utilize o Arduino IDE!)
port = serial('COM3');
% Baud rate deve ser semelhante ao que estiver no c�digo do Arduino
port.BaudRate = 38400;
% Se � uma medi��o de setup (se falso, � medi��o de interesse)
mpu.setupFlag = true;
%% Execu��o do processo de medi��o em si
mpu = utf_MPU_measurement(port,mpu);
%% Cria��o dos objetos ITA e...
% se for medi��o de setup, calcula resultante e erro rms e salva arquivo;
% sen�o, aplica filtro por bandas e calcula valores RMS, incluindo
% resultante
if(mpu.setupFlag)
    accITA = utf_MPU_ITA(mpu,path,filename);
else
    [accITA,resultante,filtered,rms] = utf_MPU_ITA(mpu,path,filename);
end
%% Salva arquivos .mat e .xls
if(~mpu.setupFlag)
    time = accITA(1).timeVector;
    freq = ita_ANSI_center_frequencies([1 80],3).';
    utf_MPU_saves(time,freq,mpu,accITA,resultante,filtered,rms,path,filename);
end
%% Plots - salva arquivos .pdf
if(~mpu.setupFlag)
    [time,freq] = utf_MPU_plots(accITA,rms,path,filename,plotSize);
end