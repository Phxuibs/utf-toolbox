/* Testar Comunicação com MATLAB */

#include<string.h>

byte b = 8;
int i = 8;
float f = 8.0;
double d = 8.0;
char c = '8';
char c_com_branco[] = "0 8";
char s[] = "0 8";

void setup()
{
  Serial.begin(38400);
  // Primeira mensagem parece sempre enviar com lixo, logo é descartada.
  Serial.println("Ready");
  Serial.println("Ready");
}

void loop()
{
  Serial.println(b);
  Serial.println(i);
  Serial.println(f);
  Serial.println(d);
  Serial.println(c);
  Serial.println(c_com_branco);
  Serial.println(s);
}
