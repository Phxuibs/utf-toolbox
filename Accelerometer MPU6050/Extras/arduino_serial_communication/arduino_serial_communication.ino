/* Medição de tempo de transferência de um valor inteiro via porta serial */

// Vetor com valores para serem enviados via porta serial
// (um int vai de -32.768 até 32.768 - https://docs.arduino.cc/language-reference/pt/vari%C3%A1veis/data-types/int/)
int vetor[] = {0,1,2,3,4,10,11,12,13,14,-10,-11,-12,-13,-14,100,101,102,103,104,1000,1001,1002,1003,1004,10000,10001,10002,10003,10004};
// variável para iteração
int i = 0;
// Variáveis para armazenar os instantes de começo e final de envio
unsigned long start_time = 0, end_time = 0;
// Variável para armazenar o tempo de envio
float elapsed_time[30];

void setup ()
{
	Serial.begin(38400);
  Serial.println("Descarte");
	
	for(i = 0; i < 30; i++)
	{
		start_time = micros();
		Serial.println(vetor[i]);
		end_time = micros();
    elapsed_time[i] = end_time - start_time;
	}
  for(i = 0; i < 30; i++)
	{
		Serial.println(elapsed_time[i]);
	}
}

void loop()
{
}