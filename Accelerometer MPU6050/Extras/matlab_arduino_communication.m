%% Testar Comunica��o com Arduino %%

clear;
clc;

% Cria um objeto anexado a uma porta serial
% (ver porta utilizando o Arduino IDE).
ard = serial('COM9');
ard.BaudRate = 38400;

% Para ler e escrever no instrumento, este deve ser aberto.
fopen(ard);
% ard.status % aparecer� aberto

% A primeira mensagem parece sempre enviar com lixo, logo descartar-se-� a
% primeira
ready = fscanf(ard); %#ok<NASGU>
ready = fscanf(ard);
disp(ready);

% Testes com diferentes tipos de vari�veis do Arduino.
% Todos s�o lidos como .txt (char), sendo a convers�o realizada pelo matlab

% teste com byte
b = fscanf(ard,'%d'); % com sinal, base 10
disp(b);
% teste com int
i = fscanf(ard,'%d');
disp(i);
% teste com float
f = fscanf(ard,'%f'); % ponto flutuante
disp(f);
% teste com double
d = fscanf(ard,'%f');
disp(d);
% teste com char
c = fscanf(ard,'%c');
disp(c);
% teste com char com espa�o em branco
c_com_branco = fscanf(ard,'%c');
disp(c_com_branco);
% teste com string
s = fscanf(ard,'%s'); % string (ignora espa�os em branco)
disp(s);

% Parar conex�o com o Arduino
fclose(ard);
%  ard.status % aparecer� fechado