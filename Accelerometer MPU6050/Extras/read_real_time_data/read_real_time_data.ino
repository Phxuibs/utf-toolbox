/* Apresenta valores de aceleração em cada eixo e a resultante, em g */

// Código baseado no MPU6050 Raw, exemplo de utilização da biblioteca do Electronic Cats (precisa da sua biblioteca instalada)
// https://github.com/ElectronicCats/mpu6050/wiki

// Utilize o código get_offset para obter valores de offset para serem inseridos neste código!
// O offset é o mesmo independentemente do range de medição escolhido!

// Verificar o código original, que possui a opção de envio binário, se desejada maior velocidade!

#include "I2Cdev.h"
#include "MPU6050.h" // Biblioteca do Electronic Cats

/* MPU6050 default I2C address is 0x68*/
MPU6050 mpu;
//MPU6050 mpu(0x69);         //Use for AD0 high
//MPU6050 mpu(0x68, &Wire1); //Use for AD0 low, but 2nd Wire (TWI/I2C) object.

#define OUTPUT_READABLE_ACCELGYRO

int16_t ax, ay, az;
bool blinkState;

// Minhas variáveis
unsigned long current_time;
float gx, gy, gz, sensitivity, resultante;

void setup() {
  /*--Start I2C interface--*/
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin(); 
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
    Fastwire::setup(400, true);
  #endif

  Serial.begin(38400); //Initializate Serial wo work well at 8MHz/16MHz

  /*Initialize device and check connection*/ 
  Serial.println("Initializing MPU...");
  mpu.initialize();
  Serial.println("Testing MPU6050 connection...");
  if(mpu.testConnection() ==  false){
    Serial.println("MPU6050 connection failed");
    while(true);
  }
  else{
    Serial.println("MPU6050 connection successful");
  }

  /* Use the code below to change accel/gyro offset values. Use MPU6050_Zero to obtain the recommended offsets */ 
  Serial.println("Updating internal sensor offsets...\n");
  mpu.setXAccelOffset(0); //Set your accelerometer offset for axis X
  mpu.setYAccelOffset(0); //Set your accelerometer offset for axis Y
  mpu.setZAccelOffset(0); //Set your accelerometer offset for axis Z
  /*Print the defined offsets*/
  Serial.print("\t");
  Serial.print(mpu.getXAccelOffset());
  Serial.print("\t");
  Serial.print(mpu.getYAccelOffset()); 
  Serial.print("\t");
  Serial.print(mpu.getZAccelOffset());
  Serial.print("\n");

  /*Configure board LED pin for output*/ 
  pinMode(LED_BUILTIN, OUTPUT);

  // Meu setup
  // Ajustar range (0=2g, 1=4g)
  mpu.setFullScaleAccelRange(4);
  // resolution = mpu.get_acce_resolution(); // parece estar com problema! resolução para 2g e 4g são iguais!
  // sensibilidade // range 2: 16384; range 4: 8192
  sensitivity = 8192.0;//*2.0;
  // Desabilitar self-test (pois altera o valor de saída e, quando habilitado, permanece habilitado mesmo após reinicialização (toggle))
  mpu.setAccelXSelfTest(false);
  mpu.setAccelYSelfTest(false);
  mpu.setAccelZSelfTest(false);
  // Preferi deixar um tempo de delay entre a leitura dos dados e a habilitação do self-test, e também após a habilitação e desabilitação
  // (lembrando que a taxa de amostragem do acelerômetro em si é de 1kHz).
  delay(110);
}

void loop() {
  // Pegar quanto tempo se passou desde que o Arduino foi inicializado
  current_time = millis();

  // Read raw acceldata
  mpu.getAcceleration(&ax, &ay, &az);
	
	// Convert to g
	gx = float(ax)/sensitivity;
	gy = float(ay)/sensitivity;
	gz = float(az)/sensitivity;
  // Calcular resultante
  resultante = pow(pow(gx,2.0)+pow(gy,2.0)+pow(gz,2.0),0.5);
	
  // Apresentar valores
	#ifdef OUTPUT_READABLE_ACCELGYRO
    Serial.print("a:\t");
    Serial.print(gx); Serial.print("\t");
    Serial.print(gy); Serial.print("\t");
    Serial.print(gz); Serial.print("\t");
    Serial.println(resultante);
  #endif
  // Apresentar tempo para realização do processo
  Serial.print("O processo levou ");Serial.print(millis()-current_time);Serial.println(" milissegundos");

  /*Blink LED to indicate activity*/
  blinkState = !blinkState;
  digitalWrite(LED_BUILTIN, blinkState);

  // Simples delay, para legibilidade dos valores de saída
  delay(110);
}
