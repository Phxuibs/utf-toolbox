%% utf_MPU_messages_display %%
% Lê mensagens do Arduino e apresenta no display até receber uma mensagem
% indicativa de finalização
% input: objeto serial, mensagem esperada para finalização (char).
function utf_MPU_messages_display(ard,msg)
flag = false;
while(~flag)
    ard_message = fscanf(ard);
    if(isequal(ard_message(1:length(ard_message)-2),msg))
        flag = true;
    else
        disp(ard_message);
    end
end
end