%% utf_MPU_plots %%
% Cria e salva plots de aceleração ao longo do tempo e valores RMS para
% cada banda de frequência.
% input:
% accITA = objeto ITA (ou array de objetos ITA) com as medições de
% aceleração (não filtrado)
% rms = valores rms, com bandas de frequência ao longo das linhas e eixos,
% se for medição triaxial, ao longo das colunas
% triAxial = se a medição é triaxial (booleano)
% filename = nome que deve estar presente ao salvar arquivos (string)
% plotSize = tamanho dos plots, em centímetros [largura altura]
% output:
% Retorna os vetores de tempo e frequência, para conveniência
function [time,freq] = utf_MPU_plots(accITA,rms,path,filename,plotSize)
%% setup
% vetor de tempo
time = accITA(1).timeVector;
% vetor de frequências
freq = ita_ANSI_center_frequencies([1 80],3);
%% plot do sinal no tempo
h = figure(1);
markers = ["-","-","-"];
colors = ["azul","verde","vermelho"];
hold on
for i = 1 : 3
    plot(time,accITA(i).time,markers(i),'LineWidth',1.5,'Color',utf_color(colors(i)));
end
hold off
ax = utf_axes(gca,32);
ax.XTick = 0 : 0.5 : time(end);
ax.XLim = [time(1) time(end)];
grid on;
legend('Eixo X','Eixo Y','Eixo Z','Location','northeastoutside',...
       'interpreter','latex');
xlabel('Tempo (s)');
ylabel("Acelera\c{c}\~{a}o ($m/s^{2}$)");
title(filename + " - Acelera\c{c}\~{a}o");
utf_adjust_plot_fine(h,path + filename + " - Aceleração",plotSize);
%% plot do rms
h = figure(2);
markers = ["-o","-.s",":d"];
hold on
for i = 1 : 3
    plot(freq,rms(:,i),markers(i)','LineWidth',1.5,'Color',utf_color(colors(i)),...
         'MarkerFaceColor',utf_color(colors(i)));
end
ax = utf_axes(gca,32);
ax.XScale = 'log';
ax.XTick = freq;
ax.XLim = [freq(1) freq(end)];
grid on;
legend('Eixo X','Eixo Y','Eixo Z','Location','northeastoutside',...
       'interpreter','latex');
xlabel("Bandas de 1/3 oitava (Hz)");
ylabel("Acelera\c{c}\~{a}o RMS ($m/s^{2}$)");
title(filename + " - Acelera\c{c}\~{a}o RMS");
utf_adjust_plot_fine(h,path + filename + " - Aceleração RMS",plotSize);
end