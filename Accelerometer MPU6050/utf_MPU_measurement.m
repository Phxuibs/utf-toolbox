%% utf_MPU_measurement %%
% Execução do processo de medição com o MPU 6050 e Arduino
% input: objeto de porta serial
% output:
% mpu, um objeto com as seguintes propriedades:
% acc = valores de aceleração (g)
% samplingRate = taxa de amostragem (Hz);
% elapsedTime = tempo de medição (efetivo) ();
% nsamples = quantidade de amostras em cada medição;
% setupFlag = se a medição é uma medição de setup (i.e. para correção
% posterior devido à orientação do acelerômetro) (boolean)
% fes = fator de escala de sensibilidade ()
% range = faixa de valores de medição (g)
function mpu = utf_MPU_measurement(port,mpu)
% Para ler e escrever no instrumento, este deve ser aberto
fopen(port);
% ard.status % aparecerá aberto
% A primeira mensagem parece sempre enviar com lixo, logo descartar-se-á a
% primeira
message = fscanf(port); %#ok<NASGU>
clearvars message;
% Informa ao Arduino qual o tipo de medição
if(mpu.setupFlag)
    fprintf(port,'0');
else
    fprintf(port,'1');
end
% Lê mensagens para apresentação ao operador
utf_MPU_messages_display(port,'fim');
% Recebe variáveis de interesse
mpu.range = fscanf(port, '%f');
mpu.sensitivity = fscanf(port,'%d');
mpu.nsamples = fscanf(port,'%d');
% Aloca variáveis para os dados de aceleração
acc = zeros(mpu.nsamples,3);
% Lê dados de aceleração
for i = 1 : mpu.nsamples
    for j = 1 : 3
	    acc(i,j) = fscanf(port, '%d');
    end
end
% Lê mensagens para apresentação ao operador
utf_MPU_messages_display(port,'fim');
% Recebe elapsed_time e sampling_rate
mpu.elapsedTime = fscanf(port,'%f');
mpu.samplingRate = fscanf(port,'%f');
% Fecha conexão com a porta
fclose(port);
%  ard.status % aparecerá fechado
% Ajusta os valores de aceleração de acordo com a sensibilidade
mpu.acc = acc/mpu.sensitivity;
end