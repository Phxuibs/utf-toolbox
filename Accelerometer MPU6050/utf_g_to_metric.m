%% utf_g_to_metric %%
% Retorna a relação da aceleração da gravidade em g para o sistema métrico
function ratio = utf_g_to_metric()
    ratio = 9.806649999788;
end