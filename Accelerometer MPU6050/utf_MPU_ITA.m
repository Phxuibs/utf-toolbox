%% utf_MPU_ITA %%
% Criação dos objetos ITA após medição com o acelerômetro.
% Se for uma medição de setup, salva os dados de medição;
% se for medição de self-test, calcula o padrão de fábrica (factory trim),
% também o chamado "resultado do autoteste" e faz a avaliação de autoteste;
% senão, filtra e calcula valores RMS.
% input:
% mpu = objeto mpu retornado pela função utf_MPU_measurement();
% filename = nome para ser colocado ao salvar arquivos (string)
% output:
% accITA = objeto ITA com os dados de aceleração.
% varargout:
% filtered = objeto ITA com os dades de medição filtrados por banda.
% rms = valores rms por banda de frequência.
function [accITA,varargout] = utf_MPU_ITA(mpu,path,filename)
%% Criação de objetos ITA, salva setup ou aplica correção de setup
% Cria objeto ITA
accITA = itaAudio;
accITA.samplingRate = mpu.samplingRate;
accITA.trackLength = mpu.elapsedTime;
% "Triplica" o objeto ITA
% Importante! Objetos ITA em array não podem ser acessados por
% interface - mas o podem por código. Eles, e seus dados, estão lá!
% Está sendo dessa forma para facilitar a posterior filtragem.
accITA = repmat(accITA,[3 1]);
% Ao salvar os valores de aceleração no objeto ITA, faz conversão de g
% para o sistema métrico (m/s^2)
for eixo = 1 : 3 % eixos X, Y e Z
    accITA(eixo).time = mpu.acc(:,eixo) * utf_g_to_metric();
end
% Se for medição do tipo setup
if(mpu.setupFlag)
    % Calcula a resultante
    resultante = sqrt(accITA(1).time.^2 + accITA(2).time.^2 + accITA(3).time.^2);
    % Calcula o erro sistemático esperado
    erro = (resultante-utf_g_to_metric())/utf_g_to_metric() * 100;
    % Salva arquivo de setup
    save(path + filename + " - setup.mat", 'accITA','resultante','erro');
else % senão
    % Carrega arquivo de setup
    set = load(path + filename + " - setup.mat");
    % Aplica correção ao erro sistemático
    for eixo = 1 : 3
        accITA(eixo).time = accITA(eixo).time - mean(set.accITA(eixo).time);
    end
    % calcula resultante
    resultante = (accITA(1).time.^2 + accITA(2).time.^2 + accITA(3).time.^2).^0.5;
end
%% Filtro e valores RMS
if(~mpu.setupFlag)
    % Filtra o sinal (valores fltrados ficam nas linhas, bandas ficam
    % nas colunas)
    for i = 1 : 3
        filtered(i) = ita_fractional_octavebands(accITA(i),'bandsperoctave',3,'freqRange',[1 80]);
    end
    % Aloca o array para valores RMS. Nas linhas, as bandas de
    % frequência; nas colunas, os eixos.
    rms = zeros(size(filtered(1).time,2),3);
    % Calcula os valores RMS para cada frequência, para cada eixo
    nsamples = length(filtered(1).time(:,1));
    for i = 1 : size(rms,1)
        for j = 1 : 3
            rms(i,j) = sqrt(sum(filtered(j).time(:,i).^2)/nsamples);
        end
    end
    % Nesse caso, resultante, filtered e rms devem ser retornados
    varargout{1} = resultante;
    varargout{2} = filtered;
    varargout{3} = rms;
end
end