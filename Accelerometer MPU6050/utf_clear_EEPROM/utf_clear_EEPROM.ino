/* Rotina para limpar a memória não volátil - EEPROM - do Arduino, e ...
   preparar um valor de taxa de amostragem para posterior cálculo de quantidade de amostras quando for rezalizar medições
*/

#include <EEPROM.h>

#define eeAddress 0

float measured_fs = 0.0;

void setup()
{
  Serial.begin(38400);
  // Limpa a memória não volátil do Arduino
  for (int i = 0; i < EEPROM.length(); i++)
  {
    EEPROM.write(i, 0); // writes a byte
  }
  Serial.begin(38400);
  Serial.println("Clear!");
  // Faz um teste de comunicação, salvando um valor na memória e lendo-o novamente
  measured_fs = 300.0;
  EEPROM.put(eeAddress, measured_fs); // put: writes any type of data
  measured_fs = 0.0;
  EEPROM.get(eeAddress, measured_fs); // get: reads any type of data
  Serial.println(measured_fs);
}

void loop()
{

}
