/* Calcula Desvio ao Padrão de Fábrica (Factory Trim) */

// Código baseado no exemplo MPU raw do Electronic Cats.
// https://github.com/ElectronicCats/mpu6050/wiki

// Os valores de offset inseridos aqui são obtidos utilizando o código get_offset!
// Não interessa o range com o qual se deseja trabalhar, o offset é o mesmo!

// Necessita da biblioteca MPU6050 do Elctronic Cats instalada.

#include "I2Cdev.h"
#include "MPU6050.h"

/* MPU6050 default I2C address is 0x68*/
MPU6050 mpu;
//MPU6050 mpu(0x69);         //Use for AD0 high
//MPU6050 mpu(0x68, &Wire1); //Use for AD0 low, but 2nd Wire (TWI/I2C) object.

#define OUTPUT_READABLE_ACCELGYRO

int16_t ax, ay, az;
bool blinkState;

// Minhas variáveis
int16_t astx, asty, astz;
uint8_t ftx, fty, ftz;
float gx, gy, gz;
float sensitivity, ftxcalc, ftycalc, ftzcalc, strx, stry, strz, devx, devy, devz, base_expoente;

void setup() {
  /*--Start I2C interface--*/
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin(); 
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
    Fastwire::setup(400, true);
  #endif

  Serial.begin(38400); //Initializate Serial wo work well at 8MHz/16MHz

  /*Initialize device and check connection*/ 
  Serial.println("Initializing MPU...");
  mpu.initialize();
  Serial.println("Testing MPU6050 connection...");
  if(mpu.testConnection() ==  false){
    Serial.println("MPU6050 connection failed");
    while(true);
  }
  else{
    Serial.println("MPU6050 connection successful");
  }

  /* Use the code below to change accel/gyro offset values. Use MPU6050_Zero to obtain the recommended offsets */ 
  Serial.println("Updating internal sensor offsets...\n");
  mpu.setXAccelOffset(0); //Set your accelerometer offset for axis X
  mpu.setYAccelOffset(0); //Set your accelerometer offset for axis Y
  mpu.setZAccelOffset(0); //Set your accelerometer offset for axis Z
  /*Print the defined offsets*/
  Serial.print("\t");
  Serial.print(mpu.getXAccelOffset());
  Serial.print("\t");
  Serial.print(mpu.getYAccelOffset()); 
  Serial.print("\t");
  Serial.print(mpu.getZAccelOffset());
  Serial.print("\n");

  /*Configure board LED pin for output*/ 
  pinMode(LED_BUILTIN, OUTPUT);

  // Meu setup
  // Ajustar range (0=2g, 1=4g)
  mpu.setFullScaleAccelRange(0);
  // Desabilitar self-test (pois altera o valor de saída e, quando habilitado, permanece habilitado mesmo após reinicialização (toggle))
  mpu.setAccelXSelfTest(false);
  mpu.setAccelYSelfTest(false);
  mpu.setAccelZSelfTest(false);
  // Preferi deixar um tempo de delay entre a leitura dos dados e a habilitação do self-test, e também após a habilitação e desabilitação
  // (lembrando que a taxa de amostragem do acelerômetro em si é de 1kHz).
  delay(110);
}

void loop() {
  // Ler aceleração ()
  mpu.getAcceleration(&ax, &ay, &az);

  delay(110);
  // Habilitar self-test
  mpu.setAccelXSelfTest(true);
  mpu.setAccelYSelfTest(true);
  mpu.setAccelZSelfTest(true);
  delay(110);

  // Ler aceleração (com self-test habilitado)
  mpu.getAcceleration(&astx, &asty, &astz);
  // Ler factory trim
  ftx = mpu.getAccelXSelfTestFactoryTrim();
  fty = mpu.getAccelYSelfTestFactoryTrim();
  ftz = mpu.getAccelZSelfTestFactoryTrim();
  // Calcular o factory trim
  ftxcalc = 4096.0 * 0.34 * pow( (0.92/0.34),  (float(ftx)-1.0)/30.0);
  ftycalc = 4096.0 * 0.34 * pow( (0.92/0.34),  (float(fty)-1.0)/30.0);
  ftzcalc = 4096.0 * 0.34 * pow( (0.92/0.34),  (float(ftz)-1.0)/30.0);
  // Calcular o resultado do self-test
  strx = float(astx) - float(ax);
  stry = float(asty) - float(ay);
  strz = float(astz) - float(az);
  // Calcular desvio
  devx = (strx - float(ftxcalc)) / float(ftxcalc);
  devy = (stry - float(ftycalc)) / float(ftycalc);
  devz = (strz - float(ftzcalc)) / float(ftzcalc);
	
  // Apresentação dos resultados
	#ifdef OUTPUT_READABLE_ACCELGYRO
    // aceleração sem self-test
    Serial.print("a: ");
    Serial.print(ax); Serial.print("\t");
    Serial.print(ay); Serial.print("\t");
    Serial.print(az); Serial.print("\t ast: ");
    // aceleração com self-test
    Serial.print(astx); Serial.print("\t");
    Serial.print(asty); Serial.print("\t");
    Serial.print(astz); Serial.print("\t str: ");
    // resultado do self-test
    Serial.print(strx); Serial.print("\t");
    Serial.print(stry); Serial.print("\t");
    Serial.print(strz); Serial.print("\t ft: ");
    // factory trim
    Serial.print(ftx); Serial.print("\t");
    Serial.print(fty); Serial.print("\t");
    Serial.print(ftz); Serial.print("\t ftcalc: ");
    // factory trim como calculado
    Serial.print(ftxcalc); Serial.print("\t");
    Serial.print(ftycalc); Serial.print("\t");
    Serial.print(ftzcalc); Serial.print("\t dev:");
    // desvio
    Serial.print(devx); Serial.print("\t");
    Serial.print(devy); Serial.print("\t");
    Serial.println(devz);
  #endif

  // Desabilitar self-test
  mpu.setAccelXSelfTest(false);
  mpu.setAccelYSelfTest(false);
  mpu.setAccelZSelfTest(false);

  /*Blink LED to indicate activity*/
  blinkState = !blinkState;
  digitalWrite(LED_BUILTIN, blinkState);

  // Simples delay, para legibilidade
  // (e também para ser o delay pós desabilitação do self-test).
  delay(280);
}
