%% utf_MPU_saves %%
% Salva arquivos .mat e .xls de medições com o acelerômetro
% input:
% tempo = vetor de tempo para os dados de aceleração (semelhante a
% accITA.timeVector)
% freq = vetor de frequências para os dados de aceleração rms (semelhante
% ao filtered.freqVector) - filtered é objeto ITA (ver utf_MPU_ITA()).
% accITA = dados de aceleração em objeto ITA
% rms = dados de aceleração por banda de frequência, em RMS
% Para mais detalhes sobre os dados deentrada, ver os valores de saída das
% funções utf_MPU_ITA() e utf_MPU_plots().
function utf_MPU_saves(time,freq,mpu,accITA,resultante,filtered,rms,path,filename)
    % Salva tudo em arquivo .mat
    save(path + filename + ".mat",'time','freq','mpu','accITA','resultante','filtered','rms');
    % Salva dados em planilha (os valores são salvos como string para
    % não conflitar com o cabeçalho)
    % Tempo, sem filtrar
    planilhaTempo = time;
    for i = 1 : 3
        planilhaTempo = [planilhaTempo string(accITA(i).time)];
    end
    % Cria cabeçalho e junta com os valores de aceleração
    planilhaTempo = [["Tempo_s" "X_m/s_2" "Y_m/s_2" "Z_m/s_2"];
                     planilhaTempo];
    % Dados na frequência, em RMS    
    planilhaRMS = [["Bandas_terço_oitava_Hz" "X_m/s_2_RMS" "Y_m/s_2_RMS" "Z_m/s_2_RMS"];
                   [freq rms]];
    xlswrite(path + filename + " - tempo.xls",planilhaTempo);
    xlswrite(path + filename + " - rms.xls",planilhaRMS);
end