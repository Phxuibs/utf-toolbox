%% utf_symmetry_coefficient_plot (v2)%%

% Plot de amostra, função de amostra, coeficiente de simetria

% Autor: Luis Henrique Sant'Ana

% input:
% struct com campos (alguns podem ser necessários ou não dependendo dos
% parâmetros utilizados. Ver função utf_symmetry_coefficient para
% explicação de cada array):
% theta1 e rho1 = ângulos (rad) e distâncias, respectivamente, para as
% curvas externas dos hemisférios
% theta2 e rho2 = ângulos (rad) e distâncias, respectivamente, para as
% curvas internas dos hemisférios
% area = função de área
% areaTheta = eixo theta para a área
% S = coefciente de simetria
% STheta = eixo theta para o coeficiente de simetria
% angRes = resolução angular das curvas de amostras e função de amostra
% (rad)
% autRes = resolução angular do coeficiente de simetria (rad)
% n = número de elementos na amostra
% e também nome do arquivo para salvar o gráfico:
% filename (string; sem formato)
% varargin (string ou vetor de string)
% pode ser "sample","sample_function","symmetry", ou "all";
% são os tipos de plots que devem ser feitos
% pode ser seguido de tamanho desejado de fonte. default: 24;
% e largura desejada, em centímetros, no plot retangular. default: 32

function utf_symmetry_coefficient_plot(sample,filename,varargin)
%% setup
p = inputParser;
addRequired(p,'sample',@(x) isstruct(x));
addRequired(p,'filename',@(x) isstring(x));
addOptional(p,'plots',"symmetry",@(x) isvector(x) &&  isstring(x));
addOptional(p,'fontSize',24,@(x) isscalar(x) && isnumeric(x));
addOptional(p,'plotWidth',32,@(x) isscalar(x) && isnumeric(x));
parse(p,sample,filename,varargin{:});

% extrair campos do struct para arrays independentes
names = fieldnames(sample);
for i=1:length(names)
eval([names{i} '=sample.' names{i} ';']);
end
% plots a serem feitos
if(isequal(p.Results.plots,"all"))
    plots = ["sample","sample_function","symmetry"];
else
    plots = p.Results.plots;
end
% quantos plots devem ser feitos
N = length(plots);

% para comparações com float, é melhor usar uma tolerância
tol = angRes/10;
% tamaho das fontes
fontSize = p.Results.fontSize;
% largura do plot retangular
plotWidth = p.Results.plotWidth;

%% plot(s)
for i = 1 : N
% plot polar
h = figure();
ax = polaraxes();
hold on
plot_core_polar(theta1,rho1,theta2,rho2,ax,1.2,n);
if(isequal(plots(i),"sample_function"))
    polarplot(areaTheta,Area,'parent',ax,'color',utf_color("indigo"),...
                  'linewidth',2);
else
    if(isequal(plots(i),"symmetry"))
        polarplot(STheta,S,'parent',ax,'color',utf_color("indigo"),...
                  'linewidth',2);
    end
end
hold off
ax.FontSize = fontSize;
ax.RLim = [0, 1];
ax.TickLabelInterpreter = 'latex';
grid on
utf_adjust_plot_fine(h,filename+"_"+plots(i)+"_polar",[18 18]);
% plot retangular
h = figure();
hold on
plot_core_rectangular(theta1,rho1,theta2,rho2,angRes,tol,1.2,n);
if(isequal(plots(i),"sample_function"))
    plot(areaTheta,Area,'color',utf_color("indigo"),'linewidth',2);
else
    if(isequal(plots(i),"symmetry"))
        plot(STheta,S,'color',utf_color("indigo"),'linewidth',2);
    end
end
hold off
ax = gca;
ax = utf_axes(ax,fontSize);
ax.XLim = [0 2*pi];
ax.YLim = [0 1];
ax.XTick = 0 : pi/6 : 2*pi;
ax.XTickLabel = string(0 : 30 : 360);
if(isequal(plots(i),"symmetry"))
    ax.YLabel.String = "$\mathrm{S_{\circ}}$";
    ax.XLabel.String = "$\tau\ (^{\circ})$";
else
    ax.XLabel.String = "$\phi\ (^{\circ})$";
    if(isequal(plots(i),"sample"))
        ax.YLabel.String = "$\rho$ (normalised)";
    else
        if(isequal(plots(i),"sample_function"))
            ax.YLabel.String = "$\mathrm{\Delta \mathrm{A}}$";
        end
    end
end
utf_adjust_plot_fine(h,filename+"_"+plots(i)+"_rectangular",[plotWidth 19]);
end
function plot_core_polar(theta1,rho1,theta2,rho2,ax,lineWidth,n)
% pigmentos iniciais e incrementos
[red,green,blue,red_inc,green_inc,blue_inc] = symmetry_plots_colors(n);
for i = 1 : n
    polarplot(theta1{i},rho1{i},theta2{i},rho2{i},'Parent',ax,...
              'Color',[red green blue],'LineWidth',lineWidth);
    % incrementos nos pigmentos
    red = red + red_inc;
    green = green + green_inc;
    blue = blue + blue_inc;
end

function plot_core_rectangular(theta1,rho1,theta2,rho2,angRes,tol,lineWidth,n)
% pigmentos iniciais e incrementos
[red,green,blue,red_inc,green_inc,blue_inc] = symmetry_plots_colors(n);
for i = 1 : n
    % se cruzar o ângulo 2pi/0, deve ser divido em duas partes, caso
    % contrário uma linha cruzará o plot inteiro
    % para saber se cruza, verifica-se se o menor valor é 0 e o maior é
    % 2pi-angRes
    % primeiro para theta1:
    % se não cruza
    if(~( (abs(min(theta1{i}))<tol) && ...
          (abs((2*pi-angRes)-max(theta1{i}))<tol) ) )
        % plot normal
        plot(theta1{i},rho1{i},'Color',[red green blue],...
                               'LineWidth',lineWidth);
    else % se cruza
        % realiza divisão (lembrando que o array está em ordem crescente)
        % índice que possui o ângulo 360-angRes
        idx = find(abs(theta1{i}-(2*pi-angRes))<tol);
        % plot até 360-angRes (incluso)
        plot(theta1{i}(1:idx),rho1{i}(1:idx),...
             'Color',[red green blue],'LineWidth',lineWidth);
        % plot de 0 até o final (incluso)
        plot(theta1{i}(idx+1:end),rho1{i}(idx+1:end),...
             'Color',[red green blue],'LineWidth',lineWidth);
    end
    % agora para theta2:
    if(~( (abs(min(theta2{i}))<tol) && ...
          (abs((2*pi-angRes)-max(theta2{i}))<tol) ) )
        % plot normal
        plot(theta2{i},rho2{i},'Color',[red green blue],...
                               'LineWidth',lineWidth);
    else % se cruza
        idx = find(abs(theta2{i}-(2*pi-angRes))<tol);
        plot(theta2{i}(1:idx),rho2{i}(1:idx),...
             'Color',[red green blue],'LineWidth',lineWidth);
        plot(theta2{i}(idx+1:end),rho2{i}(idx+1:end),...
             'Color',[red green blue],'LineWidth',lineWidth);
    end
    % incrementos nos pigmentos
    red = red + red_inc;
    green = green + green_inc;
    blue = blue + blue_inc;
end

function [red,green,blue,red_inc,green_inc,blue_inc] = ...
          symmetry_plots_colors(n)
% pigmentos iniciais
red = 0;
green = 0.45;
blue = 0.74;
% para melhor representar a distribuição aleatória, os pigmentos sofrem
% incrementos a cada hemisfério, variando a cor ao longo dos plots
% incrementos
red_inc = 0;
green_inc = 0.4/n;
blue_inc = -0.4/n;