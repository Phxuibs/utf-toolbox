%% utf_symmetry_coefficient_plot %%

% Plot de amostra, função de amostra, coeficiente de simetria

% Autor: Luis Henrique Sant'Ana

% input:
% struct com campos (alguns podem ser necessários ou não dependendo dos
% parâmetros utilizados. Ver função utf_symmetry_coefficient para
% explicação de cada array):
% theta1 e rho1 = ângulos (rad) e distâncias, respectivamente, para as
% curvas externas dos hemisférios
% theta2 e rho2 = ângulos (rad) e distâncias, respectivamente, para as
% curvas internas dos hemisférios
% area = função de área
% areaTheta = eixo theta para a área
% S = coefciente de simetria
% STheta = eixo theta para o coeficiente de simetria
% angRes = resolução angular das curvas de amostras e função de amostra
% (rad)
% autRes = resolução angular do coeficiente de simetria (rad)
% n = número de elementos na amostra
% e também nome do arquivo para salvar o gráfico:
% filename (string; sem formato)

function utf_symmetry_coefficient_plot(sample,filename)
%% setup
% extrair campos do struct para arrays independentes
names = fieldnames(sample);
for i=1:length(names)
eval([names{i} '=sample.' names{i} ';']);
end

% para comparações com float, é melhor usar uma tolerância
tol = angRes/10;
% tamaho das fontes
fontSize = 24;

%% plot do coeficiente de simetria
% plot polar
h = figure();
ax = polaraxes();
hold on
plot_core_polar(theta1,rho1,theta2,rho2,ax,1.2,n);
polarplot(STheta,S,'parent',ax,'color',utf_color("indigo"),'linewidth',2);
hold off
ax.FontSize = fontSize;
ax.RLim = [0, 1];
ax.TickLabelInterpreter = 'latex';
grid on
utf_adjust_plot_fine(h,"fig_"+filename+"_symmetry_polar",[18 18]);
% plot cartesiano
h = figure();
hold on
plot_core_cartesian(theta1,rho1,theta2,rho2,angRes,tol,1.2,n);
plot(STheta,S,'color',utf_color("indigo"),'linewidth',2);
hold off
ax = gca;
ax = utf_axes(ax,fontSize);
ax.XLim = [0 2*pi];
ax.YLim = [0 1];
ax.XTick = 0 : pi/6 : 2*pi;
ax.XTickLabel = string(0 : 30 : 360);
ax.XLabel.String = "$\tau\ (^{\circ})$";
ax.YLabel.String = "$\mathrm{S_{\circ}}$ ($\cdot$)";
utf_adjust_plot_fine(h,"fig_"+filename+"_symmetry_cartesian",[32 19]);

function plot_core_polar(theta1,rho1,theta2,rho2,ax,lineWidth,n)
% pigmentos iniciais e incrementos
[red,green,blue,red_inc,green_inc,blue_inc] = symmetry_plots_colors(n);
for i = 1 : n
    polarplot(theta1{i},rho1{i},theta2{i},rho2{i},'Parent',ax,...
              'Color',[red green blue],'LineWidth',lineWidth);
    % incrementos nos pigmentos
    red = red + red_inc;
    green = green + green_inc;
    blue = blue + blue_inc;
end

function plot_core_cartesian(theta1,rho1,theta2,rho2,angRes,tol,lineWidth,n)
% pigmentos iniciais e incrementos
[red,green,blue,red_inc,green_inc,blue_inc] = symmetry_plots_colors(n);
for i = 1 : n
    % se cruzar o ângulo 2pi/0, deve ser divido em duas partes, caso
    % contrário uma linha cruzará o plot inteiro
    % para saber se cruza, verifica-se se o menor valor é 0 e o maior é
    % 2pi-angRes
    % primeiro para theta1:
    % se não cruza
    if(~( (abs(min(theta1{i}))<tol) && ...
          (abs((2*pi-angRes)-max(theta1{i}))<tol) ) )
        % plot normal
        plot(theta1{i},rho1{i},'Color',[red green blue],...
                               'LineWidth',lineWidth);
    else % se cruza
        % realiza divisão (lembrando que o array está em ordem crescente)
        % índice que possui o ângulo 360-angRes
        idx = find(abs(theta1{i}-(2*pi-angRes))<tol);
        % plot até 360-angRes (incluso)
        plot(theta1{i}(1:idx),rho1{i}(1:idx),...
             'Color',[red green blue],'LineWidth',lineWidth);
        % plot de 0 até o final (incluso)
        plot(theta1{i}(idx+1:end),rho1{i}(idx+1:end),...
             'Color',[red green blue],'LineWidth',lineWidth);
    end
    % agora para theta2:
    if(~( (abs(min(theta2{i}))<tol) && ...
          (abs((2*pi-angRes)-max(theta2{i}))<tol) ) )
        % plot normal
        plot(theta2{i},rho2{i},'Color',[red green blue],...
                               'LineWidth',lineWidth);
    else % se cruza
        idx = find(abs(theta2{i}-(2*pi-angRes))<tol);
        plot(theta2{i}(1:idx),rho2{i}(1:idx),...
             'Color',[red green blue],'LineWidth',lineWidth);
        plot(theta2{i}(idx+1:end),rho2{i}(idx+1:end),...
             'Color',[red green blue],'LineWidth',lineWidth);
    end
    % incrementos nos pigmentos
    red = red + red_inc;
    green = green + green_inc;
    blue = blue + blue_inc;
end

function [red,green,blue,red_inc,green_inc,blue_inc] = ...
          symmetry_plots_colors(n)
% pigmentos iniciais
red = 0;
green = 0.45;
blue = 0.74;
% para melhor representar a distribuição aleatória, os pigmentos sofrem
% incrementos a cada hemisfério, variando a cor ao longo dos plots
% incrementos
red_inc = 0;
green_inc = 0.4/n;
blue_inc = -0.4/n;