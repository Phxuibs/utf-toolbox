%% utf_sample_integrity %%

% This algorithm checks if there is any superposition of hemispheres in a
% given sample

% author: Luis H. Sant'Ana. 09 August 2023.

% input:
% rho: distance from the center of each element to the center of plane
% (numeric vector);
% phi: angle from the center of each element, in degrees (numeric vector); 
% r: elements' radius (numerica scalar).

% output:
% f: lower triangular matrix where 1 indicates a superposition between
% elements

function [f] = utf_sample_integrity(rho,phi,r)
%% parsing
p = inputParser;
isposNumVector = @(x) isvector(x) && isnumeric(x) && all(x>0);
addRequired(p,'rho',isposNumVector);
addRequired(p,'phi',isposNumVector);
addRequired(p,'r',@(x) isscalar(x) && isnumeric(x) && (x>0));
parse(p,rho,phi,r);
if(~isequal(length(rho),length(phi)))
    error("rho and phi must have same length.");
end
%% Calcualte distances and create flags
disp("Verifying sample integrity...");
f = zeros(length(rho));
for i = 2 : length(f)
    for j = 1 : i-1
        distance = sqrt( rho(i)^2 + rho(j)^2 -2*rho(i)*rho(j) * ...
                         cosd( phi(i) - phi(j) ) );
        if(distance <= 2*r)
            f(i,j) = 1;
        end
    end
end
%% Display result and count any inconsistency
nErrors = sum(f,'all');
if(isequal(nErrors,0))
    disp("Alrighty!");
else
    disp("Check the f matrix!");
    if(nErrors==1)
        disp("1 inconsistency found.");
    else
        disp(nErrors + " inconsistencies found.");
    end
end
end