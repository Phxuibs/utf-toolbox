%% utf_sample_function %%

% Cálcula a função de amostra (área).
% curvesTheta e curvesRho são equivalentes a theta1 e rho1 ou theta2 e
% rho2 retornados da função utf_circles.

% Autor: Luis Henrique Sant'Ana

% input:
% curvesTheta = eixo de ângulos
% curvesRho = distâncias à origem do plano
% type = "external" (e.g. para theta1 e rho1) ou "internal" (e.g. para
% theta2 e rho2)
% rho = distância do centro do hemisfério até a origem do plano
% r = raio de um hemisfério
% angRes = resolução angular (em radianos)

function [areaTheta,area] = utf_sample_function(curvesTheta,curvesRho,...
                                                 type,rho,r,angRes)
%% setup
% índice do elemento que passa pelo centro (se houver)
idx_center = find(rho<r);
% para comparações com float, é melhor usar uma tolerância
tol = angRes/10;
% array de 0 até 2pi-ang_res para os ângulos
areaTheta = 0 : angRes : 2*pi - angRes;
% função da amostra
area = zeros(size(areaTheta));

%% cálculo da área
for i = 1 : length(curvesTheta)
    
    % No caso da curva externa, se o hemisfério não passa pelo centro, os
    % dois valores extremos devem ser removidos, pois a área deve ser zero
    % e não há valor na curva interna para anular depois a área
    % contabilizada na parte externa
    if( (type=="external") && ~isequal(i,idx_center) )
        curvesTheta{i} = curvesTheta{i}(2:end-1);
        curvesRho{i} = curvesRho{i}(2:end-1);
    end
    
    % se não cruza o ângulo 0/2pi
    if(~( (abs(min(curvesTheta{i}))<tol) && ...
          (abs((2*pi-angRes)-max(curvesTheta{i}))<tol) ) )
      
        % identifica os índices limtes de area_theta que correspondem aos
        % ângulos em theta1
        idx1 = find(abs(areaTheta-min(curvesTheta{i}))<tol);
        idx2 = find(abs(areaTheta-max(curvesTheta{i}))<tol);
        
        if( type=="external" || ...
            type=="internal" && ~isequal(i,idx_center) )
            % realiza a soma quadrática nessa faixa
            area(idx1:idx2) = area(idx1:idx2) + curvesRho{i}.^2;
        else
            % realiza a subtração quadrática nessa faixa
            area(idx1:idx2) = area(idx1:idx2) - curvesRho{i}.^2;
        end
            
    else % se cruza o ângulo 0/2pi
        
        % também identifica índice, mas agora são 3 os índices de interesse
        idx1 = find(abs((areaTheta-curvesTheta{i}(1)))<tol); % índice relativo
        % ao global (area_theta) em que começa a curva antes de 2pi rad;
        idx = find(abs(curvesTheta{i}-(2*pi-angRes))<tol); % índice da curva
        % correspondente ao maior valor (i.e. antes do 0);
        idx2 = find(abs(areaTheta-curvesTheta{i}(end))<tol); % índice relativo
        % ao global (area_theta) em que termina a curva depois de 0 rad
        
        if( type=="external" || ...
            type=="internal" && ~isequal(i,idx_center) )
            % faz a soma quadrática nessas faixas
            area(idx1:end) = area(idx1:end) + curvesRho{i}(1:idx).^2;
            area(1:idx2) = area(1:idx2) + curvesRho{i}(idx+1:end).^2;
        else
            % faz a subtração quadrática nessa faixa
            area(idx1:end) = area(idx1:end) - curvesRho{i}(1:idx).^2;
            area(1:idx2) = area(1:idx2) - curvesRho{i}(idx+1:end).^2;
        end
            
    end
end

% Se são curvas internas, inverte-se o sinal da área, pois deve subtrair
% a área externa
% Os que passam pelo centro devem somar também, por isso foram
% contabilizados como subtração
if(type == "internal")
    area = area*-1;
end
end