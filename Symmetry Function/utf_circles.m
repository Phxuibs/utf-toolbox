%% utf_circles %%

% Gera circunferências de raio 'r' cujos centros são dados em coordenadas
% polares 'theta' e 'rho'.
% Diferentemente de uma circunferência em coordenadas polares comum, esta é
% gerada apenas para a faixa de ângulos coberta pelo cone que tangencia a
% circunferência, em duas curvas: uma mais próxima da origem do plano,
% antes dos pontos de tangência, e outra mais distante da origem, após os
% pontos de tangência. Assim, todos os valores da circunferência são
% positivos.
% Se a circunferência passa pelo centro, a faixa de ângulos completa (0 a
% 2pi) é abrangida.

% Autor: Luis Henrique Sant'Ana

function [theta1,rho1,theta2,rho2] = utf_circles(theta,rho,r,ang_res)
%% cálculo das curvas
% tangentes
tan1 = theta-asin(r/rho);
tan2 = theta+asin(r/rho);
% como os resultados são imaginários se o hemisfério passa pelo centro da
% amostra, usa-se real()
if(r>rho)
    tan1 = real(tan1);
    tan2 = real(tan2);
end
% arredondar de forma a se ajustar com a resolução angular
tan1 = round(tan1*(1/ang_res))/(1/ang_res);
tan2 = round(tan2*(1/ang_res))/(1/ang_res);
% criar vetor de ângulos para a parte do hemisfério mais distante do centro
theta1 = tan1 : ang_res : tan2;
% valores negativos de theta1 são passados para positvo, subtraindo de 2*pi
theta1(theta1<0) = 2*pi+theta1(theta1<0); % (soma, pois são negativos)
% criar vetor de ângulos para a parte do hemisfério mais próxima do centro
theta2 = theta1(2:end-1); % theta1;
% se o hemisfério passa pelo centro da amostra
if(r>rho)
    % theta2 deve ser deslocado em pi rad para que os valores de theta2 e
    % rho2 sejam positivos
    theta2 = theta2 + pi;
end
% valores acima de 2*pi rad também são ajustados subtraindo 2*pi
theta1(theta1>=2*pi) = theta1(theta1>=2*pi) - 2*pi;
theta2(theta2>=2*pi) = theta2(theta2>=2*pi) - 2*pi;
% ainda, pode ocorrer de valores que deveriam ser zeros não o serem de fato
% (e.g 2.2345e-16). Logo:
theta1(theta1<ang_res) = 0;
theta2(theta2<ang_res) = 0;
% (para evitar números imaginários devido ao arredondamento, usa-se real())
rho1 = real( rho*cos(theta1-theta)+sqrt(r^2-rho^2*sin(theta1-theta).^2) );
% as continhas então mudam para rho2 se o hemisfério passa pelo centro ou
% não. Se tangencia o centro, rho2 não é necessário (é veor nulo).
if(r<rho)
    rho2 = real( rho*cos(theta2-theta)-sqrt(r^2-rho^2*sin(theta2-theta).^2) );
else
    if(r>rho)
    rho2 = real( rho*cos(theta2-theta)+sqrt(r^2-rho^2*sin(theta2-theta).^2) );
    else
        rho2 = zeros(1,length(theta2));
        % e como o hemisfério tangencia a origem do plano, podem existir
        % pequenos valores negativos em rho1 que são declarados como zero
        % se abaixo de uma tolerância
        tol = 1e-10;
        rho1( (rho1<0) & (abs(rho1)<tol) ) = 0;
    end
end
% verificar se não há nenhum valor negativo ou acima de 2pi rad
if( (min(theta1)<0) || (min(theta2)<0) || (min(rho1)<0) || (min(rho2)<0) || ...
    (max(theta1)>=2*pi) || (max(theta2)>=2*pi) )
    error("Limites não respeitados na criação de curvas");
end
end