%% utf_circular_correlation %%

% Realiza a correlação circular da função de amostra para o cálculo do
% coeficiente de simetria

% Autor: Luis Henrique Sant'Ana

% input:
% a = função de amostra
% autRes = resolução de ângulo para a autocorrelação (rad)
% angRes = resolução de ângulo da função de amostra (rad)
% output:
% Ryy_phi = ângulos correspondentes da autocorrelação
% Ryy = autocorrelação normalizada (dividida pelo seu maior valor)

function [RyyTheta,Ryy] = utf_circular_correlation(a,autRes,angRes)

% ângulos (rad)
RyyTheta = 0 : autRes : 2*pi-autRes;
% array para armazenar a função de amostra com a resolução para a
% correlação circular
y = nan(size(RyyTheta));
% pega valores da função de amostra
k = 0;
for i = 1 : round(autRes/angRes) : length(a)
    k = k + 1;
    y(k) = a(i);
end
% realiza a correlação circular
Ryy = nan(size(y));
% l é o lag; Y é o sinal atrasado
for l = 0 : length(y)-1
    if(~isequal(l,0))
        Y = y(length(y)+1-l:length(y));
        Y = horzcat(Y, y(1:length(y)-length(Y)));
    else
        Y = y;
    end
    Ryy(l+1) = sum(y.*Y);
end
% normaliza a correlação
Ryy = Ryy / max(Ryy);