%% utf_symmetry_coefficient v3 %%

% Retorna as funções de área e de simetria de uma amostra composta por
% hemisférios

% Autor: Luis Henrique Sant'Ana

% input
% é struct com os seguintes atributos:
% R = raio da mesa
% r = raio de um hemisfério
% theta = vetor com as coordenadas polares de ângulo (rad)
% rho = vetor com as coordenadas polares de distância até o centro
% obs.: não importa a unidade de comprimento utilizada; o sistema apenas
% deve se consistente

% output
% salva alguns campos no struct:
% theta1 e rho1 = ângulos (rad) e distâncias, respectivamente, para as
% curvas externas dos hemisférios
% theta2 e rho2 = ângulos (rad) e distâncias, respectivamente, para as
% curvas internas dos hemisférios
% area = função de área
% areaTheta = eixo theta para a área
% S = coefciente de simetria
% STheta = eixo theta para o coeficiente de simetria
% angRes = resolução angular das curvas de amostras e função de amostra
% (rad)
% autRes = resolução angular do coeficiente de simetria (rad)
% n = número de elementos na amostra

function [amostra] = utf_symmetry_coefficient(amostra)

% número de elementos
n = length(amostra.theta);
% resolução angular para amostra e função de amostra
amostra.angRes = pi/3600; % equivalente a 0.05 graus
% criação de arrays para as curvas dos hemisférios (células são utilizadas,
% pois a quantidade de ângulos cobertos depende da distância do hemifério
% até o centro)
theta1 = cell([n 1]); rho1 = theta1; theta2 = theta1; rho2 = theta1;

% geração das curvas dos hemisférios e normalização
for i = 1 : n
    [theta1{i},rho1{i},theta2{i},rho2{i}] = utf_circles(amostra.theta(i),...
                                  amostra.rho(i),amostra.r,amostra.angRes);
    rho1{i} = rho1{i}/amostra.R;
    rho2{i} = rho2{i}/amostra.R;
end

% área
[amostra.areaTheta,area1] = utf_sample_function(theta1,rho1,"external",...
                                     amostra.rho,amostra.r,amostra.angRes);
[~,area2] = utf_sample_function(theta2,rho2,"internal",...
                                     amostra.rho,amostra.r,amostra.angRes);
amostra.Area = area1 + area2;

% resolução angular para a função de simetria
amostra.autRes = pi/180; % equivalente a 1 grau
% coeficiente de simetria
[amostra.STheta,amostra.S] = utf_circular_correlation(amostra.Area,...
                                            amostra.autRes,amostra.angRes);
                                        
amostra.theta1 = theta1;
amostra.theta2 = theta2;
amostra.rho1 = rho1;
amostra.rho2 = rho2;
amostra.n = n;