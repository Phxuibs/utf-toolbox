%% calcular f_cr %%

clear
clc

%% setup e ler respostas

% caminho até a pasta de medições
path = "Sala Otima Simulada T 5s/";
% nome para salvar arquivos
filename = "otima_T_5s";
% nomes das medições
files = dir(path+"*.wav");
% quantidade de sinais
n = length(files);

for i = 1 : n
    % lê arquivo de áudio
    [x(i,:),fs] = audioread(path+files(i).name);
end

% transformada de Fourier e vetor de frequências
X = fft(x,[],2);
freq = linspace(0,fs,length(x));
% freq = linspace( 0, fs, length( x ) + 1 ); % curso ensinou assim
% f = f( 1 : end-1 );
% limitar até a frequência 'f'
f = 1000;
[~,idx_f] = min(abs(freq-f));
freq = freq(1:idx_f);
X = X(:,1:idx_f);
% picos do módulo
[pks,locs] = findpeaks(20*log10( abs(X(1,:))/max(abs(X(1,:))) ));
% encontar picos que estejam a menos de 3dB de picos vizinhos
less_3dB(1) = false;
for i = 2 : length(pks)-1
    if( (abs(pks(i)-pks(i-1)) < 3) && (abs(pks(i)-pks(i+1)) < 3) )
        less_3dB(i) = 1;
    else
        less_3dB(i) = 0;
    end
end
less_3dB(i+1) = 0;
%%
fig = figure();
hold on
plot(freq,20*log10( abs(X(1,:))/max(abs(X(1,:))) ),'linewidth',1);
plot(freq(locs),pks,'^','color',utf_color("vermelho"));
hold off
ax = gca;
ax = utf_axes(ax,22);
ax.YTick = ax.YLim(1) : 3 : ax.YLim(2);

%% modos - kuttruff, acoustics, p. 170 e 173 -
% percebeu-se que não há boa concordância entre os pico do módulo da FT com
% os modos da sala
% dimensões da sala
L = [7.974 5.819 2.694];
V = L(1)*L(2)*L(3);
% frequência limite de interesse (f)
% velocidade do som
c = 343.2;
% número esperado de modos
N_f = 4/3*pi*V*(f/c)^3; % para altas frequências - OK!
% N_f = 4*pi/3*V*(f/c)^3 + pi/4*S*(f/c)^2 + L/8*(f/c); % improved version
% índice limite para cada dimensão, resultando em 'dimension_limit'^(3)
% combinações pra geração dos modos
dimension_limit = N_f^(1/3);
% modos
modes = [];
for l = 0 : dimension_limit
    for m = 0 : dimension_limit
        for n = 0 : dimension_limit
            modes = [modes c/2 * ( (l/L(1))^2 + (l/L(2))^2 + ...
                                                (n/L(3))^2 )^(1/2) ];
        end
    end
end
modes = sort(modes);
modes = modes(modes <= f);
% plot
fig = figure();
hold on
plot(freq,20*log10( abs(X(1,:))/max(abs(X(1,:))) ),'linewidth',1);
plot([modes; modes], repmat(ylim',1,size(modes,2)),...
     ':','color',utf_color("indigo"))
hold off