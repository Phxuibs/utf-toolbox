%%%% utf_fcr_curve (v5) %%
%
% Descrição: a partir de uma resposta ao impulso, pode realizar diferentes
% testes com uma janela deslizante na função de transferência, retornando
% uma estimativa da frequência de transição
%
%
% input (obrigatórios):
%
% RI: resposta ao impulso (sinal bruto - não um objeto ITA, por exemplo)
% numeric vector
%
% fs: taxa de amostragem
% numeric scalar
%
%
% input (pares opcionais nome-valor):
%
% freqRange: limites de frequência para as análises.
% Padrão: [0 1000]. numeric duplet.
%
% windowSize: tamanho da janela, sendo a unidade de referência determinada
% por windowSizeUnit.
% Padrão: 1000. numeric scalar.
%
% windowSizeUnit: unidade de referência para windowSize, podendo ser
% 'samples' ou 'hertz'.
% Padrão: 'samples'. scalar character.
%
% spacingHertz: espaçamento entre janelas de análise subsequentes, em hertz.
% Padrão: 5. numeric scalar.
%
% method: métrica utilizada. Pode ser curtose, DUPH ou K-S.
% Padrão: 'K-S'. character scalar or a character cell array for multiple
% methods. Accepted methods: 'ks','KS','k-s','K-S','duph','DUPH',
% 'kurtosis','curtose'.
%
% angleInterval: intervalo de ângulos (em graus) para análise de DUPH. De
% acordo com o mestrado de Sant'Ana (2021), valores recomendados são 18° ou
% 20°.
% Padrão: 20. numeric scalar.
%
%
% Output:
% struct fcrCurve com métodos:
% f: valores de frequência do começo e final de cada janela de análise
% p: p-valores correspondentes a cada janela de análise
% properties: um struct que armazena os valores de entrada e parêmetros
% utilizados
% São ainda apresentados métodos com variáveis pertinentes a cada métrica
% utilizada (curtose, DUPH, K-S). Em especial, tem-se os vetores de trials:
% são vetores lógicos que apresentam resultados dos testes estatísticos de
% hipóteses. 0 significa que a hipótese nula não foi rejeitada (i.e. não
% rejeita-se a hipótese de que está com propriedades estatísticas
% referentes à zona acima da frequência de transição) e 1 indica que
% toma-se a hipótese alternativa (i.e. a zona avaliada está abaixo da
% frequência de transição).
%
%
% Observação: anteriormente, a comparação de um valor de curtose, DUPH ou
% p-valor de K-S com um valor crítico era feito com eps() (porque lá no
% começo do desenvolvimento foi percebido algum comportamento estranho).
% Por exemplo: trials = pv - eps(pv) < alpha + eps(alpha);
%
%
% Autor: Luis H. Sant'Ana

function fcrCurve = utf_fcr_curve(RI,fs,varargin)
%% Parseamento
defaultFreqRange = [0 1000];
defaultWindowSize = 1000;
defaultWindowSizeUnit = 'samples';
defaultSpacingHertz = 5;
defaultMethod = 'K-S';
defaultAngleInterval = 20;

isPosNumScalar = @(x) isnumeric(x) && isscalar(x) && all(x>0);
p = inputParser;
addRequired(p,'RI',@(x) isnumeric(x) && isvector(x));
addRequired(p,'fs',isPosNumScalar);
addParameter(p,'freqRange',defaultFreqRange,@(x) isnumeric(x) && all(x>=0) && isequal(numel(x),2));
addParameter(p,'windowSize',defaultWindowSize,isPosNumScalar);
addParameter(p,'windowSizeUnit',defaultWindowSizeUnit,@(x) isequal(x,'samples') || isequal(x,'hertz'));
addParameter(p,'spacing',defaultSpacingHertz,isPosNumScalar);
addParameter(p,'method',defaultMethod,@(x) any(contains(x,{'ks','KS','k-s','K-S',...
                                                'duph','DUPH','kurtosis','curtose'})));
addParameter(p,'angleInterval',defaultAngleInterval,isPosNumScalar);
parse(p,RI,fs,varargin{:});

freqRange = p.Results.freqRange;
windowSize = p.Results.windowSize;
windowSizeUnit = p.Results.windowSizeUnit;
spacingHertz = p.Results.spacing;
method = p.Results.method;
angleInterval = p.Results.angleInterval;

% flags de métodos
if(any(contains(method,{'kurtosis','curtose'})))
    flagKurtosis = true;
else
    flagKurtosis = false;
end
if(any(contains(method,{'ks','k-s','KS','K-S'})))
    flagKS = true;
else
    flagKS = false;
end
if(any(contains(method,{'duph','DUPH'})))
    flagDUPH = true;
else
    flagDUPH = false;
end
%% Setup dependende do sinal de entrada (RI)
% vetor de frequências
NFFT = length(RI);
freqVector = transpose((0:NFFT/2)*fs/NFFT);
% função de transferência
TF = fft(RI);
TF = TF(1:end/2+1);
% resolução em frequência
freqResolution = fs/NFFT;
%% É necessária a largura da janela em amostras
% se a unidade de largura de janela estiver em hertz
if(strcmpi(windowSizeUnit,'hertz'))
    % calcula a largura da janela em amostras
    windowSizeSamples = round( windowSize / freqResolution );
else % caso contrário, está em amostras, então
    % aloca o valor na variável final, que será utilizada no algoritmo
    windowSizeSamples = windowSize;
end
% e calcula a largura de banda de cada janela, para referência
windowBandwidth = windowSizeSamples * freqResolution;
%% Quantidade de janelas de análises
% índices correspondentes aos limites de frequências
[~,idxLower] = min( abs(freqVector - freqRange(1)) );
[~,idxUpper] = min( abs(freqVector - freqRange(2)) );
% verificar se há amsotras suficientes para pelo menos uma análise
totalSamples = idxUpper-idxLower+1;
if(totalSamples < windowSizeSamples)
    error("Quantidade de amostras insuficientes.");
end
% espaçamento entre janelas subsequentes, em amostras
spacingSamples = spacingHertz / freqResolution;
% quantidade de análises a serem feitas
% (ela é ditada por quantos espaçamentos serão feitos considerando a
% quantidade restante de amostras depois última amostra da primeira janela.
% O "+1" conta a primeira análise.)
N = floor(( totalSamples - windowSizeSamples ) / spacingSamples ) + 1;
%% Índices de frequência para as janelas
% cada análise compreende uma faixa de frequências: limite inferior da
% janela e limite superior da janela.
% índices do vetor de frequências que correspondem a cada limite de janela
fIdx = nan(N,2);
% determina índices da janela
for i = 1 : N
    % primeiro índice da janela de análise
    % se não for a primeira análise
    if(~isequal(i,1))
        % índice da frequência inferior da análise 'i'
        [~,fIdx(i,1)] = min(abs(freqVector - ...
                                (freqRange(1) + (i-1)*spacingHertz) ) );
    else % se for a primeira análise
        % índice da frequência inferior da janela é semelhante ao índice da
        % frequência do limite inferior de análise
        fIdx(i,1) = idxLower;
    end
    % índice da frequência superior da análise 'i'
    fIdx(i,2) = fIdx(i,1) + windowSizeSamples - 1;
end
%  frequências correspondentes
f = [freqVector(fIdx(:,1)) freqVector(fIdx(:,2))];
%% Análise de curtose
if(flagKurtosis)
    kurtosisReal = nan(N,1);
    kurtosisImag = nan(N,1);
    for i = 1 : N
        % o parâmetro '1' ou '0' diz respeito ao viés sistemático do
        % equacionamento. O padrão do Matlab é '1', sem correção.
        kurtosisReal(i) = kurtosis(real(TF(f_idx(i,1):f_idx(i,2))),0) - 3;
        kurtosisImag(i) = kurtosis(imag(TF(f_idx(i,1):f_idx(i,2))),0) - 3;
    end
    % valor de corte de referência (95º percentil)
    kurtosisRef = utf_kurtosis_ref(windowSizeSamples);
    % realiza os testes de hipóteses - hipóteses nula (0) e alternativa (1)
    kurtosisRealTrials = kurtosisReal > kurtosisRef;
    kurtosisImagTrials = kurtosisImag > kurtosisRef;
    % aplica testes com distribuição binomial para determinar a frequência
    % de transição
    kurtosisRealFcr = utf_shrinking_binomial(kurtosisRealTrials,f(:,1));
    kurtosisImagFcr = utf_shrinking_binomial(kurtosisImagTrials,f(:,1));
end
%% Fase do sinal
if(flagDUPH || flagKS)
    % ângulos de fase, em graus
    phase = angle( TF ) / pi * 180;
end
%% Análise de DUPH
if(flagDUPH)
    % limites das barras de cada histograma
    edges = -180 : angleInterval : 180;
    % número de barras em cada histograma
    nbins = length(edges) - 1;
    % para cada um dos N histogramas:
    counts = nan(N,nbins); % frequência (vulgo "quantidade de ocorrências") de cada barra
    for i = 1 : N
        % frequência relativa (vulgo frequência)
        [counts(i,:),~] = histcounts(phase(fIdx(i,1):fIdx(i,2)),edges,...
                                     'normalization','probability');
    end
    % frequência relativa normalizada(invariante ao intervalo de ângulos)
    counts = counts * nbins;
    % maior desvio da referência (com a normalização, a referência é sempre
    % em 1 - ao invés de 1/nbins)
    duph = max(abs(counts - 1),[],2);
    % valor de corte de referência (95º percentil)
    DUPHreference = utf_DUPH_ref(windowSizeSamples,angleInterval);
    % realiza os testes de hipóteses - hipóteses nula (0) e alternativa (1)
    DUPHtrials = duph > DUPHreference;
    % aplica testes com distribuição binomial para determinar a frequência
    % de transição
    DUPHfcr = utf_shrinking_binomial(DUPHtrials,f(:,1));
end
%% Análise de K-S
if(flagKS)
    % distribuição hipotética de referência
    U = makedist('uniform','lower',-180,'upper',180);
    % p-valores dos testes
    pv = nan(N,1);
    % resultados dos testes
    KStrials = nan(N,1);
    % estatística dos testes
    KSstatistic = nan(N,1);
    % análise de K-S, retornando p-valor, estatística (maior desvio) e
    % valor crítico (este depende apenas de alpha e quantidade de amostras,
    % então é o mesmo em todas as análises).
    for i = 1 : N
        [KStrials(i),pv(i),KSstatistic(i),KScv] = kstest(phase(fIdx(i,1):fIdx(i,2)),'CDF',U);
    end
    % passa para lógico, porque é iniciado como double
    KStrials = logical(KStrials);
    % aplica testes com distribuição binomial para determinar a frequência
    % de transição
    KSfcr = utf_shrinking_binomial(KStrials,f(:,1));
end
%% Retorno da função
% índices das janelas
fcrCurve.fIdx = fIdx;
% valores em frequência correspondentes
fcrCurve.f = f;
% valores de entrada ou parâmetros utilizados (não específicos a métodos)
fcrCurve.properties.RI = RI;
fcrCurve.properties.fs = fs;
fcrCurve.properties.freqRange = freqRange;
fcrCurve.properties.windowsSize = windowSize;
fcrCurve.properties.windowSizeUnit = windowSizeUnit;
fcrCurve.properties.windowSizeSamples = windowSizeSamples;
fcrCurve.properties.spacingHertz = spacingHertz;
fcrCurve.properties.method = method;
% informações adicionais
fcrCurve.properties.windowBandwidth = windowBandwidth;
fcrCurve.properties.TF = TF;
fcrCurve.properties.freqVector = freqVector;
% se foi utilizado curtose
if(flagKurtosis)
    fcrCurve.kurtosis.kurtosisReal = kurtosisReal;
    fcrCurve.kurtosis.kurtosisImag = kurtosisImag;
    fcrCurve.Kurtosis.kurtosisRef = kurtosisRef;
    fcrCurve.kurtosis.kurtosisRealTrials = kurtosisRealTrials;
    fcrCurve.kurtosis.kurtosisImagTrials = kurtosisImagTrials;
    fcrCurve.kurtosis.kurtosisRealFcr = kurtosisRealFcr;
    fcrCurve.kurtosis.kurtosisImagFcr = kurtosisImagFcr;
end
% se foi utilizado DUPH ou K-S
if(flagDUPH || flagKS)
    fcrCurve.phase = phase;
end
% se foi utilizado DUPH
if(flagDUPH)
    fcrCurve.DUPH.duph = duph;
    fcrCurve.DUPH.counts = counts;
    fcrCurve.DUPH.nbins = nbins;
    fcrCurve.DUPH.edges = edges;
    fcrCurve.DUPH.angleInterval = angleInterval;
    fcrCurve.DUPH.DUPHreference = DUPHreference;
    fcrCurve.DUPH.DUPHtrials = DUPHtrials;
    fcrCurve.DUPH.DUPHfcr = DUPHfcr;
end
% se foi utilizado K-S
if(flagKS)
    fcrCurve.KS.pv = pv;
    fcrCurve.KS.statistic = KSstatistic;
    fcrCurve.KS.cv = KScv;
    fcrCurve.KS.KStrials = KStrials;
    fcrCurve.KS.KSfcr = KSfcr;
end
end