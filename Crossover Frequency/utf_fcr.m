%%%% utf_fcr (v4) %%
%
% Calcula frequência de transição a partir de curva de frequência de
% transição (por exemplo, utilizando a função utf_fcr_curve).
% É comum também enviar como parâmetro de entrada uma média de curvas
% obtidas a partir de diferentes posições fonte-receptor.
%
% input:
% pode ser um vetor de structs, que declara que cada struct é como o
% retornado pela função utf_fcr_curve e calcula a frequência de transição a
% partir de uma média entre os structs
% ou:
% f = vetor com as frequências correspondentes a cada amostra da curva
% 'fcr_curve'
% fcr_curve = curva que pode ser valores de curtose da parte real ou
% imaginária, ou desvios a uma distribuição uniforme da fase.
% method = método para geração da 'fcr_curve', que pode ser
% 'kurtosis_real', 'kurtosis_imag', 'duph' ou 'ks'
% w = tamanho da janela de análise, em samples, se não for ks test
%
% output:
% fcr = frequência de transição
%
% Autor: Luis Henrique Sant'Ana

function [fcr] = utf_fcr(varargin)
% com struct
% se só há um valor de entrada, deve ser um struct como o do retorno da
% função utf_fcr_curve
if(isequal(nargin,1) && isstruct(varargin{1}))
    fcr_curve = varargin{1};
    f = fcr_curve(1).f(:,1);
    method = fcr_curve(1).properties.method;
    w = fcr_curve(1).properties.window_size_samples;
    
    if(any(contains(method,{'kurtosis_real','kurtosis_imag'})))
        kurtosis_ref = utf_kurtosis_ref(w);
        fcr.kurtosis_ref = kurtosis_ref;
        if(any(contains(method,{'kurtosis_real'})))
            fcr.kurtosis_real = utf_fcr_per_se(f,...
                                    mean([fcr_curve.kurtosis_real],2),...
                                    kurtosis_ref); % bypass aqui
        end
        if(any(contains(method,{'kurtosis_imag'})))
            fcr.kurtosis_imag = utf_fcr_per_se(f,...
                                    mean([fcr_curve.kurtosis_imag],2),...
                                    kurtosis_ref); % bypass aqui
        end
    end
    if(any(contains(method,{'duph','DUPH'})))
        angle_interval = fcr_curve(1).D.angle_interval;
        normalization_flag = fcr_curve(1).D.normalization_flag;
        DUPH_max = utf_DUPH_ref(w,angle_interval);
        if(~normalization_flag)
            edges = -180 : angle_interval : 180;
            nbins = length(edges) - 1;
            DUPH_max = DUPH_max / nbins;
        end
        fcr.duph = utf_fcr_per_se(f,mean([fcr_curve.duph],2),DUPH_max);
        fcr.DUPH_max = DUPH_max;
    end
    
    if(any(contains(method,{'ks'})))
        fcr.ks = utf_fcr_per_se_ks(f,mean([fcr_curve.ks],2),0.05);
    end
% sem struct
% caso contrário, deve haver vetor de frequência, array com curva(s) para
% detecção de frequência de transição e método de detecção e largura de
% janela utilizada se não for ks
else    
    % parse
    validateMethod = @(x) any(contains(x,{'kurtosis','kurtosis_real',...
                                   'kurtosis_imag','duph','DUPH','ks'}));
    p = inputParser;
    addRequired(p,'f',@(x) isvector(x) && isnumeric(x));
    addRequired(p,'fcr_curve',@(x) ismatrix(x) && isnumeric(x));
    addRequired(p,'method',validateMethod);
    if(any(contains(varargin{3},{'duph','DUPH','kurtosis','kurtosis_real',...
                                               'kurtosis_imag'})))
        addRequired(p,'w',@(x) isnumeric(x) && isscalar(x));
    end
    if(isequal(nargin,6) && any(contains(varargin{3},{'duph','DUPH'})))
        addRequired(p,'angle_interval',@(x) isnumeric(x) && isscalar(x));
        addRequired(p,'normalization_flag',@(x) islogical(x));
    end
    parse(p,varargin{:});
    
    f = p.Results.f;
    fcr_curve = p.Results.fcr_curve;
    method = p.Results.method;
    if(any(contains(varargin{3},{'duph','DUPH','kurtosis','kurtosis_real',...
                                               'kurtosis_imag'})))
        w = p.Results.w;
    end
    if(isequal(nargin,6) && any(contains(varargin{3},{'duph','DUPH'})))
        angle_interval = p.Results.angle_interval;
        normalization_flag = p.Results.normalization_flag;
    end
    
    % análise de frequência de transição em si
    [~,dim] = min(size(fcr_curve));
    fcr_curve_mean = mean(fcr_curve,dim);
    if(any(contains(method,{'kurtosis','kurtosis_real',...
                                       'kurtosis_imag'})))
        kurtosis_ref = utf_kurtosis_ref(w);
        fcr = utf_fcr_per_se(f,fcr_curve_mean,kurtosis_ref);
    else
        if(any(contains(method,{'duph','DUPH'})))
            DUPH_max = utf_DUPH_ref(w,angle_interval);
            if(~normalization_flag)
                edges = -180 : angle_interval : 180;
                nbins = length(edges) - 1;
                DUPH_max = DUPH_max / nbins;
            end
            fcr = utf_fcr_per_se(f,fcr_curve_mean,DUPH_max);
        else
            if(any(contains(method,{'ks'})))
                fcr = utf_fcr_per_se_ks(f,fcr_curve_mean,0.05);
            end
        end
    end
end

end

% função para a determinação da frequência de transição em si
function [fcr] = utf_fcr_per_se(f,fcr_curve,ref)
    % último índice com valor maior ou igual a 'ref'
    fcr_idx = find(fcr_curve + eps(fcr_curve) >= ref - eps(ref),1,'last');
%     fcr_idx = find(fcr_curve >= ref ,1,'last');
    % se há mais índices na curva, o próximo indicará a frequência de
    % transição
    if(length(fcr_curve) > fcr_idx)
        fcr_idx = fcr_idx + 1;
        fcr = f(fcr_idx);
    else
        % senão, não há como identificar a frequência a partir dessa curva
        disp("Oxe, não pode ser identificada uma frequência de transição!");
        fcr = nan;
    end
end

% especialmente para KS
function [fcr] = utf_fcr_per_se_ks(f,fcr_curve,alpha)
    % último índice com valor menor ou igual a 'alpha'
    fcr_idx = find(fcr_curve - eps(fcr_curve) <= alpha + eps(alpha),1,'last');
    % se há mais índices na curva, o próximo indicará a frequência de
    % transição
    if(length(fcr_curve) > fcr_idx)
        fcr_idx = fcr_idx + 1;
        fcr = f(fcr_idx);
    else
        % senão, não há como identificar a frequência a partir dessa curva
        disp("Oxe, não pode ser identificada uma frequência de transição!");
        fcr = nan;
    end
end