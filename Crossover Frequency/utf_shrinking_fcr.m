%% utf_shrinking_fcr %%
%
% Instead of using a sliding window, K-S tests are applied to a shrinking
% window
%
%
% input (obrigatórios):
%
% RI: resposta ao impulso (sinal bruto - não um objeto ITA, por exemplo)
% numeric vector
%
% fs: taxa de amostragem
% numeric scalar
%
%
% input (pares opcionais nome-valor):
%
% freqRange: limites de frequência para as análises.
% Padrão: [0 1000]. numeric duplet.
%
% lastWindowSize: tamanho da última janela, sendo a unidade de referência
% determinada por lastWindowSizeUnit.
% Padrão: 1000. numeric scalar.
%
% lastWindowSizeUnit: unidade de referência para lastWindowSize, podendo
% ser 'samples' ou 'hertz'.
% Padrão: 'samples'. scalar character.
%
% spacingHertz: espaçamento dado para encolhimento da janela, em hertz.
% Padrão: 5. numeric scalar.
%
%
% output:
% fcrCurve: struct com diversos métodos.
%
%
% Autor: Luis H. Sant'Ana

function [fcrCurve] = utf_shrinking_fcr(RI,fs,varargin)
%% parseamento
defaultFreqRange = [0 1000];
defaultLastWindowSize = 1000;
defaultLastWindowSizeUnit = 'samples';
defaultSpacingHertz = 5;

isPosNumScalar = @(x) isnumeric(x) && isscalar(x) && all(x>0);
p = inputParser;
addRequired(p,'RI',@(x) isnumeric(x) && isvector(x));
addRequired(p,'fs',isPosNumScalar);
addParameter(p,'freqRange',defaultFreqRange,@(x) isnumeric(x) && all(x>=0) && isequal(numel(x),2));
addParameter(p,'lastWindowSize',defaultLastWindowSize,isPosNumScalar);
addParameter(p,'lastWindowSizeUnit',defaultLastWindowSizeUnit,@(x) isequal(x,'samples') || isequal(x,'hertz'));
addParameter(p,'spacing',defaultSpacingHertz,isPosNumScalar);
parse(p,RI,fs,varargin{:});

freqRange = p.Results.freqRange;
lastWindowSize = p.Results.lastWindowSize;
lastWindowSizeUnit = p.Results.lastWindowSizeUnit;
spacingHertz = p.Results.spacing;
%% Setup dependente do sinal de entrada (RI) (não alterado de utf_fcr_curve)
% vetor de frequências
NFFT = length(RI);
freqVector = transpose((0:NFFT/2)*fs/NFFT);
% função de transferência
TF = fft(RI);
TF = TF(1:end/2+1);
% resolução em frequência
freqResolution = fs/NFFT;
%% É necessária a largura da ÚLTIMA janela em amostras
% se a unidade de largura de janela estiver em hertz
if(strcmpi(lastWindowSizeUnit,'hertz'))
    % calcula a largura da janela em amostras
    lastWindowSizeSamples = round( lastWindowSize / freqResolution );
else % caso contrário, está em amostras, então
    % aloca o valor na variável final, que será utilizada no algoritmo
    lastWindowSizeSamples = lastWindowSize;
end
%% Quantidade de janelas de análises
% índices correspondentes aos limites de frequências
[~,idxLower] = min( abs(freqVector - freqRange(1)) );
[~,idxUpper] = min( abs(freqVector - freqRange(2)) );
% espaçamento entre janelas subsequentes, em amostras
spacingSamples = spacingHertz / freqResolution;
% número total de amostras
totalSamples = idxUpper-idxLower+1;
% quantidade de análises a serem feitas
% (ela é ditada por quantos espaçamentos serão feitos considerando a
% quantidade restante de amostras depois última amostra da primeira janela.
% O "+1" conta a primeira análise.)
N = floor(( totalSamples - lastWindowSizeSamples ) / spacingSamples ) + 1;
%% Índices de frequência para as janelas
% cada análise compreende uma faixa de frequências: limite inferior da
% janela e limite superior freqRange(2);
% índices do vetor de frequências que correspondem a cada limite inferior
% de janela
fIdx = nan(N,1);
% determina índices inferiores das janelas
for i = 1 : N
    % primeiro índice da janela de análise
    % se não for a primeira análise
    if(~isequal(i,1))
        % índice da frequência inferior da análise 'i'
        [~,fIdx(i)] = min(abs(freqVector - ...
                                (freqRange(1) + (i-1)*spacingHertz) ) );
    else % se for a primeira análise
        % índice da frequência inferior da janela é semelhante ao índice da
        % frequência do limite inferior de análise
        fIdx(i) = idxLower;
    end
end
% concatena com os índices superiores das janelas, que são sempre o do
% limite da faixa de análise
fIdx = [fIdx repmat(idxUpper,[N 1])];
% quantidade de amostras em cada janela
windowSizeSamples = fIdx(:,2) - fIdx(:,1) + 1;
% e calcula a largura de banda de cada janela, para referência
windowBandwidth = windowSizeSamples * freqResolution;
%  frequências correspondentes
f = [freqVector(fIdx(:,1)) freqVector(fIdx(:,2))];
%% Análise de K-S
% fase do sinal, em graus
phase = angle( TF ) / pi * 180;
% distribuição hipotética de referência
U = makedist('uniform','lower',-180,'upper',180);
% p-valores dos testes
pv = nan(N,1);
% resultados dos testes
KStrials = nan(N,1);
% estatística dos testes
KSstatistic = nan(N,1);
% análise de K-S, retornando p-valor, estatística (maior desvio) e
% valor crítico (este depende apenas de alpha e quantidade de amostras,
% então é o mesmo em todas as análises).
for i = 1 : N
    [KStrials(i),pv(i),KSstatistic(i),KScv] = kstest(phase(fIdx(i,1):fIdx(i,2)),'CDF',U);
end
% passa para lógico, porque é iniciado como double
KStrials = logical(KStrials);
% aplica testes com distribuição binomial para determinar a frequência
% de transição
KSfcr = utf_shrinking_binomial(KStrials,f(:,1));
%% Retorno da função
% frequências de cada janela
fcrCurve.f = f;
% índices que, se aplicados em freqVector, resultam em f
fcrCurve.fIdx = fIdx;
% propriedades
fcrCurve.properties.RI = RI;
fcrCurve.properties.fs = fs;
fcrCurve.properties.freqRange = freqRange;
fcrCurve.properties.lastWindowSize = lastWindowSize;
fcrCurve.properties.lastWindowSizeUnit = lastWindowSizeUnit;
fcrCurve.properties.spacingHertz = spacingHertz;
% prorpiedades adicionais
fcrCurve.properties.TF = TF;
fcrCurve.properties.freqVector = freqVector;
fcrCurve.properties.phase = phase;
fcrCurve.properties.windowSizeSamples = windowSizeSamples;
fcrCurve.properties.windowBandwidth = windowBandwidth;
% relacionadods a K-S
fcrCurve.KS.pv = pv;
fcrCurve.KS.statistic = KSstatistic;
fcrCurve.KS.cv = KScv;
fcrCurve.KS.KStrials = KStrials;
fcrCurve.KS.KSfcr = KSfcr;
end
