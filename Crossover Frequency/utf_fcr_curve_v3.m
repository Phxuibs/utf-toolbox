%% utf_fcr_curve v3 %%

% Calcula curva para a análise de frequência de transição (crossover
% frequency - f_{cr}) a partir de uma resposta ao impulso

% Autor: Luis Henrique Sant'Ana

%%% input %%%%
%%% Obrigatórios:
% x = resposta ao impulso (sinal bruto, não um objeto ITA, por exemplo)
% fs = taxa de amostragem [Hz]
%%% Parâmetros opcionais (name-value pairs)
% 'freqRange' = limites de frequências de análise (duplet) [Hz] (padrão: [0
% 1000])
% 'window_bandwidth' = tamanho da janela de análise (Hz) (padrão: 200)
% 'window_size_samples' = 
% 'spacing' = espaçamento entre janelas de análise subsequentes [Hz]. Se
% vazio, o espaçamento é de uma amostra. (padrão: [])
% 'method' = 'ks' (padrão) ou 'DUPH' ou 'kurtosis_real' ou 'kurtosis_imag'.
% Se for 'DUPH', calcula a frequência de transição a partir do máximo desvio de
% uma distribuição uniforme da fase (maximum Deviation from a Uniform
% distribution of PHase). Se for 'kurtosis_real', calcula a frequência de
% transição a partir da curtose normalizada (kurtosis(.)-3) da parte real
% da função de transferência. Para 'kurtosis_imag', o processo é
% semelhante, mas utiliza a parte imaginária da função de transferência.
% Para utilizar mais de um método, pode-se agrupá-los em um cell array
% (e.g. 'method',{'duph','kurtosis_real'}). Padrão:
% {'duph','kurtosis_real','kurtosis_imag'}.
% 'duph' pode ser 'duph' ou 'DUPH'.
%%% Se 'method' contiver 'duph':
% 'angle_interval' = intervalo de ângulos para cada barra do histograma
% [graus] (padrão: 20)
% 'normalization_flag' = se verdadeiro, realiza normalização da frequência 
% relativa e a referência para o cálculo do desvio é sempre 1,
% independentemente do intervalo de ângulo (ou equivalentemente, número de
% grupos); se falso, utiliza a frequência relativa dos ângulos de fase
% diretamente, sendo a referência = 1/nbins [boolean]. Padrão: true

%%% output %%%
%%% struct 'fcr' com variáveis:
% duph = máximo desvio de uma distribuição uniforme de um sinal de fase [.]
% kurtosis_real = curtose normalizada (kurtosis(.)-3) da parte real da
% função de transferência
% kurtosis_imag = curtose normalizada (kurtosis(.)-3) da parte imaginária
% da função de transferência
% f = frequências de cada valor de duph (e de cada histograma) e curtose,
% constando uma frequência inferior, uma central e uma superior, sendo
% então também correspondente a cada janela de análise [Hz]
% f_idx = índices que correspondem às frequências 'f', extraídas do vetor
% fcr.properties.freqVector
% D = struct com arrays para gerar as análises em barras que originaram
% cada valor de 'duph'. Sendo (i) a análise de interesse, basta usar:
% histogram('BinEdges',H.edges(i),'BinCounts',H.histo_counts(i))
% properties = struct com todas as variáveis de entrada e mais algumas que
% podem ser de interesse.

function [fcr_curve] = utf_fcr_curve_v3(x,fs,varargin)
%%  setup
% parsing (código no final do script)
[x,fs,freqRange,window_bandwidth,spacing,method,...
        angle_interval,normalization_flag] = utf_fcr_parsing(x,fs,varargin{:});
% vetor de frequências
freqVector = ( 0 : 1/length(x) : 1 - 1/length(x) ) * fs;
% função de transferência
X = fft(x);
% reolução em frequência
freqResolution = fs/length(x);
% largura da janela, em amostras
window_size = round( window_bandwidth / freqResolution );
% índices correspondentes aos limites de frequências
[~,idx_lower] = min( abs(freqVector - freqRange(1)) );
[~,idx_upper] = min( abs(freqVector - freqRange(2)) );
% estimativa (valor médio do espaçamento, em samples, e número de análises
% a serem feitas
if(~isempty(spacing))
    spacing_size = spacing / freqResolution;
    N = floor(( (idx_upper-idx_lower+1) - window_size ) / ...
                 spacing_size ) + 1;
else
    spacing_size = 1;
    N = ( (idx_upper-idx_lower+1) - window_size ) + 1;
end
% índices que correspondem a cada limite de janela
f_idx = nan(N,2);
% cada análise compreende uma faixa de frequências: limite inferior da
% janela e limite superior da janela:
f = nan(N,2);

% se for analisada por K-S test ou DUPH:
if(any(contains(method,{'ks','duph','DUPH'})))
    % ângulos de fase, em graus
    phase = angle( X ) / pi * 180;
end

% se for analisada por DUPH:
if(any(contains(method,{'duph','DUPH'})))
    % limites das barras de cada histograma
    edges = -180 : angle_interval : 180;
    % número de barras em cada histograma
    nbins = length(edges) - 1;
    if(~normalization_flag)
        duph_reference = 1 / nbins;
    else
        duph_reference = 1;
    end
    % para cada um dos 'N' histogramas:
    counts        = nan(N,nbins); % frequência (vulgo "número de ocorrências") de cada barra
    histo_counts  = nan(N,nbins); % valor de cada barra do histograma
    duph          = nan(N,1);     % máximo desvio ao valor de referência
end

% se for analisada por curtose da parte real:
if(any(contains(method,'kurtosis_real')))
    kurtosis_real  = nan(N,1); % valores de curtose da parte real
end

% se for analisada por curtose da parte imaginária:
if(any(contains(method,'kurtosis_imag')))
    kurtosis_imag  = nan(N,1); % valores de curtose da parte imaginária
end

% se for analisada por teste Kolmogorov-Smirnov:
if(any(contains(method,'ks')))
    U = makedist('uniform','lower',-180,'upper',180);
    h = nan(N,1); % resultado do teste
    p = nan(N,1); % p-valor dos testes
end

%% Análise

for i = 1 : N
    
    % primeiro índice da janela de análise
    if(~isequal(i,1))
        % índice da frequência inferior da análise 'i'
        if(~isempty(spacing))
            [~,f_idx(i,1)] = min(abs(freqVector - ...
                                 (freqRange(1) + (i-1)*spacing)));
        else
            f_idx(i,1) = f_idx(i-1,1) + 1;
        end
    else
        % índice da frequência inferior da análise 1
        f_idx(1,1) = idx_lower;
    end
    
    % índices limites da análise 'i'
    f_idx(i,2) = f_idx(i,1) + window_size - 1;
    % frequências correspondentes
    f(i,1) = freqVector(f_idx(i,1));
    f(i,2) = freqVector(f_idx(i,2));
    % com a análise dos parâmetros de DUPH, percebeu-se que o correto é
    % utilizar a frequência do começo da janela como referência, afinal,
    % todos os valores da janela estão sendo avaliados
    % a frequência f(:,2) (final da janela) antes era f(:,3).
%     f(i,2) = ( f(i,1) + f(i,3) ) / 2;

    % duph
    if(any(contains(method,{'duph','DUPH'})))
        % % frequência relativa (vulgo frequência)
        [counts(i,:),~] = histcounts(phase(f_idx(i,1):f_idx(i,2)),edges,...
                                     'normalization','probability');
        if(normalization_flag)
            % frequência relativa normalizada
            counts(i,:) = counts(i,:) * nbins;
        end
        % maior desvio da referência
        duph(i) = max(abs(counts(i,:) - duph_reference));
    end
    
    % curtose (o parâmetro '1' ou '0' diz respeito ao viés sistemático do
    % equacionamento. O padrão do Matlab é '1', sem correção.)
    if(any(contains(method,'kurtosis_real')))
        kurtosis_real(i) = kurtosis(real(X(f_idx(i,1):f_idx(i,2))),1)-3;
%         kurtosis_real(i) = kurtosis(real(X(f_idx(i,1):f_idx(i,2))),0)-3;
    end
    if(any(contains(method,'kurtosis_imag')))
        kurtosis_imag(i) = kurtosis(imag(X(f_idx(i,1):f_idx(i,2))),1)-3;
%         kurtosis_imag(i) = kurtosis(imag(X(f_idx(i,1):f_idx(i,2))),0)-3;
    end
    
    % kstest
    if(any(contains(method,'ks')))
        [h(i),p(i)] = kstest(phase(f_idx(i,1):f_idx(i,2)),'CDF',U);
    end
end
%% frequência de transição
if(any(contains(method,{'duph','DUPH'})))
    fcr_duph = utf_fcr(f(:,1),duph,'duph',window_size,angle_interval);
end
if(any(contains(method,'kurtosis_real')))
    fcr_kurtosis_real = utf_fcr(f(:,1),kurtosis_real,'kurtosis');
end
if(any(contains(method,'kurtosis_imag')))
    fcr_kurtosis_imag = utf_fcr(f(:,1),kurtosis_imag,'kurtosis');
end
if(any(contains(method,{'ks'})))
    fcr_ks = utf_fcr(f(:,1),p,'ks');
end
%% Preparar variáveis para retorno da função
% Agrupar variáveis relacionadads à duph em um struct (com exceção da DUPH
% em si):
if(any(contains(method,{'duph','DUPH'})))
    D.edges = edges;
    D.counts = counts;
    D.angle_interval = angle_interval;
    D.normalization_flag = normalization_flag;
    D.reference = duph_reference;
    D.phase = phase;
end
% Agrupar variáveis de entrada e parâmetros básicos em um struct:
properties.RI = x;
properties.fs = fs;
properties.freqRange = freqRange;
properties.window_bandwidth = window_bandwidth;
properties.spacing = spacing;
properties.method = method;
properties.FRF = X;
properties.freqVector = freqVector;
properties.freqResolution = freqResolution;
properties.window_size_samples = window_size;
properties.estimated_spacing_samples = spacing_size;
% preparar variável final
if(any(contains(method,{'duph','DUPH'})))
    fcr_curve.duph = duph;
    fcr_curve.D = D;
    fcr_curve.fcr_duph = fcr_duph;
end
if(any(contains(method,'kurtosis_real')))
    fcr_curve.kurtosis_real = kurtosis_real;
    fcr_curve.fcr_kurtosis_real = fcr_kurtosis_real;
end
if(any(contains(method,'kurtosis_imag')))
    fcr_curve.kurtosis_imag = kurtosis_imag;
    fcr_curve.fcr_kurtosis_imag = fcr_kurtosis_imag;
end
if(any(contains(method,'ks')))
    fcr_curve.ks = p;
    fcr_curve.ks_H = h;
    fcr_curve.fcr_ks = fcr_ks;
end
fcr_curve.f = f;
fcr_curve.f_idx = f_idx;
fcr_curve.properties = properties;
end

%% função de parseamento
function [IR,fs,freqRange,window_bandwidth,spacing,method,...
          angle_interval,normalization_flag] = utf_fcr_parsing(IR,fs,varargin)

% valores padrão dos pares (name-value pairs)
freqRangeDefault = [0 1000];
window_bandwidth_default = 200;
spacing_default = [];
method_default = {'ks'};
angle_interval_default = 20;
normalization_flag_default = true;

% parseamento
p = inputParser;
validSignal = @(x) isnumeric(x) && isvector(x);
validNumericScalar = @(x) isnumeric(x) && isscalar(x);
validDuplet = @(x) isnumeric(x) && isvector(x) && isequal(numel(x),2);
validSpacingSize = @(x) isempty(x) || (isnumeric(x) && isscalar(x));
validMethod = @(x) any(contains(x,{'ks','duph','DUPH',...
                                   'kurtosis_real','kurtosis_imag'}));
validLogicalScalar = @(x) islogical(x) && isscalar(x);
addRequired(p,'x',validSignal);
addRequired(p,'fs',validNumericScalar);
addParameter(p,'freqRange',freqRangeDefault,validDuplet);
addParameter(p,'window_bandwidth',window_bandwidth_default,validNumericScalar);
addParameter(p,'spacing',spacing_default,validSpacingSize);
addParameter(p,'method',method_default,validMethod);
addParameter(p,'angle_interval',angle_interval_default,validNumericScalar);
addParameter(p,'normalization_flag',normalization_flag_default,validLogicalScalar);
parse(p,IR,fs,varargin{:});

% preparar os valores de saída
freqRange = p.Results.freqRange;
window_bandwidth = p.Results.window_bandwidth;
spacing = p.Results.spacing;
method = p.Results.method;
angle_interval = p.Results.angle_interval;
normalization_flag = p.Results.normalization_flag;
end