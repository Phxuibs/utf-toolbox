%%%% utf_DUPH_ref %%
%
% Retorna valor de referência para determinação da frequência de transição
% a partir de uma cruva de DUPH
%
%
% input:
%
% windowSizeSamples: largura da janela, em amostras.
% numeric scalar
%
% angleInterval: intervalo de ângulos, em graus, utilizado para o
% agrupamento das amostras de fase para a análise de DUPH.
% numeric scalar (18 ou 20)
%
%
% output:
% DUPHrefrence: valor de referência para DUPH
%
%
% Autor: Luis Henrique Sant'Ana

function [DUPHreference] = utf_DUPH_ref(windowSizeSamples,angleInterval)
%% parseamento
p = inputParser;
addRequired(p,'windowSizeSamples',@(x) isscalar(x) && isnumeric(x));
addRequired(p,'angleInterval',@(x) isscalar(x) && isnumeric(x));
parse(p,windowSizeSamples,angleInterval);
% modelo simulado a partir de 50000 janelas utilizou janelas entre 500 e
% 2000 amostras, inclusive
if(windowSizeSamples<500 || windowSizeSamples>2000)
    error("'windowSizeSamples' deve respeitar: 500 <= windowSizeSamples <= 2000");
end
% verificar tamanho do intervalo utilizado
if(~isequal(angleInterval,18) && ~isequal(angleInterval,20))
    error("'angleInterval' deve ser 18 ou 20");
end
%% Setup
% largura das janelas simuladas, em amostras
windowSize = [500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600,1650,1700,1750,1800,1850,1900,1950,2000];
% determina o percentil de 95%
if(angleInterval == 20) % para 20 graus de intervalo de ângulos
    percentil = [0.548,0.538181818181818,0.5,0.495384615384615,0.465714285714286,0.448,0.44,0.418823529411765,0.42,0.402105263157895,0.388,0.382857142857143,0.374545454545455,0.361739130434783,0.355000000000000,0.352000000000000,0.343076923076923,0.333333333333333,0.331428571428571,0.328275862068965,0.320000000000000,0.312258064516129,0.305000000000000,0.301818181818182,0.301176470588235,0.296000000000000,0.290000000000000,0.284324324324325,0.280000000000000,0.280000000000000,0.278000000000000];
else % ou 18 graus
    percentil = [0.6,0.563636363636364,0.533333333333333,0.507692307692308,0.485714285714286,0.493333333333333,0.475,0.458823529411765,0.444444444444444,0.431578947368421,0.42,0.409523809523809,0.400000000000000,0.391304347826087,0.383333333333333,0.376000000000000,0.369230769230769,0.362962962962963,0.357142857142857,0.351724137931034,0.346666666666667,0.341935483870968,0.325000000000000,0.321212121212121,0.317647058823530,0.314285714285714,0.311111111111111,0.308108108108108,0.305263157894737,0.302564102564102,0.290000000000000];
end
%% Interpola e retorna o valor do 95º percentil (referência)
% interpolante
pp = pchip(windowSize,percentil);
% referência
DUPHreference = ppval(pp,windowSizeSamples);
end

% modelo que utiliza 1,96 desvio padrão
% DUPHreference = 1.39 * exp(-0.52 * log(windowSizeSamples/100));

% modelo que utiliza 1 desvio padrão
% DUPHreference = 1.17*exp(-0.51*log(windowSizeSamples/100));