%% utf_critical_distance %%

% Calculates the critical distance (i.e. reverberation distance or
% diffuse-field distance)

% Author: Luis H. Sant'Ana

% room parameter must be a scalar (volume) or a length 3 vector (the room
% dimensions)

function [r_c] = utf_critical_distance(T60,roomParameter,varargin)
defaultC = 343;

p = inputParser;
addRequired(p,'T60',@(x) isnumeric(x) && all(x>0))
addRequired(p,'roomParameter',@(x) isnumeric(x) && all(x>0) && (isscalar(x) || ...
                              (isvector(x) && isequal(length(x),3)) ));
addOptional(p,'c',defaultC,@(x) isnumeric(x) && isscalar(x) && all(x>0));
parse(p,T60,roomParameter,varargin{:});

c = p.Results.c;
%% Critical distance
% room volume
if(~isscalar(roomParameter))
    V = roomParameter(1) * roomParameter(2) * roomParameter(3);
else
    V = roomParameter;
end
% critical distance
r_c = sqrt( (6*log(10)*V) ./ (4*pi*c*T60) );
end