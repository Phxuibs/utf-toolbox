%% utf_schroeder_frequency %%

% calculate Schroeder frequency

% Author: Luis H. Sant'Ana

% roomParameter may be either the room Volume (scalar) or its dimensions
% (rectangular room) (vector of length 3)

function [f_S,f_1200] = utf_schroeder_frequency(T60,roomParameter,varargin)
%% parse
defaultM = 3; % desired modal overlap
defaultC = 343; % speed of sound

p = inputParser;
addRequired(p,'T60',@(x) isnumeric(x) && all(x>0));
addRequired(p,'roomParameter',@(x) isnumeric(x) && all(x>0) && (isscalar(x) || ...
                              (isvector(x) && isequal(length(x),3)) ));
addOptional(p,'m',defaultM,@(x) isscalar(x) && isnumeric(x) && all(x>0));
addOptional(p,'c',defaultC,@(x) isscalar(x) && isnumeric(x) && all(x>0));
parse(p,T60,roomParameter,varargin{:});

c = p.Results.c;
m = p.Results.m;
%% Schroeder Frequency
% room volume
if(~isscalar(roomParameter))
    V = roomParameter(1) * roomParameter(2) * roomParameter(3);
else
    V = roomParameter;
end
% Schroeder frequency
f_S = sqrt( (m*c^3*T60) / (12*log(10)*V) );
% Scroeder freqeuncy according to Vorlaender
f_1200 = 1200 * sqrt(T60/V);
end