%% utf_modal_overlap %%

% Calculates de modal overlap curve for a schoebox room with rigid walls

% Author: Luis H. Sant'Ana

function [M,f] = utf_modal_overlap(varargin)
%% parse
defaultF = 0:1000;
defaultL = [10 5 2.5].';
defaultT60 = 5;
defaultC = 343;
defaultOnlyObliqueModes = false;

p = inputParser;
addParameter(p,'f',defaultF,@(x) isnumeric(x) && isvector(x) && all(x>=0));
addParameter(p,'L',defaultL,@(x) isnumeric(x) && isvector(x) && all(x>0)...
                              && isequal(length(x),3));
addParameter(p,'T60',defaultT60,@(x) isnumeric(x) && isvector(x) && all(x>0));
addParameter(p,'c',defaultC,@(x) isnumeric(x) && isscalar(x) && all(x>0));
addParameter(p,'onlyObliqueModes',defaultOnlyObliqueModes,...
                @(x) isscalar(x) && islogical(x));
parse(p,varargin{:});

if(~isscalar(p.Results.T60))
    if(~isequal(length(p.Results.T60),length(p.Results.f)))
        error("'f' and 'T60' must have same length!");
    end
end

f = p.Results.f;
L = p.Results.L;
T60 = p.Results.T60;
c  = p.Results.c;
onlyObliqueModes = p.Results.onlyObliqueModes;
%% Modal Overlap calculation

% modal density of a rectangular room
V = L(1)*L(2)*L(3); % volume
n = (4*pi*V)/(c^3)*f.^2; % oblique (3D) modes
if(~onlyObliqueModes) % if all modes are desired to be counted
    S = 2*(L(1)*L(2) + L(1)*L(3) + L(2)*L(3)); % surface area
    P = sum(4*L); % perimeter
    n = n + (pi*S)/(2*c^2)*f + (P)/(8*c); % update modal density
end

% 3-dB bandwidth of a mode
Delta_f = (3*log(10)) ./ (pi*T60);

% Modal overlap
M = n .* Delta_f;
end