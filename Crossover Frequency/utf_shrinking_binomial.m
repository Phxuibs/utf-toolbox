%%%% utf_shrinking_binomial %%
%
% Determina a frequência de transição a partir de resultados de testes de 
% hipótese: 0 corresponde à não rejeição da hipótese nula (i.e. o
% comportamento estatístico é como o esperado acima da frequência de
% transição) e 1 corresponde à rejeição da hipótese nula.
%
% input:
% 
% trials: série de resultados dos testes.
% logical vector
%
% f: vetor de frequências correspondentes aos resultados dos testes em
% trials
% numeric vector
%
%
% output:
%
% fcr: frequência de transição
% numeric scalar ou NaN se uma frequência não pode ser determinada
%
% Autor: Luis H. Sant'Ana

function [fcr] = utf_shrinking_binomial(trials,f)
%% parseamento
p = inputParser;
addRequired(p,'trials',@(x) islogical(x) && isvector(x));
addRequired(p,'f',@(x) isnumeric(x) && isvector(x) && all(x>=0));
parse(p,trials,f);
%% Índices em que a hipótese nula não é rejeitada logo após uma rejeição
% hipótese nula rejeitada com shift para a direita
shifted = circshift(trials,1);
% considera avaliação no primeiro índice, caso a hipótese nula não tenha
% sido rejeitada
shifted(1) = 1;
% desconsidera avaliação no último índice, mesmo que a hipótese nula não
% tenha sido rejeitada
shifted(end) = 0;
% índices em que a hipótese nula não é rejeitada que são antecedidos por
% uma rejeição
idx = find(shifted & ~trials);
%% Teste por distribuição binomial
% a probabilidade do teste falhar se a hipótese nula for verdadeira - e as
% amostras independentes (?) - é o nosso nível de significância
% (probabilidade de falsos negativos). Para todos os testes (curtose, DUPH,
% K-S) ele é de 5%. Aqui, ela é a possibilidade do teste de Bernoulli dar
% certo.
p = 0.05;

% testa-se então janelas iniciadas com a hipótese nula não sendo rejeitada,
% logo após uma rejeição, até o final da série de testes até que uma janela
% passe no teste de distribuição binomial
passed = false;
for i = 1 : length(idx)
    janela = trials(idx(i):end);
    % quantidade de falhas no teste de frequência de transição (frequência
    % do evento ou vulgo "quantidade de ocorrências").
    freq = sum(janela);
    % a quantidade de amostras
    N = length(janela);
    % e constroi-se então a distribuição binomial
    binomialDist = makedist('Binomial','N',N,'p',p);
    % se a quantidade de acertos dos testes de Bernoulli for acima do
    % esperado para 95%, declara-se que a quantidade de falhas no teste de
    % frequência de transição é muito grande e a janela testada não
    % corresponde com o esperado acima da frequência de transição.
    % Há 95% de chance de obter uma quantidade de acertos dos testes de
    % Bernoulli igual ou menor a cv:
    binomialCV = icdf(binomialDist,0.95);
    % por este método, ele pega a quantidade acima de 0.95, mesmo que a
    % abaixo esteja com diferença absoluta mais próxima. Assim, o teste
    % para verificar se a quantidade de falhas no teste de frequência de
    % transição é aceitável é se a frequência de falhas no teste de
    % frequência de transição (ou acertos no teste de Bernoulli) é menor do
    % que cv (não mais igual ou menor, como citado antes):
    if(freq < binomialCV)
        passed = true;
        break;
    end
end
%% Frequência de transição
if(passed)
    if(~isequal(idx(i),1)) 
        % frequência de transição é a frequência correspondente à anterior
        % do teste dar certo:
        fcr = f(idx(i)-1);
    else
        % a frequência de transição é logo no primeiro índice
        fcr = f(1);
    end
else % fcr indeterminada
    fcr = NaN;
end
end