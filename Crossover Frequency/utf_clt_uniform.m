%%%% utf_clt_uniform %%
%
% Calcula o 5º percentil para a (tendente) distribuição normal obtida a
% partir da soma de N amostras provindas de uma distribuição uniforme na
% faixa [0,1]
%
% input:
%
% N: quantidade de amostras. Recomenda-se N > 10.
% numeric scalar.
%
%
% output:
%
% percentil: 5º percentil
%
% Autor: Luis H. Sant'Ana

function [percentil] = utf_clt_uniform(N)
%% parseamento
p = inputParser;
addRequired(p,'N',@(x) isnumeric(x) && isscalar(x) && all(x>0) );
parse(p,N);
%% Cálculo do percentil
% desvio padrão da distribuição normal tendente
sigma = 1 / sqrt(12) / sqrt(N);
% distribuição normal tendente
normalDist = makedist('normal','mu',0.5,'sigma',sigma);
% valor crítico para nível de significância de 5%
percentil = icdf(normalDist,0.05);
end