%% D para distribuição uniforme %%

% calcula o maior desvio absoluto de um grupo amostral x a uma distribuição
% uniforme U[-180,+180].

% Autor: Luis Henrique Sant'Ana

function [D,d_abaixo,d_acima] = utf_D_fase(x)
%% parse e ordenação
if(~isnumeric(x) || ~isvector(x))
    error("x não é numérico ou vetor!");
end
if(min(x)<-180 || max(x)>180)
    error("x não é limitado em [-180,+180]!");
end

% organiza x
x = sort(x);
% é impossível obter mesmos valores de uma distribuição contínua. Assim,
% não haverá tratamento para casos em que há valores repetidos. Se for esse
% o caso, acusa erro.
if(~isequal(x,unique(x)))
    error("Os valores de x não são únicos!");
end
%% D
% número de amostras
n = length(x);
% utilizar o objeto de distribuição evita normalizações
dist = makedist('uniform','lower',-180,'upper',180);

% Calcula desvio abaixo da curva (desvio à direita)
% aloca vetor para os desvios
d_abaixo = zeros([1 n]);
for j = 1 : n
    % frequência acumulada (até logo antes o quantil atual)
    freq = (j-1)/n;
    % numa distribuição uniforme, CDF(x) = x;
    d_abaixo(j) = cdf(dist,x(j)) - freq;
end

% Calcula desvio acima da curva (desvio à equerda)
% aloca vetor para os desvios
d_acima = zeros([1 n]);
% o último desvio sempre é igual a zero
for j = 1 : n
    % frequência acumulada
    freq = j/n;
    % numa distribuição uniforme, CDF(x) = x;
    d_acima(j) = freq - cdf(dist,x(j));
end

% desvio máximo e índice correspondente
D = max([d_abaixo d_acima]);

end