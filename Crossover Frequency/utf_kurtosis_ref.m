%%%% utf_kurtosis_ref %%
%
% Retorna o 95º de curtose de excesso para uma distribuição normal em
% função da quantiade de amostras
%
%
% input:
% windowSizeSamples = tamanho da janela, em amostras.
% numeric scalar
%
%
% output:
% ref = referência (95º percentil)
%
%
% Autor: Luis Henrique Sant'Ana

function ref = utf_kurtosis_ref(windowSizeSamples)
%% parseamento
p = inputParser;
addRequired(p,'windowSizeSamples',@(x) isscalar(x) && isnumeric(x));
parse(p,windowSizeSamples);

% modelo simulado a partir de 50000 janelas entre 500 e 1000 amostras
if(windowSizeSamples<500 || windowSizeSamples>1000)
    error("'windowSizeSamples' deve respeitar: 500 <= windowSizeSamples <= 1000");
end
%% Setup
% Largura das janelas simuladas, em amostras
windowSize = 500 : 100 : 1000;
% 95º percentil
percentil = [0.386481320093653,0.351960122995814,0.325726301229511,...
             0.302847320350776,0.284751063431864,0.269994057372368];
%% Interpola e apresenta o 95º percentil correspondente
% interpolante
pp = pchip(windowSize,percentil);
% 95º percentil correspondente
ref = ppval(pp,windowSizeSamples);
end