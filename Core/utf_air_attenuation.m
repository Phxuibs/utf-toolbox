%% utf_air_attenuation v_2 %%

% cálculo da atenuação do som pelo ar

% Autor: Luis Henrique Sant'Ana

% segue-se as equações de acordo com o artigo de Bass et. al
% "Atmospheric absorption of sound: Further developments"
% J. Acoust. Soc. Am. 97 (1), January 1995

% os resultados foram comparados com a ferramenta criada pelo professor
% Daniel A. Russel, da Pennsylvania State University, e mostraram-se
% acurados. Russel utilizou o mesmo artigo como referência.
% https://www.acs.psu.edu/drussell/Demos/Absorption/Absorption.html

% para possíveis conversões de unidades de pressão:
% https://www.unitconverters.net/pressure-converter.html
% qualquer unidade de pressão pode ser utilizada, mas é melhor utilizar atm
% para ficar em ordem de grandeza adequada (sem ajustes de potência de 10)
% pressão atmosférica de referência (atm): p_s0 = 1;

% para entradas de temperatura, umidade e frequência, aceita-se vetores.

% input:
% f = frequência (Hz) (vetor)
% T = temperatura (°C) (vetor)
% h_r = umidade relativa (%) (vetor)
% p_s = pressão atmosférica (escalar)
% p_s0 = pressão atmosférica de referência (escalar)

% as saídas são retornadas com dimensão correspondente aos vetores T e h_r

% output:
% alpha_dB = coeficiente de atenuação de pressão sonora pelo ar (dB / m)
% m = coeficiente de atenuação de energia sonora pelo ar (m^-1)
% nomes como utilizados pela ISO 17497-1:2004 (eq. 3, p. 8)

function [alpha_dB,m] = utf_air_attenuation(f,T,h_r,p_s,p_s0)
%% parse
p = inputParser;
addRequired(p,'f',@isvector);
addRequired(p,'T',@isvector);
addRequired(p,'h_r',@isvector);
addRequired(p,'p_s',@isscalar);
addRequired(p,'p_s0',@isscalar);
parse(p,f,T,h_r,p_s,p_s0);
% additional parsing
if(~isequal(size(T),size(h_r)))
    if(xor(isscalar(T),isscalar(h_r)))
        if(isscalar(T))
            T = repmat(T,size(h_r));
        else
            h_r = repmat(h_r,size(T));
        end
    else
        error("Os vetores T e h_r devem ser ambos linha ou coluna "+...
              "e de mesmo comprimento!");
    end
end
% ajuste de dimensão
% os coeficientes de atenuação ao longo da frequência serão dispostos em
% linhas, sendo os pares de temperatura e umidade correspondentes às
% colunas.
% Ao final, ajusta-se a matriz para corresponder à forma como os
% vetores de temperatura e umidade foram enviados. - removido
% transposeFlag = false;
if(size(T,1)>1)
%     transposeFlag = true;
    T = T';
    h_r = h_r';
end
if(size(f,2)>1)
    f = f';
end
%% constantes e conversão
% temperatura atmosférica de referência (K)
T_0 = 293.15;
% temperatura da tríplice isotérmica (K)
T_01 = 273.16;
% converter temperatura para kelvins
T = T + 273.15;
%% cálculos

% 1: pressão de saturação (atm) (eq 2 - "versão prática")
% p_sat = p_s0 * 10.^( -6.8346 * ( T_01 ./ T ) .^(1.261) + 4.6151 );
% (eq 1 - "versão não prática")
p_sat = p_s0 * 10.^( 10.79586 * (1 - ( T_01 ./ T ) ) -...
                    5.02808 * log10( T / T_01 ) + 1.50474 *...
                    10^(-4) * ( 1 - 10.^(-8.29692*( ( T / T_01 ) - 1 ) ) )-...
                    4.2873 * 10^(-4) *...
                    ( 1 - 10.^(-4.76955*( (T_01./T) - 1) ) ) -...
                    2.2195983 );

% 2: umidade absoluta (concentração molar de vapor de água) (%) (eq 6)
h = h_r .* ( p_sat / p_s );

% 3: frequências de relaxamento para oxigênio e nitrogênio (Hz) (eqs 4 e 5)
% (aqui é realizada a multiplicação pela pressão (p_s) pois, como explicado
% no artigo, as equações 4 e 5 na verdade calculam as frequências de
% relaxamento relativas à pressão, ou seja, f_O/p_s e f_N/p_s)
f_O = p_s/p_s0 .* ( 24 + 4.04 * 10^4 * h .* ( 0.02 + h )./( 0.391 + h ) );

f_N = p_s/p_s0 .* ( T_0 ./ T ).^(1/2) .* ...
                  ( 9 + 280 * h .* exp( -4.17 * ( (T_0./T).^(1/3) - 1 ) ) );
  
% 4: coeficiente de absorção (ou atenuação) pelo ar (neper / m) (eq 3)
% (devido à multiplicações explicadas no passo 3, não é necessário
% multiplicar o termo direito da equação 3 por p_s para obter o coeficiente
% de absorção (alpha))
% lembrar que operações básicas entre vetor linha ou coluna com uma matriz
% são realizadas de maneira, respectivamente, "linha-wise" ou "coluna-wise"
alpha = f.^2 / p_s0 .* (...
              1.84 * 10^(-11) * ( T / T_0 ).^(1/2) + ...
            ( T / T_0 ).^(-5/2) .* (...
              0.01278 * exp(-2239.1./T) ./ (f_O + f.^2 * f_O.^(-1)) +...
              0.1068  * exp(-3352./T)   ./ (f_N + f.^2 * f_N.^(-1)) ) );

% conversão pra decibel [dB / m]
alpha_dB = alpha * 20 * log10(exp(1));

% conversão para coeficiente de atenuação de energia (m^-1)
m = alpha_dB / (10 * log(exp(1)));
%% transposição, se aplicada
% if(transposeFlag)
%     alpha_dB = alpha_dB';
%     m = m';
% end
end