%% utf_NPS_mean %%

% Média e desvio padrão de valores de NPS

% Autor: Luis Henrique Sant'Ana

% input:
% NPS = valores de NPS dispostos na frequência ao longo das colunas, sendo
% a média desejada entre as linhas

% output:
% p = valores na pressão;
% NPS_mean = valores médios em dB;
% NPS_u = incerteza combinada em dB
% (calculada pela propagação gaussiana de incertezas);

function [p,NPS_mean,NPS_u] = utf_NPS_mean(NPS)

    % passando para pressão
    p = 10.^(NPS/10);
    
    % média para valores em dB
    NPS_mean = 10*log10(mean(p));
    
    % desvio padrão propagado pela função de conversão para dB
    % (derivada parcial da função multiplicada pela incerteza padrão)
    NPS_u = sqrt( ( 10./ ( log(10)*mean(p) ) ) .^2 .* ...
                  ( std(p) ./ sqrt( size(p,1) ) ) .^2 );
end