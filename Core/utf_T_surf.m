%% utf_T_surf %%

% Retorna T_60 apenas devido a superfícies 

% Autor: Luis Henrique Sant'Ana

% input:
% T_tot: T_60 devido a superfícies e ar (s) (vetor)
% f: vetor de frequências (vetor)
% parâmetros em par (opcionais)
% T: temperatura (° C) (escalar) padrão: 20
% h_r: umidade relativa (%) (escalar) padrão: 50
% p_s: pressão estática (escalar) padrão: 1 (atm)
% p_s0: pressão estática de referência (escalar) padrão: 1 (atm)

% output
% T_surf: T_60 devido apenas a superfícies

function [T_surf] = utf_T_surf(T_tot,f,varargin)
%% parse
defaultT = 20;
defaultH_r = 50;
defaultP_s = 1;
defaultP_s0 = 1;

p = inputParser;
validNumericScalar = @(x) isnumeric(x) && isscalar(x);
validNumericVector = @(x) isnumeric(x) && isvector(x);
addRequired(p,'T_tot',validNumericVector);
addRequired(p,'f',validNumericVector);
addParameter(p,'T',defaultT,validNumericScalar);
addParameter(p,'h_r',defaultH_r,validNumericScalar);
addParameter(p,'p_s',defaultP_s,validNumericScalar);
addParameter(p,'p_s0',defaultP_s0,validNumericScalar);
parse(p,T_tot,f,varargin{:});
if(~isequal(length(T_tot),length(f)))
    error("'T_tot' e 'f' devem possuir mesmo comprimento!");
end

T = p.Results.T;
h_r = p.Results.h_r;
p_s = p.Results.p_s;
p_s0 = p.Results.p_s0;
%% cálculo

alpha = utf_air_attenuation(f,T,h_r,p_s,p_s0); % atenuação dB/m
d_air = alpha * utf_speed_of_sound(T,[]); % atenuação dB/s
d_tot = 60 ./ T_tot; % decaimento total
if(~isequal(size(d_tot),size(d_air)))
    d_tot = d_tot';
end
T_surf = 60 ./ (d_tot - d_air);  % T_60 por superfícies
