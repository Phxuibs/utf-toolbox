%% utf_alpha_w %%

% Calcula alpha_w a partir de alpha_s de acordo com a ISO 11654

% Autor: Luis H. Sant'Ana

% input:
% f: bandas de terço de oitava
% alpha_s: coeficientes de absorção sonora correspondentes

% output:
% alpha_w: coeficiente de absorção sonora ponderado (weighted)
% shape: vetor de strings com os indicadores de formas (L,M,H)

function [alpha_w,shape,ref_shf,alpha_p,alpha_octave,f_octave] = utf_alpha_w(f,alpha_s)

% calcular alpha_s para bandas de oitava
f_octave = [250 500 1000 2000 4000];
alpha_octave = utf_third_to_full(alpha_s,f,f_octave);

% arredondar para o 0.05 mais próximo
alpha_p = utf_1_20_rounding(alpha_octave);

% reference curve
ref = [0.8 1 1 1 0.9];
% + 0.05 para uma primeira correção forçada
ref_shf = ref + 0.05;
% soma de desvios não favoráveis (desvios abaixo da referência)
unf_sum = 1; % 1 para forçar o primeiro loop
while(unf_sum>0.1)
    ref_shf = ref_shf - 0.05;
    idx = alpha_p < ref_shf;
    unf_sum = sum(ref_shf(idx) - alpha_p(idx));
end
alpha_w = ref_shf(2);

% verificar desvios acima da referência que sejam maiores ou iguais a 0.25
idx = (alpha_p - ref_shf)>=0.25;
shape = nan;
if(idx(1))
    disp("Shape L");
    shape = [shape "L"];
end
if(idx(2) || idx(3))
    disp("Shape M");
    shape = [shape "M"];
end
if(idx(4) || idx(5))
    disp("Shape H");
    shape = [shape "H"];
end
end