%% utf_WS %%

% This is an implementation of the Welch-Satterthwaite equation to
% calculate the effective number of degrees of freedom for combined
% uncertainty.

% This equation is valid when the quantities involved are independent and
% the function that relates them is linear [1].

% [1] JCGM 100. Evaluation of measurement data: guide to the expression of
% uncertainty in measurement. 2008.

% input:
% u: vector with the uncertainties of the individual quantities.
% nu: vector with the degrees of freedom of the individual uncertanties.
% u_c: single value which is the combined uncertainty.

% output:
% nu_eff: effective degrees of freedom.

% Author: Luis H. Sant'Ana

function nu_eff = utf_WS(u,nu,u_c)
%% Parsing
p = inputParser;
p.addRequired("u",@(x) isvector(x) && isnumeric(x));
p.addRequired("nu",@(x) isvector(x) && isnumeric(x));
p.addRequired("u_c",@(x) isnumeric(x) && isequal(length(x),1));
parse(p,u,nu,u_c);
%% Calculation
nu_eff = u_c^4 / sum(u.^4 ./ nu);
end