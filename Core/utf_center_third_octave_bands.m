%% utf_third_octave_bands %%

% retorna array com centros de terço de oitava com range delimitado

% Autor: Luis Henrique Sant'Ana

% input
% limits = tuplet com limite inferior e superior para centros de bandas de
% terço de oitava

function [freq_bands] = utf_center_third_octave_bands(limits)

    % faz que o primeiro valor do tuplet seja menor ou igual ao segundo
    while(limits(1) > limits(2))
        disp(['O primeiro valor da faixa de frequência deve ser menor ou' ...
              ' igual ao segundo.']);
        prompt = ['Digite um novo valor para limite inferior de faixa de' ...
                  ' frequência: '];
        limits(1) = input(prompt);
        prompt = ['Digite um novo valor para limite superior de faixa de' ...
                  ' frequência: '];
        limits(2) = input(prompt);
    end
    
    % cria array com os frequências centrais de terços de oitava
    third_octave_bands = [3.16 3.98 5.0 6.3 7.9 10.0 12.6 15.8 20.0...
        25 31.5 40 50 63 80 100 125 160 200 ...
        250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 ...
        6300 8000 10000 12500 16000 20000];
    
    % obtém os índices correspondentes às frequências de interesse
    [~,idx(1)] = min(abs(third_octave_bands - limits(1)));
    [~,idx(2)] = min(abs(third_octave_bands - limits(2)));
    
    % pega as frequências de interese com os índices encontrados
    freq_bands = nan(1,idx(2)-idx(1)+1);
    for i = idx(1) : idx(2)
        freq_bands(i-idx(1)+1) = third_octave_bands(i);
    end
end