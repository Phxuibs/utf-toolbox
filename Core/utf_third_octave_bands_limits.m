%% utf_octave_bands_limits %%

% retorna limites de bandas de terço de oitava para a faixa especificada

% Autor: Luis Henrique Sant'Ana

% input:
% freqRange = tuplet com centros de terços de oitava de interesse

% output:
% limites = limites para os centros correspondentes

function limites = utf_third_octave_bands_limits(freqRange)

if(~(isnumeric(freqRange) && isvector(freqRange)))
    error("'FreqRange' não é numérico ou vetor (ou ambos)!");
end

% limites
limits = [89.1 112 141 178 224 282 355 447 562 708 891 1122 1413 1778 2239 2818 3548 4467];

% centros correspondentes (aos limites mínimos)
center = [100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000];

% obtém os índices correspondentes às bandas de interesse
sort(freqRange);
[~,idx(1)] = min(abs(center - freqRange(1)));
[~,idx(2)] = min(abs(center - freqRange(2)));

% retorna os limites de interesse
limites = limits(idx(1) : idx(2)+1);