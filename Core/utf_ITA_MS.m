%% utf_ITA_MS_v2 %%
%
% Cria e edita um setup de medição do ITA toolbox
%
% input (pares nome-valor opcionais):
%    'input'   canais de entrada   (array)
%    'output'   canais de saída   (array)
%    'freqRange'   faixa de frequências   (tuplet)
%    'fftDegree'   grau de FFT   (escalar)
%    'samplingRate'   taxa de amostragem (escalar)
%    'stopMargin'   tempo de espera depois de enviar o sinal (escalar) (s)
%    'numAverages'   número de médias (escalar)
%    'latencySamples' quantidade de amostras a ser removida (consideração
% de latência) (escalar)
%
% output:
%    'MS' objeto de setup de medição ITA
%
% Observações:
% É inútil ter várias instâncias de um objeto setup de medição: ao alterar
% um setup de medição, todos os setups são alterados, pois correspondem à
% mesma coisa;
% 
% Ao ajustar 'freqRange' ou 'stopMargin' o sinal de excitação é recriado
% (provavelmente ao alterar fftDegree e samplingRate também, mas não foi
% testado)
%
% Autor: Luis Henrique Sant'Ana

function [MS] = utf_ITA_MS(varargin)
%% parsear parâmetros de entrada

% valores padrão
defaultInput = 1;
defaultOutput = 1;
defaultFreqRange = [500 20000];
defaultFFTDegree = 17;
defaultFS = 48000;
defaultSM = 0.1;
defaultAVG = 5;
defaultLatencySamples = 0;

% criar o sistema de parseamento e validar
p = inputParser;
isNumericVector = @(x) isnumeric(x) && isvector(x);
addParameter(p,'input',defaultInput,isNumericVector);
addParameter(p,'output',defaultOutput,isNumericVector);
addParameter(p,'freqRange',defaultFreqRange,isNumericVector);
addParameter(p,'fftDegree',defaultFFTDegree,@isscalar);
addParameter(p,'samplingRate',defaultFS,@isscalar);
addParameter(p,'stopMargin',defaultSM,@isscalar);
addParameter(p,'numAverages',defaultAVG,@isscalar);
addParameter(p,'latencySamples',defaultLatencySamples,@isscalar);
parse(p,varargin{:});

% passar valores validados para variáveis a serem utilizadas
input = p.Results.input;
output = p.Results.output;
freqRange = p.Results.freqRange;
fftDegree = p.Results.fftDegree;
samplingRate = p.Results.samplingRate;
stopMargin = p.Results.stopMargin;
numAverages = p.Results.numAverages;
latencySamples = p.Results.latencySamples;
%% criação e edição do objeto de medição

MS = itaMSTF(); % criar objeto de setup e medição

% alterar parâmetros que (provavelmente) recriam o sinal de excitação
MS.freqRange = freqRange; % faixa de frequências para o sweep
MS.fftDegree = fftDegree; % grau de FFT
MS.samplingRate = samplingRate; % taxa de amostragem
MS.stopMargin = stopMargin; % silêncio após sinal

% alterar outros parâmetros
MS.inputChannels = input; % canais de entrada
MS.outputChannels = output; % canais de saída
MS.averages = numAverages; % número de médias
MS.outputamplification = '0'; % Deixar amplificação em 0dBFS
MS.lineardeconvolution = true; % ativar deconvolução linear
MS.latencysamples = latencySamples; % considerar latência

end
