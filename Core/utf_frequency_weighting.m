%% utf_frequency_weighting %%
% Returns the frequency weighting for a given frequency and weighting
% (either 'A' or 'C')
% input:
% weighting: "A" or "C" (string)
% f: frequency (numeric vector)
% optional input:
% rounding: if rounding shoulud be applied to the final values (logical)
% output:
% weightingFunction: the weighting to be applied, in decibels (numerical
% vector)
% Author: Luis H. Sant'Ana
% The equations were obtained from Wikipedia, which cites the ISO 61672
% directly.
% Deviations of 0.1 dB were observed from the tabular data for some octave
% bands on the A weighting from other sources.
% source: https://en.wikipedia.org/wiki/A-weighting#Function_realisation_of_some_common_weightings
function [weightingFunction] = utf_frequency_weighting(weighting,f,varargin)
%% parse
defaultRounding = false;
p = inputParser;
p.addRequired('weighting',@(x) isstring(x) && isscalar(x));
p.addRequired('f',@(x) isnumeric(x) && isvector(x));
p.addOptional('rounding',defaultRounding,@(x) islogical(x));
parse(p,weighting,f,varargin{:});
rounding = p.Results.rounding;
%% Weighting functions declarations
% for A
RA = @(f) (12194^2 * f.^4) ./ ...
          ( (f.^2+20.6^2) .* sqrt( (f.^2+107.7^2) .* (f.^2+737.9^2) ) .* (f.^2+12194^2) );
% for C
RC = @(f) (12194^2 * f.^2) ./ ( (f.^2+20.6^2) .* (f.^2+12194^2) );
%% Calculate weighting
switch weighting
    case "A"
        weightingFunction = 20*log10(RA(f)) - 20*log10(RA(1000));
    case "C"
        weightingFunction = 20*log10(RC(f)) - 20*log10(RC(1000));
end
%% Rounding
if(rounding)
    weightingFunction = round(weightingFunction*10)./10;
end
end