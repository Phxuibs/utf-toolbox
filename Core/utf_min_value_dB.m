%% utf_min_value_dB %%

% O objetivo é identificar o índice correspondente ao menor valor do sinal,
% em dB.
% Isso é feito a partir de uma média móvel, pois qualquer valor em zero
% resultaria em um valor mínimo

% Autor: Luis Henrique Sant'Ana

% input:
% RI = array que representa RI (RI em si); valores de amplitude ao longo
% das linhas; frequências ao longo das colunas
% time array = vetor de tempo
% fs = taxa de amostragem da RI
% output:
% idx = instante para corte
% trend = média móvel do sinal
% t_trend = vetor de tempo da média móvel

function [idx,trend,t_trend] = utf_min_value_dB(RI,time_array,fs)
%% ajuste do sinal para trend
% passa RI para dB
RI_dB = 10*log10(RI.^2./max(RI.^2,[],1));
% ajustar -inf para o menor valor da resposta
% primeiro: passar -inf para NaN
RI_dB(RI_dB == -inf) = nan;
% segundo: pegar menores valores por frequência
min_value = min(RI_dB,[],1,'omitnan');
% teceiro: para cada frequência, passar NaN para o menor valor
for j = 1 : size(RI_dB,2)
    RI_dB(isnan(RI_dB(:,j)),j) = min_value(j);
end
%% preparos de janela e vetor de tempo
% resolução, em segundos, do trend
trend_resolution = 0.05;
% tamanho da janela para a média do trend
window_size = trend_resolution * fs;
% tamanho da primeira dimensão do trend (quantas médias serão feitas)
% no caso, faz-se médias do tamanho da janela; a última média é do tamanho
% da janela mais os samples que sobrariam
length_trend = floor(length(RI_dB(:,1))./window_size);
% vetor de tempo para do trend (precisa do array de tempo da RI)
t_trend = (trend_resolution/2 : trend_resolution : time_array(end))';
% verificar se o último elemento do t_trend deve ser retirado, já que os
% samples que sobrarem vão junto na última janela
if(~isequal(length(t_trend),length_trend))
    t_trend = t_trend(1:end-1);
end
%% trend em si
% criação do array para o trend
trend = nan(length_trend,size(RI_dB,2));
% calcula-se o trend em si, por banda de frequência
for j = 1 : size(RI_dB,2)
    % primeira até a penúltima janela
    for i = 1 : size(trend,1)-1
        trend(i,j) = mean(RI_dB(1+((i-1)*window_size):...
                          window_size+((i-1)*window_size),j));
    end
    % última janela (window_size + samples que sobrariam)
    trend(end,j) = mean(RI_dB(1+(i*window_size):end,j));
end
%% identificar valor mais baixo do trend
% índice dos valores mínimos do trend
[~,trend_min_idx] = min(trend,[],1);
% tempo correspondente
t = t_trend(trend_min_idx);
% índice correspondente na RI
idx = round(t*fs);
end