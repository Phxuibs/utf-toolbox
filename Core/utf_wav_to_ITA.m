%% utf_wav_to_ITA %%

% Loads a .wav file and converts it to an ITA object

% input:
% filename: name of the .wav file (without the ".wav")(string)
% (optional)
% path: path to the .wav file (string)

% output:
% ITA: ITA object

% Author: Luis Henrique Sant'Ana

function ITA = utf_wav_to_ITA(filename,varargin)
%% parse
defaultPath = "";
p = inputParser;
p.addRequired("filename",@(x) isStringScalar(x));
p.addOptional("path",defaultPath,@(x) isStringScalar(x));
parse(p,filename,varargin{:});
path = p.Results.path;
%% Load .wav file and convert it to an ITA object
% read audio ('fs' is the sampling rate):
[audio,fs] = audioread(path + filename + ".wav");
% create an ITA object
ITA = itaAudio;
ITA.samplingRate = fs; % set sampling rate
ITA.time = audio; % set the time data
end