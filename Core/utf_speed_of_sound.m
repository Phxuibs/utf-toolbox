%%%% utf_speed_of_sound %%
%
% Calcula a velocidade do som para determinada temperatura.
% O método para cálculo de incertezas é a propagação gaussiana;
%
%
% input (opcionais):
% t: temperatura, em graus Celsius.
% numeric array
% ut: incerteza padrão da temperatura, em graus Celsius
% numeric array
%
%
% output:
% speed = velocidade do som, em metros por segundo
% uSpeed = incerteza da velocidade do som, em metros por segundo
%
%
% Autor: Luis Henrique Sant'Ana

function [speed,varargout] = utf_speed_of_sound(varargin)
%% parse
p = inputParser;
addOptional(p,'t',nan,@(x) isnumeric(x));
addOptional(p,'ut',nan,@(x) isnumeric(x));
parse(p,varargin{:});

t = p.Results.t;
ut = p.Results.ut;

% se não foi inserida uma temperatura, retorna um valor padrão
if(isnan(t))
    speed = 343;
    return
else
    % se foi, verifica se deve calcular incerteza
    if(~isnan(ut))
        flagU = true;
        % e então verifica se as dimensões entre os valores de temperatura
        % e sua incerteza são iguais
        if(~isequal(size(t),size(ut)))
            error("Dimensões de 't' e 'ut' devem ser semelhantes!");
        end
    else
        flagU = false;
    end
end  
%% Definição de constantes
a = 343.2;
b = 273.15;
c = 293.15;
%% Cálculo da velocidade do som
speed = a * sqrt( ( b + t ) / c );
%% Cálculo de incerteza
if(flagU)
    dspeed_dt = a^2 ./ (2*c*speed);
    uSpeed = sqrt( dspeed_dt.^2 .* ut.^2 );
    varargout{1} = uSpeed;
end
end