%% utf_k %%

% retorna fator de abrangência k (coeficiente t de student) de 95 % para nu
% graus de liberdade

% Autor: Luis H. Sant'Ana

function k = utf_k(nu)

k = tinv(0.975,nu);