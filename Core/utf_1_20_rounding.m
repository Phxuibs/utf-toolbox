%% utf_1_20_rounding %%

% Arredonda números até a segunda casa decimal em passos de 0.05
% (necessário para calcular alpha_w, por exemplo, pela ISO 11654)

% Autor: Luis H. Sant'Ana

function y = utf_1_20_rounding(x)

% valores da primeira casa decimal para frente à esquerda
a = fix(x*10)/10;
% valores da segunda casa decimal para trás à direita
b = x - a;

y = nan(size(x));
for i = 1 : length(x)
    if(b(i)<=0.05)
        y(i) = a(i) + (b(i)>0.025)*0.05;
    else
        y(i) = a(i) + 0.05 + (b(i)>=0.075)*0.05;
    end
end
