%% utf_dB_mean %%

% Calculates the average of logarithmic values

% input: x (numeric vector).
% output: y (numeric vector).

% Author: Luis H. Sant'Ana

function y = utf_dB_mean(x)

y = 10 * log10( mean( 10.^(x/10), 1 ) );

end