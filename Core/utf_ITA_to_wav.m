%% utf_ITA_to_wav %%

% Carrega objetos ITA e salva cada canal como wav (normalizado).

% input:
% ITA: single ITA audio object.
% (name-value pairs)
% path: path to folder to save the .wav files (string)
% filename: filename prefix to save the .wa files (string)

% Author: Luis H. Sant'Ana

function utf_ITA_to_wav(ITA,varargin)
%% parse
defaultPath = "";
defaultFilename = "ITA";
p = inputParser;
p.addParameter("path",defaultPath,@(x) isStringScalar(x));
p.addParameter("filename",defaultFilename,@(x) isStringScalar(x));
parse(p,varargin{:});
path = p.Results.path;
filename = p.Results.filename;
%% Convert ITA object to .wav file and save it
ITA = ita_normalize_dat(ita_extract_dat(ITA));
for j = 1 : length(ITA.channelUnits)
    audiowrite(path+filename+"_channel_"+j+".wav",ITA.time(:,j),ITA.samplingRate);
end
end