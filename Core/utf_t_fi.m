%% utf_t_fi v2 %%

% Calcula quanto tempo há entre frequências f_i até o final do sweep

% Autor: Luis Henrique Sant'Ana

% input: (todos são parâmetros; optional name-value pairs)
%   'f_i' frequência(s) de interesse (vetor) (Hz)
%   'f_start'   frequência de início do sweep   (escalar)   (Hz)
%   'f_end'   frequência final do sweep   (escalar)   (Hz)
%   'fftDegree'   grau de FFT   (escalar)   (.)
%   'fs'   taxa de amostragem   (escalar)   (Hz)
%   'sm'   stop margin; quanto o sweep é encurtado para promover silêncio
%   ao final (escalar)   (s)
%   'pmWhole' se deve aumentar a faixa do sweep em mais e menos 1 tom
%   (semelhante ao que o ITA faz) (escalar boolean)

% output:
%   'dur'   tempo de reverberação "disponível" (vetor) (s)
%   't'   instante em que a frequência é gerada (vetor) (s)
%   'T'   duração do sinal (escalar numérico) (s)

function [dur,t,T] = utf_t_fi(varargin)
%% parse
defaultF_i = utf_filter_mid_frequency(-3:12,3,2);
[~,defaultF_i] = utf_filter_half_band_frequencies(defaultF_i,3,2);
defaultF_start = utf_filter_mid_frequency(-3,3,2);
defaultF_end = utf_filter_mid_frequency(12,3,2);
defaultFFTDegree = 16;
defaultFS = 48000;
defaultSM = 0.2;
defaultPMWhole = true;

p = inputParser;
validScalarPosNum = @(x) isnumeric(x) && isscalar(x) && (x > 0);
addParameter(p,'f_i',defaultF_i,@(x) isnumeric(x) && isvector(x));
addParameter(p,'f_start',defaultF_start,validScalarPosNum);
addParameter(p,'f_end',defaultF_end,validScalarPosNum);
addParameter(p,'fftDegree',defaultFFTDegree,validScalarPosNum);
addParameter(p,'fs',defaultFS,validScalarPosNum);
addParameter(p,'sm',defaultSM,validScalarPosNum);
addParameter(p,'pmWhole',defaultPMWhole,@(x) islogical(x) && isscalar(x));
parse(p,varargin{:});

f_i = p.Results.f_i;
f_start = p.Results.f_start;
f_end = p.Results.f_end;
fftDegree = p.Results.fftDegree;
fs = p.Results.fs;
sm = p.Results.sm;
pmWhole = p.Results.pmWhole;
%% range adjustment with +- whole step, if requested

if(pmWhole)
    % um tom equivale a meia banda de terço de oitava
    % ITA utiliza filtros de base 2 (ita_mpb_filter)
    f_start = utf_filter_half_band_frequencies(f_start,3,2);
    [~,f_end] = utf_filter_half_band_frequencies(f_end,3,2);
end

flag = arrayfun(@(x) f_start <= x && x <= f_end, f_i);
if(~isequal(sum(~flag),0))
    error("Frequencies of interest are outside the sweep range!");
end

%% script em si

% duração do sinal
T = 2^fftDegree / fs;
% instantes para as frequências de interesse
t = ( (T-sm) * ( log(f_i)-log(f_start) ) / log(f_end/f_start));
% tempo restante até o final do sweep
dur = T - t;
end