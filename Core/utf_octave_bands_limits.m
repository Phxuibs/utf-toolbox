%% utf_octave_bands_limits %%

% retorna limites de bandas de oitava para a faixa especificada

% Autor: Luis Henrique Sant'Ana

% input:
% freqRange = tuplet com centros de oitava de interesse

% output:
% limites = limites para os centros correspondentes

function limites = utf_octave_bands_limits(freqRange)

if(~(isnumeric(freqRange) && isvector(freqRange)))
    error("'FreqRange' não é numérico ou vetor (ou os dois)!");
end

% limites
limits = [11 22 44 88 177 355 710 1420 2840 5680 11360 22720];

% centros correspondentes (aos limites mínimos)
center = [16 31.5 63 125 250 500 1000 2000 4000 8000 16000];

% obtém os índices correspondentes às bandas de interesse
sort(freqRange);
[~,idx(1)] = min(abs(center - freqRange(1)));
[~,idx(2)] = min(abs(center - freqRange(2)));

% retorna os limites de interesse
limites = limits(idx(1) : idx(2)+1);