%% utf_duplicate_t_h %%

% A silly function just to adapt the temperature and humidity values from
% the 6 measurement positions for the scattering coeffcient measurement to
% 12, since only 6 values are registered due to the use of 2 microphones.

% Author: Luis Henrique Sant'Ana

% input:
% x: array to be adjusted (measurement positions are expected to be
% alongside the first dimension).

% output:
% y: adjusted array

function [y] = utf_duplicate_t_h(x)
    y = nan([size(x,1)*2 size(x,2)]);
    for i = 1 : size(x,1)
        idx = i + (i-1);
        y(idx:idx+1,:) = repmat(x(i,:),[2 1]);
    end
end