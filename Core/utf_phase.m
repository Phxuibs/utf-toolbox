%% utf_phase %%

% a partir de RI, adquire fase e vetor de frequências

% Autor: Luis Henrique Sant'Ana

% input:
% RI = resposta aoa impulso (sinal no tmepo, previamente cortado)
% fs = taxa de amostragem da RI

% output:
% f = o vetor de frequências correspondentes
% phase = fase

function [f, phase] = utf_phase(RI,fs)
%% phase
% get the signal phase
phase = angle(fft(RI));
% only half of which is of interest
phase = phase(1 : floor(end/2));
% convert to degrees
phase = 180/pi * phase;
%% frequency
f = linspace(0, fs, length(RI) + 1)';
f = f(1 : end-1);
f = f(1 : floor(end/2));
end