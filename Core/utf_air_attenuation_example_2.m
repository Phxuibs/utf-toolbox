%% plotting sound attenuation %%
clear
clc
close all
f = logspace(1,6,1000);
T = 20;
h_r = [0 10 20:20:100];
alpha_dB = utf_air_attenuation(f,T,h_r,1,1);
%%
% h = figure(1);
psvg = figure (1);
hold on
for j = 1 : size(alpha_dB,2)
    p(j) = plot(f,alpha_dB(:,j)*100,'linewidth',2);
end
hold off
% ax = utf_axes(gca,24);
ax = gca;
ax.XScale = 'log';
ax.YScale = 'log';
xlim(ax,[f(1) f(end)]);
yAxis = logspace(-3,4,8);
ylim([yAxis(1) yAxis(end)]);
ax.YTick = yAxis;
grid on
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';
L = legend(string(h_r),'interpreter','latex','location','southeast','numcolumns',2);
% L.Title.String = "$h_\mathrm{r} / p_\mathrm{s}$ [\% / atm]";
L.Title.String = "h_r / p_s [% / atm]";
L.Title.Interpreter = 'tex';
xlabel(["frequency / pressure"; "f / p_s [Hz / atm]"]);
ylabel(["absorption coefficient / pressure"; "a / p_s " + ...
        "[Hz / (100 m \times atm)]"]);
ax.MinorGridLineStyle = '-';
ax.MinorGridColor = [0 0 0];
ax.GridColor = [0 0 0];
ax.MinorGridAlpha = 0.5;
ax.GridAlpha = 0.8;
title(["Sound absorption coefficient per atmosphere for air at 20 °C"; ...
       "according to relative humidity per atmosphere"]);
ax.FontName = 'Times New Roman';
ax.FontSize = 22;
L.Title.FontWeight = "normal";
% ax.TickLabelInterpreter = 'latex';
% ax.Title.Interpreter = 'latex';
% ax.XLabel.Interpreter = 'latex';
% ax.YLabel.Interpreter = 'latex';

% ax = gca;
% ax.XScale = 'log';
% ax.YScale = 'log';
% xlabel({'f / p_s [Hz/atm]';'Frequency/pressure'},'FontSize',10,...
%     'FontWeight','normal','FontName','Times New Roman');
% ylabel('Absorption coefficient/pressure a / p_s [dB/100 m atm]',...
%     'FontSize',10,'FontWeight','normal','FontName','Times New Roman');
% title({'Sound absorption coefficient per atmosphere for air at 20°C ';...
%     'according to relative humidity per atmosphere'},...
%     'FontSize',10,'FontWeight','bold','FontName','Times New Roman')
% hleg = legend('   0','   10',...
%     '   20','   40','   60',...
%     '   80','   100');
% v = get(hleg,'title');
% set(v,'string',{'h_r / p_s [%/atm]'},'FontName','Times New Roman','FontSize',10);
% set(hleg,'Location','SouthEast','EdgeColor','black')
% axis([1e1 1e6 1e-3 1e4]);
% grid on;
% set(gca,'gridlinestyle','-');
% set(gca,'MinorGridLineStyle','-')