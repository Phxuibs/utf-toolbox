%% utf_air_attenuation_example %%

% Exemplo de uso da função de cálculo do coeficiente de atenuação sonora
% pelo ar (alpha [dB/m]);

% Autor: Luis Henrique Sant'Ana

%% setup
clear
clc
freqRange = [100 20000];
f = utf_center_third_octave_bands(freqRange);
h_r = [30 60 90];
T = [5 15 25];
p_s0 = 1;
p_s = 1;
%% cálculo

% preparar o array com coeficiente de atenuação
alpha_dB = nan(length(f),length(T),length(h_r));

% cálculo em si
for k = 1 : length(h_r)
    alpha_dB(:,:,k) = utf_air_attenuation(f,T,h_r(k),p_s,p_s0);
end

% coeficente para (dB/s)
alpha_dB_s = nan(size(alpha_dB));
for j = 1 : size(alpha_dB,2)
    alpha_dB_s(:,j,:) = alpha_dB(:,j,:) * utf_speed_of_sound(T(j),[]);
end
%% plot
close all
h = figure();

style = ["--",":","-"];
cores = ["azul_claro","verde","vermelho"];
hold on
for k = 1 : length(h_r)
    for j = 1 : length(T)
        plot(f,alpha_dB_s(:,j,k),style(k),'linewidth',2,'color',...
             utf_color(cores(j)));
    end
end
% plot pra legenda
P = 1;
for k = 1 : length(h_r)
     p(P) = plot(24000,0,style(k),'linewidth',2,...
                 'color',[0 0 0]);
     P = P + 1;    
end
for j = 1 : length(T)
    p(P) = plot(24000,0,'-','linewidth',2,...
                 'color',utf_color(cores(j)));
    P = P + 1;
end
hold off
ax = gca;
ax = utf_axes(ax,22);
ax.XScale = 'log';
ax.YScale = 'log';
ax.XLim = freqRange;
ax.YLim = [min(alpha_dB_s,[],'all') max(alpha_dB_s,[],'all')];
ax.XTick = f;
ax.XTickLabel = f;
ax.XTickLabelRotation = 45;
lgd = legend(p,string([h_r T]),'Location','Northwest','interpreter','latex',...
             'numcolumns',2);
title(lgd, "$h_r$ (\%); $T$ ($^{\circ}$C)",...
      'interpreter','latex');
xlabel("Frequ\^{e}ncia (Hz)");
ylabel("$\alpha$ (dB/s)");
title(string(p_s) + " atm");
ax = utf_minor_grid(ax);
ax.XMinorGrid = 'off';
ax.XMinorTick = 'off';
ax.Box = 'on';

utf_adjust_plot_fine(h,"utf_air_attenuation_example",[34 22]);