%% utf_t_fi %%

% Cálculo do instante que corresponde a determinada frequência no sweep log
% e quanto tempo ela possui até o final do sinal

% Autor: Luis Henrique Sant'Ana

% input:
%   'fi'   frequência de interesse (Hz)
%   'f_start'   frequência de início do sweep   (escalar)   (Hz)
%   'f_end'   frequência final do sweep   (escalar)   (Hz)
%   'fftDegree'   grau de FFT   (escalar)   (.)
%   'fs'   taxa de amostragem   (escalar)   (Hz)
%   'sm'   stop margin   (escalar)   (s)

% output:
%   'dur'   tempo de reverberação "disponível" (s)
%   't'   instante em que a frequência gerada (s)
%   'T'   duração do sinal (s)

function [dur,t,T] = utf_t_fi(fi,f_start,f_end,fftDegree,fs,sm)

% duração do sinal
T = 2^fftDegree / fs;
% instante para fi
t = ( (T-sm) * ( log(fi)-log(f_start) ) / log(f_end/f_start));
% tempo restante até o final do sweep
dur = T - t;

end