%% utf_third_to_full %%

% Calcula a média aritmética de um coeficiente 'x', em terços de oitava
% 'f', para bandas de oitava 'f_octave'

% Autor Luis H. Sant'Ana

function x_octave = utf_third_to_full(x,f,f_octave)

x_octave = nan(size(f_octave));
for i = 1 : length(x_octave)
    idx = find(f==f_octave(i));
    if(nnz(isnan(x(idx-1:idx+1)))>0)
        disp("Valor calculado para banda de oitava excluindo nan!");
    end
    x_octave(i) = nanmean(x(idx-1:idx+1));
end