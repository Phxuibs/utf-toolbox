%% utf_LZeq_BK %%

% Pega valores de LZeq da planilha no "formato BK"

% Autor: Luis Henrique Sant'Ana

% A planilha deve estar de acordo com o padrão do software da BK "BZ-5503
% Measurement Partner Suite" (i.e. a pasta de planilhas contém uma planilha
% com nome 'TotalSpectra', com frequências dispostas nas colunas, no
% formato:
% 'LZeq <valor_central_da_banda>Hz' para bandas abaixo de 1000Hz e
% 'LZeq <valor_central_da_banda>kHz' para bandas de 1000Hz e acima desta).
% A padronização dos nomes das células (arquivos salvos no analisador) é
% importante, pois a rotina buscará por um termo especificado pelo usuário
% além de termo específico acompanhado de algarismos contadores (i.e.
% '1','2','3',...) para identificação de medições com diferentes posições
% de fonte sonora.

% input:
% file = string ou char array com nome da planilha (e.g 'planilha_1.xls');
% type_snip = trecho do nome a ser buscado nas linhas da planilha
% correspondente ao tipo de medições (e.g. 'bn', "backgorund noise", para
% uma medição de ruído de fundo);
% source_snip = trecho do nome a ser buscado nas linhas da planilha que
% acompanha a posição de fonte sonora; por exemplo, se 'source_snip' for
% 'f' (fonte), será buscado por 'f1', 'f2', ..., 'fn_sources'); no caso de
% medição de ruído de fundo, por exemplo, em que não há fonte sonora, pode
% ser deixado caracter vazio (i.e.'');
% nsources = quantidade de posições de fonte sonora;
% nmics = quantidade de posições de microfone, mas literalmente quantas
% medições foram feitas para cada posição de fonte; se 'source_snip' é '',
% então é a quantidade de medições a serem encontradas com o snip
% 'type_snip'; nota-se que deve haver mesma quantidade de posições de
% microfone (medições) para cada posição de fonte sonora;
% freq_range = tuplet com a faixa de frequência de interesse (os valores
% devem ser compatíveis com os apresentados na planilha); exemplo:
% [50 10000] 

% output = array com os vlores da planilha; n_mics linhas, frequências
% dispostas nas colunas, n_sources níveis

function [output] = utf_LZeq_BK(file,type_snip,source_snip,n_sources,...
                                n_mics,freq_range)
%% carregar planilha
    % carrega planilha (parte numérica e parte de texto)
    [num,txt] = xlsread(file,'TotalSpectra');
%% índices de linhas

    % verifica quais linhas tem 'type_snip' e 'source_snip', através da
    % textual da planilha (txt) e salva o índice da linha em row_idx
    row_idx = nan(n_mics,n_sources);
    for j = 1 : n_sources
        if(strcmp(source_snip,'')) % se source_snip é caracter vazio, busca
            % apenas por linhas que tenham o type_snip
            has_snip = ~cellfun('isempty',strfind(txt(:,1), type_snip));
        else % caso contrário, busca por linhas que tenham tanto o
            % source_snip quanto o type_snip
            has_snip = ~cellfun('isempty',strfind(txt(:,1), ...
                       [source_snip num2str(j)])) &...
                       ~cellfun('isempty',strfind(txt(:,1), type_snip));
        end % has_snip é um vetor lógico
        
        if(sum(double(has_snip)) ~= n_mics) % verifica se a quantidade de
            % linhas com os snips desejados para a posição de fonte atual
            % (j) não corresponde a quantidade esperada (n_mics)
            error("Quantidade de posições de microfone identificadas " +...
                  "não corresponde com a declarada para snip '%s' e " +...
                  "posição de fonte número %i.",type_snip,j);
            disp(['Quantidade declarada: ' num2str(n_mics) '.']);
            disp(['Quantidade identificada: '...
                  num2str(sum(double(has_snip))) '.']);
            error("Verifique snips de identificação e suas " +...
                  "exclusividades, além das quantidades de posições " +...
                  "declaradas.");
            return;
        end
        
        k = 1;
        for i = 1 : size(has_snip,1)
            if(has_snip(i)) % se has_snip for verdadeiro (i.e. contém snip
                % de interesse, salva o índice em row_idx
                row_idx(k,j) = i;
                k = k + 1;
            end
            if(k > n_mics) % se a quantidade de n_mics já foi atingida, não
                % todas od índices de interesse já foram obtidos
                break;
            end
        end
    end
    disp("Snip de medição e quantidade de medições encontrados com sucesso.");
    % diminui 1 do índice, pois da parte numérica são removidos os
    % cabeçalhos
    row_idx = row_idx - 1;
%% índices de colunas

    % cria um snip pra analisar faixa de frequência de acordo com o
    % "formato BK"
    freq_snip = cell(1,2);
    for i = 1 : length(freq_range)
        if(freq_range(i) > 1000)
            freq_snip(i) = {['LZeq ' num2str(freq_range(i)/1000) 'kHz']};
        else
            freq_snip(i) = {['LZeq ' num2str(freq_range(i)) 'Hz']};
        end
    end
    % verifica quais colunas são os limites do range
    % has_freq_snip é um vetor lógico
    has_freq_snip = ~cellfun('isempty',strfind(txt(1,:), freq_snip(1))) | ...
                    ~cellfun('isempty',strfind(txt(1,:), freq_snip(2))) ;
    column_idx = zeros(1,2);
    k = 1;
    for i = 1 : length(has_freq_snip)
        if(has_freq_snip(i)) % se a coluna possui o snip de interesse,
            % salva o índice da coluna
            column_idx(k) = i;
            k = k + 1;
        end
        if(k > 2) % se já atingiu 2, então não precisa mais checar
            break
        end
    end
    disp("Limites de frequência encontrados com sucesso.");
    % diminui 1 do índice, pois da parte numérica são removidos os
    % cabeçalhos
    column_idx = column_idx - 1;
%% passar os valores numéricos para o vetor de saída
    output = zeros(n_mics,column_idx(2)-column_idx(1)+1,n_sources);
    for i = 1 : size(output,1)
        for k = 1 : size(output,3)
            output(i,:,k) = num(row_idx(i,k),column_idx(1) : column_idx(2));
        end
    end
    disp("Output gerado com sucesso.");
end