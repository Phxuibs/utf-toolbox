%% utf_txts_equal %%

% verificar se dois .txt são iguais

% Autor: Luis Henrique Sant'Ana

% input
% name1 = string com nome e path do primeiro arquivo (sem ".txt")
% name2 = string com nome e path do segundo arquivo (sem ".txt")

function [] = utf_txts_equal(name1,name2)

% acrescentar ".txt" aos nomes
name1 = name1 + ".txt";
name2 = name2 + ".txt";

% pegar dados do arquivo 1
fileID = fopen(name1);
file1 = fscanf(fileID,'%c');
fclose(fileID);

% pegar dados do arquivo 2
fileID = fopen(name2);
file2 = fscanf(fileID,'%c');
fclose(fileID);

% verificar igualdade
if(strcmp(file1,file2))
    disp("Arquivos são iguais");
else
    disp("Arquivos não são iguais");
end

end