%% utf_D_idx (v3) %%

% O objetivo desta função é indicar um índice adequado para o corte da RI
% para a geração da curva de Schroeder

% Autor: Luis Henrique Sant'Ana

% input:
% RI = resposta ao impulso (raw data) (já devem vir filtradas)
% t = vetor de tempo
% fs = taxa de amostragem
% D = faixa de análise (e.g. para T20, D = 20);
% (opcionais)
% freqRange = tuplet com limites inferior e superior da faixa de
% frequências de interesse (correspondente às bandas limites de terço de
% oitava utilizadas e presentes em 'RI');
% plotFlag = habilita criação de plots (boolean)
% safety = margem extra para detecção dos picos abaixo do desejado (dB)
% (escalar positivo)
% output:
% DIdx = instante de tempo que indica um decaimento -(D+15)dB (s)
% f = centros de terço de oitava correspondentes
function [DIdx,f] = utf_D_idx(RI,t,fs,D,varargin)
%% processamento inicial, utilizando objetos ITA
p = inputParser;
addRequired(p,'RI',@(x) isnumeric(x) && ismatrix(x));
addRequired(p,'t',@(x) isnumeric(x) && isvector(x));
addRequired(p,'fs',@(x) isnumeric(x) && isscalar(x));
addRequired(p,'D',@(x) isnumeric(x) && isscalar(x));
addOptional(p,'freqRange',[],@(x) isnumeric(x) && isvector(x) &&...
                           isequal(length(x),2));
addOptional(p,'plotFlag',false,@(x) islogical(x));
addParameter(p,'safety',5,@(x) isscalar(x) && (x>0))
parse(p,RI,t,fs,D,varargin{:});

% vetor de frequências
if(~isempty(p.Results.freqRange))
    f = utf_center_third_octave_bands(p.Results.freqRange);
else
    f = [];
end
% criar os arrays para armazenar os valores de picos
% como cada RI filtrada pode conter diferentes quantidades de picos,
% cria-se um cell array. "withOut" removal of outliers
% (peaksWithOut -> picos com outliers)
peaksWithOut = cell(length(f),1);
peaksTimeWithOut = cell(size(peaksWithOut));
% O resultado da rotina é um índice de corte para cada banda de frequência.
% É possível realizar a análise de EDC depois com o ITA, utilizando o
% índice de corte calculado. Assim:
% cada linha deve corresponder a uma filtragem de RI
if(size(RI,1)>size(RI,2))
    RI = RI.';
end
if(size(t,1)>size(t,2))
    t = t.';
end
%% processamento por frequência sem objetos ITA

for i = 1 : size(RI,1)
% amplitude instantânea
A(i,:) = abs(hilbert(RI(i,:)));

% índice de menor valor em dB e trends para cada frequência
[minIdx(i),trend(i,:),t_trend] = utf_min_value_dB(A(i,:)',t,fs);
% identificar valor máximo e seu índice separadamente, por precisão
[peak(i), peakIdx(i)] = max(A(i,:));
% "corte" da amplitude instantânea, pois na verdade transforma-se-os em NaN
% para facilitar plots, tanto antes do valor máximo quanto depois do
% valor mínimo
A(i, 1 : peakIdx(i)-1) = nan;
A(i, minIdx(i) : end) = nan;
% passar para dB
AdB(i,:) = 10*log10(A(i,:).^2/max(A(i,:).^2));

% identificar demais picos
% segurança para detectar os picos mais próximos de 30 para depois
% interpolar, então não limitar em 30, mas 30 + segurança
safety = p.Results.safety;
DIdx(i) = 0;
    while(DIdx(i) <= 0)
        % detectar picos
        [peaksWithOut{i}, peaksTimeWithOut{i}] = findpeaks(AdB(i,:),fs,...
                                 'MinPeakHeight',peak(i)-(D+15+safety),...
                                 'MinPeakDistance',0.001);
        % colocar o pico mais alto no começo de cada lista de picos
        % identificados (devido ao "corte", ele não é identificado)
        peaksWithOut{i} = [double(peak(i)) peaksWithOut{i}];
        peaksTimeWithOut{i} = [peakIdx(i)/fs peaksTimeWithOut{i}];
        % identificar picos outliers a partir do sua distância demasiada e
        % removê-los
        peaks{i} =         peaksWithOut{i}(~isoutlier(peaksTimeWithOut{i},'quartiles'));
        peaksTime{i} = peaksTimeWithOut{i}(~isoutlier(peaksTimeWithOut{i},'quartiles'));
        % analisando da direita para a esquerda, remover qualquer pico que
        % seja menor do que um pico anterior
        for j = length(peaks{i})-1 : -1 : 1
            % verifica se é menor:
            if( sum(peaks{i}(j) < peaks{i}(j+1:end)) >= 1 )
                peaks{i} = [peaks{i}(1:j-1) peaks{i}(j+1:end)];
                peaksTime{i} = [peaksTime{i}(1:j-1) peaksTime{i}(j+1:end)];
            end
        end
        % em casos muito raros (mas já aconteceu, ver RI
        % H_r_25_R_395_woes_T4_P6.ita, canal 2), pode ocorrer de picos
        % iguais ficarem lado a lado. Apesar de monotonicidade aceitar
        % pontos vizinhos e iguais, a função pchip do matlab não aceita -
        % todos devem ser únicos.
        % A alternativa realizada é adicionar erros de ponto flutuante à
        % esquerda (foram necessários 5 erros - 5 * eps) idealmente, essa
        % função seria executada novamente, pois o ajuste poderia causar o
        % pico à esqeurda ficar semelhante ao da sua esquerda - mas isso
        % não é feita porque a chance é muita remota haha
        for j = 1 : length(peaks{i})-1
              if(peaks{i}(j) == peaks{i}(j+1))
                  peaks{i}(j) = peaks{i}(j) + 5*eps("like",peaks{i}(j));
              end
        end        
        % realizar interpolação cúbica de hermite monotônica e identificar
        % índices correspondentes ao decaimento desejado
        % input: valor em dB; output: tempo
        pp{i} = pchip(peaks{i},peaksTime{i});
        DIdx(i) = ppval(pp{i},-D-15);
        if(DIdx(i) <= 0)
            safety = safety + 5;
            if(safety > 20)
                error("Oh, Lord! I'm having trouble finding the peaks!");
            end
        end
    end
end
%% plots no tempo
if(p.Results.plotFlag)
safety = p.Results.safety; % reset safety, if it was altered
for i = 1 : size(RI,1)
    fig = figure();
    hold on
    % RI em dB
    P(1) = plot(t,10*log10(RI(i,:).^2/max(RI(i,:).^2)),'color',utf_color("azul"));
    % amplitude instantânea
    P(2) = plot(t,AdB(i,:),'linewidth',1.75,'color',utf_color("amarelo_queimado"));
    % média dos valores em dB da amplitude instantânea (trend)
    P(3) = plot(t_trend,trend(i,:),':','linewidth',3,'color',utf_color("framboesa"));
    % índice do menor valor em dB
    xline(minIdx(i)/fs,'--','linewidth',2,'color',utf_color("framboesa"));
    % plot da interpolante
    P(4) = plot(ppval(pp{i},peaks{i}(1):-0.01:peaks{i}(end)),...
                      peaks{i}(1):-0.01:peaks{i}(end),...
                      'linewidth',2,'color',utf_color("indigo"));
    % índice correspondente a -(D+15)
    xline(DIdx(i),'-.','linewidth',2,'color',utf_color("indigo"));
    % -(D+15)
    yline(-(D+15),'-.','linewidth',2,'color',utf_color("indigo"));
    % picos detectados, sem qualquer filtro
    P(5) = plot(peaksTimeWithOut{i},peaksWithOut{i},'^','color',...
           utf_color("verde_escuro"),'MarkerSize',8,'MarkerFaceColor',...
           utf_color("verde_escuro"));
    % picos filtrados
    P(6) = plot(peaksTime{i},peaks{i},'o','linewidth',2,'color',...
           utf_color("indigo"),'MarkerSize',12);
    % plot do maior pico; pode parecer errado no tempo, mas o índice do
    % vetor está certo
    plot(peakIdx(i)/fs,peak(i),'^','color',[1 0 0],...
         'MarkerSize',8,'MarkerFaceColor',[1 0 0]);
    hold off
    ax = gca;
    ax = utf_axes(ax,22);
    ax.XLim = [0 peaksTimeWithOut{i}(end)+0.1];
    ax.YLim = [-(D+15+safety+5) 0];
    ax.YTick = ax.YLim(1) : 5 : 0;
    if(~isempty(f))
        title("Banda 1/3 oitava: "+f(i)+" Hz");
    end
    ylabel("Amplitude (dB)");
    xlabel("Tempo (s)");
    ax = utf_minor_grid(ax);
    ax.Layer = 'top';
    ax.TickDir = 'out';
%     ax.Box = 'on';
    legend([P(1:3) P(5) P(6) P(4)],"RI","$A_\mathrm{inst}$","\textit{trend}",...
           "Picos n\~{a}o filtrados","Picos filtrados","Interpolante",...
           'location','northeast','interpreter','latex');
end
end
end