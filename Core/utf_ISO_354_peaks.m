%% utf_ISO_354_peaks %%

% Checks if values in a vector are not peaks or dips above or below 15% of
% the mean values of the values around it.

% input:
% x: numerical vector.
% output:
% y: vector which contains the same correspondent value of x if it respects
% the 15% divergence limit. Otherwise, contains NaN.

% Author: Luis H. Sant'Ana

function [y] = utf_ISO_354_peaks(x)
%% parser
p = inputParser;
p.addRequired("x",@(x) length(x)>=3 && isnumeric(x) && isvector(x));
p.parse(x);
%% check for peaks or dips
y = nan([1 length(x)]);
for i = 1 : length(x)
    % calculate the average value
    switch i
        case 1
            avg = x(2);
        case length(x)
            avg = x(end-1);
        otherwise
            avg = ( x(i-1) + x(i+1) ) / 2;
    end
    % check limits
    if( x(i)<=avg*1.15 && x(i)>=avg*0.85 )
        y(i) = x(i);
    else
        y(i) = nan;
    end
end
end