%% utf_air_density
%
% Calcula densidade do ar a partir de pressão e temperatura de acordo com a
% norma ASTM E2611 [1].
%
%
% input (todos parâmetros):
% T: temperatura (°C) (array numérico ou (exclusivamente) escalar)
% P: pressão (kPa) (array numérico ou (exclusivamente) escalar)
%
%
% output:
% rho: densidade do ar (kg/m^3) (array numérico)
%
%
% Autor: Luis Henrique Sant'Ana
%
% [1] ASTM E2611-09. Standard Test Method for Normal Incidence
% Determination of Porous Material Acoustical Properties Based on the
% Transfer Matrix Method. 2009.


function rho = utf_air_density(varargin)
%% parseamento
defaultT = 20;
defaultP = 90.8; % considerando 914 m de altitude

p = inputParser;
addParameter(p,'T',defaultT,@(x) isnumeric(x));
addParameter(p,'P',defaultP,@(x) isnumeric(x));
parse(p,varargin{:});
T = p.Results.T;
P = p.Results.P;
if(~isequal(size(T),size(P)) && ~xor(isscalar(T),isscalar(P)))
    error("Dimensões de arrays de pressão e temperatura devem ser "+...
          "semelhantes ou um dos dois deve ser escalar.");
end
%% Air density

rho = 1.290*(P/101.325).*(273.15./(273.15+T));
% de acordo com a Wikipedia (https://en.wikipedia.org/wiki/Density_of_air)
% a constante à frente, mais precisa, é 1.2922; deste modo, os valores
% conferem com os demais apresentados na tabela da Wiki, com os valores de
% referência ainda sendo 101.325 kPa (para pressão a nível do mar médio) e
% temperatura de 0 °C
end