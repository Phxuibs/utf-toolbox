%% utf_get_ITA_object %%
%
% Retorna objeto ITA (proveniente de arquivo ".ita") a partir de pasta ou
% arquivo selecionado
%
% input (name-value pairs):
% filename: nome do arquivo ou snip do nome do arquivo, sem o ".ita"
% (buscará por ou "filename*.ita") (string); default = "".
% folder: string ou char array com caminho para a pasta; default = "".
% merge: se múltiplos objetos ITA devem ser fundidos em um único objeto ITA
% com múltiplos canais (boolean); default = false;
%
% output:
% ITA = objeto ITA ou array de objetos ITA
%
% Author: Luis Henrique Sant'Ana
%
function [ITA,filename] = utf_get_ITA_object(varargin)
%% Parseamento
defaultFilename = "";
defaultPath = "";
defaultMerge = false;
p = inputParser;
p.addParameter("filename",defaultFilename,@(x) isStringScalar(x));
p.addParameter("path",defaultPath,@(x) isStringScalar(x));
p.addParameter("merge",defaultMerge,@(x) islogical(x) && isscalar(x));
parse(p,varargin{:});
filename = p.Results.filename;
path = p.Results.path;
merge = p.Results.merge;
%% Pasta e nomes de arquivos
% Verifica possíveis arquivos para o snip
arquivosITA = dir(path + filename + "*.ita");
% Formata os nomes dos arquivos encontrados em células
files = cell(size(arquivosITA,1),1);
for i = 1 : length(files)
    files(i) = {[char(path) arquivosITA(i).name]};
end
%% Extrai o(s) objeto(s) ITA
ITA = ita_read(files);
%% Funde em um único objeto ITA
if(merge)
    ITA = ita_merge(ITA);
end
end