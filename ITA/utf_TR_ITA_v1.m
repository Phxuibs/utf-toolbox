%% utf_TR_ITA %%

% O objetivo é calcular TR para todos os arquivos .ita em um diretório.
% Muitas melhorias ainda podem ser feitas na rotina!

% Autor: Luis Henrique Sant'Ana

% input:
% folder = nome da pasta (string)
% freqRange = tuplet com valores centrais de bandas limites de interesse
% output:
% TR = tempos de reverberação (linhas) por frequências (colunas) para cada
% arquivo (os arquivos são lidos em ordem alfabética)

function [TR] = utf_TR_ITA(folder,freqRange)
%% obter os nomes de todos os arquivos .ita partir da pasta
folder = folder + "/";
arquivosITA = dir(folder + "*.ita");
filename = cell(size(arquivosITA,1),1);
for i = 1 : length(filename)
    filename(i) = {[char(folder) arquivosITA(i).name]};
end
%% Pegar RIs, filtrar, cortar, calcular TR
TR = zeros([numel(filename) ...
            numel(utf_center_third_octave_bands(freqRange))]);
for i = 1 : size(TR,1)
% Pegar RI
RI = utf_get_RI_ITA(filename(i));
% Lembra-se que é melhor filtrar antes de cortar as RIs, pois o corte
% abrupto pode causar artefatos na frequência
RI_filtered = ita_fractional_octavebands(RI,'freqRange',freqRange);
% O corte deve ser simples.
% Pode-se também usar zeros no final, pois o ITA detecta e remove os zeros.
% A vantagem de usar os zeros é que o comprimento do sinal é preservado,
% facilitando uso em arrays.
% Aqui será utilizada uma função própria do ITA, pois não deseja-se
% empilhar vetores num array e porque aparecem mensagem devido ao grande
% número de zeros avisando que atrapalha na detecção de ruído e que é pra
% usar essa função (ver abaixo).
% Da forma como é utilziada, parece que apenas pega metade das amostras,
% que deve ser suficiente para remover as não linearidades (era a mesma
% quantia que estava sendo removida manualmente).
% (ita_extract_dat(.) em ITA/kernel/DSP/edit)
RI_filt_zeros = ita_extract_dat(RI_filtered);
% TR pelo método do Lundeby (método E no artigo do Guski)
T_E = ita_roomacoustics(RI_filt_zeros,'broadbandanalysis',true,...
                        'edcMethod','subtractNoiseAndCutWithCorrection',...
                        'T15');
% TR para cada frequência
TR(i,:) = T_E.T15.freq;
end
end