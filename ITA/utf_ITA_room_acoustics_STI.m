%% utf_ITA_room_acoustics_STI %%
%
% Calculate parameters bunch of parameters from ITA room acoustics function
% (EDT, LDT, T10, T15, T20, T30, C80, D50, Center_time, PSNR_Lundeby) and
% STI (both for female and male speakers) from an ITA object.
% Also, saves it all on an spreadsheet by default.
%
% Hint: use ita_merge(ITA_object) to put an array of, or multiple ITA
% objects, into a single one with multiple channels!
%
% input:
% ITA: ITA object.
% (name-value pairs)
% freqRange: frequency range (numeric tuplet)
% method: check ita_roomscoustics for available methods. Default:
% 'subtractNoiseAndCutWithCorrection'.
% saveSpreadsheet: if a spreadsheet is to be saved (boolean)
% path: path to save the spreadsheet (string)
% filename: name for the spreadsheet (string)
%
% output:
% P: parameters object, together with frequency vector
%
% Author: Luis H. Sant'Ana
%
function [P] = utf_ITA_room_acoustics_STI(ITA,varargin)
%% Setup
defaultFreqRange = [500 10000];
defaultMethod = "subtractNoiseAndCutWithCorrection";
defaultSaveSpreadsheet = true;
defaultPath = "";
defaultFilename = "ITA";
p = inputParser;
p.addParameter("freqRange",defaultFreqRange,@(x) isvector(x) && isequal(length(x),2) && isnumeric(x));
p.addParameter("method",defaultMethod,@(x) isStringScalar(x));
p.addParameter("saveSpreadsheet",defaultSaveSpreadsheet,@(x) isboolean(x));
p.addParameter("path",defaultPath,@(x) isStringScalar(x));
p.addParameter("filename",defaultFilename,@(x) isStringScalar(x));
parse(p,varargin{:});
f = ita_ANSI_center_frequencies(p.Results.freqRange,3).'; % frequency vector
%% Calculate Acoustic Parameters
% Parameters using ita_roomacoustics
P = ita_roomacoustics(ITA,'EDT','LDT','T10','T15','T20','T30',...
                          'C80','D50','Center_Time','PSNR_Lundeby',...
                      'freqRange',p.Results.freqRange,...
                      'method',p.Results.method);
% STI (must process a single channel at a time)
for j = 1 : ITA.nChannels
    P.STI_female(j) = ita_speech_transmission_index(ita_split(ITA,j),...
                                                    'gender','female');
    P.STI_male(j)   = ita_speech_transmission_index(ita_split(ITA,j),....
                                                    'gender','male');
end
% Adds frequency vector to the parameters object
P.f = f;
%% Save Parameters to a Spreadsheet
if(p.Results.saveSpreadsheet)
    parameters = ["EDT","LDT","T10","T15","T20","T30",...
                  "C80","D50","Center_Time","PSNR_Lundeby"];
    for k = 1 : length(parameters)
        eval("writematrix([f P." + parameters(k) + ".freq],'" + ...
             p.Results.path + p.Results.filename + ".xls'," + ...
             "'Sheet','" + parameters(k) + "','WriteMode','overwritesheet');");
    end
% STI_female
writematrix(P.STI_female,p.Results.path + p.Results.filename + ".xls", ...
            'Sheet','STI_female','WriteMode','overwritesheet');
% STI_male
writematrix(  P.STI_male,p.Results.path + p.Results.filename + ".xls", ...
              'Sheet','STI_male','WriteMode','overwritesheet');
end
end