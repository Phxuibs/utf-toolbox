%% utf_TR_ITA (v3) %%
%
% Calcula tempo de reverberação de respostas ao impulso em formato .ita em
% diretório selecionado
%
% input (all are name-value pairs)
% 'path': caminho até a pasta com os arquivos .ita (string)
% 'snip': pedaço do nome do arquivo que identifica-o como desejado (string)
% 'freqRange': tuplet com as bandas limites desejadas
% 'D': faixa de análise do tempo de reverberação (e.g. D = 15 para T15)
% (escalar)
% 'idxFlag': se a RI deve ser filtrada e cortada a -(D+15) dB, para cada
% frequência, antes de calcular T_60
% 'idxPlotFlag': se devem ser plotadas as curvas com a detecção do índice D
%
% output:
% T: tempos de reverberação
%
% obs. 1: cortar cria artefatos no domínio da frequência, podendo afetar a
% filtragem. Porém, como testado, o efeito no tempo de reverberação é
% desconsiderável. Logo, corta-se antes porque filtragem é o processo mais
% demorado, sendo mais rápido com um sinal menor.
% obs. 2: o corte padrão é realizado na metade da RI. A função
% ita_extract_dat(.) (em ITA/kernel/DSP/edit) faz, aparentemente, a mesma
% coisa e é recomendada sua utilização para esse tipo de operação.
%
% Autor: Luis Henrique Sant'Ana

function T = utf_TR_ITA(varargin)
%% parse
defaultPath = "";
defaultSnip = "";
defaultFreqRange = [500 16000];
defaultD = 15;
defaultIdxFlag = false;
defaultIdxPlotFlag = false;

p = inputParser;
addParameter(p,'path',defaultPath,@(s) isstring(s));
addParameter(p,'snip',defaultSnip,@(s) isstring(s));
addParameter(p,'freqRange',defaultFreqRange,@(x)isvector(x)&&numel(x)==2);
addParameter(p,'D',defaultD,@(x) isscalar(x) && isnumeric(x));
addParameter(p,'idxFlag',defaultIdxFlag,@(x) islogical(x));
addParameter(p,'idxPlotFlag',defaultIdxPlotFlag,@(x) islogical(x));
parse(p,varargin{:});

path = p.Results.path;
snip = p.Results.snip;
freqRange = p.Results.freqRange;
D = p.Results.D;
idxFlag = p.Results.idxFlag;
idxPlotFlag = p.Results.idxPlotFlag;
%% TR
% nomes dos arquivos
files = dir(path+"*"+snip+"*.ita");
% ler medições (arquivos .ita)
for i = 1 : length(files)
    RI(i) = ita_read(path+files(i).name);
end
% junta todas em uma;
RI = ita_merge(RI);

% corte na metade da RI
RI_crop = ita_extract_dat(RI);

if(~idxFlag)
    % corte na metade da RI
%     RI_crop = ita_extract_dat(RI);
    % TR
    TR = ita_roomacoustics(RI_crop,['T' num2str(D)],'freqRange',...
            freqRange,'edcMethod','subtractNoiseAndCutWithCorrection');
    % pega os valores de TR
    eval("T = TR.T"+string(num2str(D))+".freq.';");
else
    % vetor de tempo
    timeVector = RI_crop.timeVector;
    % taxa de amostragem
    fs = RI_crop.samplingRate;
    % por RI
    for i = 1 : size(RI.time,2)
        % filtra
        RI_filt = ita_fractional_octavebands(ita_split(RI_crop,i),'freqRange',freqRange);
        % bandas
        f = utf_center_third_octave_bands(freqRange);
        % por banda
        for j = 1 : size(RI_filt.time,2)
            % identifica instante de tempo para corte
            DIdx = utf_D_idx(RI_filt.time(:,j),timeVector,fs,D,...
                             [f(j) f(j)],idxPlotFlag);
            % corta
            RI_crop_DIdx = ita_time_crop(ita_split(RI_filt,j),[0 DIdx],'time');
            % calcula T_D
            TR = ita_roomacoustics(RI_crop_DIdx,['T' num2str(D)],...
                    'broadbandanalysis',true,'edcMethod','noCut')';
            eval("T(i,j) = TR.T"+string(num2str(D))+".freq;");
        end
    end
end