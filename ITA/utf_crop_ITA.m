%% utf_crop_ITA %%

% retorna RI ".ita" cortada

% Autor: Luis Henrique Sant'Ana

% input:
% tcrop = duplet com limites, em segundos, para corte do sinal; se o
% primeiro termo for 'nan', o sinal é cortado a partir do maior valor
% absoluto até o maior valor absoluto + o tempo no segundo termo do duplet

% output:
% RI = RI .ita cortada

function [RI_crop] = utf_crop_ITA(RI,tcrop)
    %% corte de RI para parte de interesse
    if(isnan(tcrop(1)))
        for i = 1 : length(RI)
            % determinar primeiro valor de tcrop a partir do valor máximo
            [~,ix] = max(abs(RI(i).time));
            tcrop(1) = ix/RI(i).samplingRate;
            % determinar o segundo valor de tcrop
            tcrop = [tcrop(1) tcrop(1)+tcrop(2)];
            % time crop
            RI_crop(i) = ita_time_crop(RI(i),tcrop);
        end
    else
        for i = 1 : length(RI)
            % corta conforme valores passados
            % time crop
            RI_crop(i) = ita_time_crop(RI(i),tcrop);
        end
    end
end