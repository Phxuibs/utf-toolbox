%% utf_NPS_ITA %%

% cálculo de NPS a partir de RI '.ita', em pasta selecionada ou de único
% arquivo '.ita'

% Autor: Luis Henrique Sant'Ana

% input:
% file = string ou char array correspondente ao nome do arquivo ou snip do
% nome do arquivo (e.g. "filename.ita" ou "*file*");
% tcrop = tuplet para corte do sinal (ver utf_crop_ITA() para detalhes).
% freq_range = tuplet com os limites da faixa de frequência de interesse

% output:
% NPS = array com NPS de cada medição; frequência disposta ao longo das
% colunas;

function [NPS] = utf_NPS_ITA(file,tcrop,freq_range)
    % sensibilidade do microfone (mV/Pa)
    mic = 40e-3;
    % pressão de referência (Pa)
    p_ref = 20 * 10^-6;
    %% aquisição de RI(s) e corte
    RI_crop = utf_crop_ITA(file,tcrop);
    %% filtragem de RI
    % (passar os centros das bandas resulta em análise da banda por inteiro,
    % não apenas a partir daquela frequência; testes foram realizados).
    for i = 1 : length(RI_crop)
        RI_filtered(i) = ita_fractional_octavebands(RI_crop(i),...
                         'bandsperoctave',3,'freqRange',freq_range);
    end
    %% cálculo de energia a partir das RI filtradas
    RI_energy = nan(length(RI_filtered),RI_filtered(1).dimensions);
    for i = 1 : length(RI_filtered)
        for j = 1 : RI_filtered(1).dimensions
%             RI_energy(i,j) = sum(abs(RI_filtered(i).freq(:,j)).^2);
              RI_energy(i,j) = sum((abs(RI_filtered(i).freq(:,j))/mic).^2) ./ ...
                               length(RI_filtered(i).freq(:,j));
        end
    end
    %% cálculo de NPS
%     NPS = 10 * log10( RI_energy );
    NPS = 10 * log10( RI_energy / p_ref^2 );
end