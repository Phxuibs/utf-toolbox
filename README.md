# UTF TOOLBOX

Welcome to the UTF-Toolbox!

It's basically an upload of scripts I've developed in the last years for research in acoustics. If it's not your thing, you can use the scripts in the "Aesthetics" folder to easily generate more beautifil plots in MATLAB!

## Requirements

Some files, specially those contained in the "ITA" folder, require the [ITA-Toolbox](https://git.rwth-aachen.de/ita/toolbox.git).

## Roadmap

The toolbox is currently being improved in what concerns the crossover frequency statistical estimation ("fcr" folder) and the symmetry function ("symmetry coefficient" folder), and will be revised in their entirety.
Also, the entire Toolbox is under revision in what concerns organization and readability (although translating everything to english, for example, is not a concern)!

## Contact

Wanna talk? You can e-mail me at luis.2014@alunos.utfpr.edu.br. =)

## Licensing

See the LICENSE.txt file. Also, it is called the UTF-Toolbox because it was developed by researchers at Universidade Tecnológica Federal do Paraná (UTFPR); however, this project was never formally presented to the institution and therefore is not sponsored or endorsed by it in any way.

## Acknowledgements

Thanks go to José Henrique Larcher for his help in codes and also setting up this repository.
Also, some codes under the "Parametric Loudspeakers" folder were developed by researchers at La Salle Campus Barcelona - Universidad Ramon Llull, specially by Prof. Dr. Marc Arnela.
