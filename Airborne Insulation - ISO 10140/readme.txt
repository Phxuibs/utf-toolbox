Rotina para análise de isolamento acústico aéreo em laboratório, utilizando posições fixas de microfone e diferentes posições de fonte sonora, como descrito pela norma ISO 10140, a partir de valores de NPS e medições de TR a partir da ITA toolbox.

Caveats!!!
Não é feito ajuste caso a diferença de NPS entre as medições na sala receptora e de ruído de fundo sejam abaixo de 6dB, mas a ocorrência pode ser visualizda no gráfico final.
Não é calculado graus de liberdade e repetitividade; é calculado desvio padrão.
Não são consideradas incertezas no cáclulo da "constante k", ou seja, de valores como temperatura e dimensões da sala.

Autores: Luis H. Sant'Ana, Rodrigo Scoczynski Ribeiro
Universidade Tecnológica Federal do Paraná - UTFPR, Curitiba, maio de 2020.

Considerações quanto às medições de TR e ITA toolbox:
As funções de transferência obtidas a partir do ITA toolbox devem estar
no seu formato padrão '.ita';
ITA toolbox deve estar instalado!