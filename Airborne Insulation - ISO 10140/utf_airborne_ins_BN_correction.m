%% utf_airborne_ins_BN_correction %%

% realiza ajuste caso a diferença de NPS esteja entre 6dB e 15dB

% Autor: Luis Henrique Sant'Ana

% input:
% R_p = array com valores de pressão da sala receptora;
% BN_p = array com valores de pressão de ruído de fundo;
% t6_15 = índices lógicos que indicam os valores que necessitam de correção

% output:
% Rc_NPS = valores de NPS da receptora corrigidos
% Rc_NPS_u = incerteza padrão de RC_NPS

function [Rc_NPS_mean,Rc_NPS_u] = utf_airborne_ins_BN_correction(R_p,BN_p,...
    t6_15)

    % ajusta o tamanho do array de BN e calcula a média e o desvio padrão
    BN_p = repmat((BN_p),1,1,size(R_p,3));
    BN_p_mean = mean(BN_p);
    BN_p_std = std(BN_p);
    % o valor corrigido, inicialmente, tem os mesmos valores que os medidos
    % na sala receptora
    Rc_mean = mean(R_p);
    Rc_u = std(R_p);
    % se necessita de correção, subtrai o valor médio de BN e calcula a
    % incerteza combinada
    Rc_mean(t6_15) = Rc_mean(t6_15) - BN_p_mean(t6_15);
    Rc_u(t6_15) = sqrt( ( Rc_u(t6_15) / sqrt( size(R_p,1) ) ).^2 +...
                        ( BN_p_std(t6_15) / sqrt( size(BN_p,1) ) ).^2 );
    % passar para dB
    Rc_NPS_mean = 10 * log10(Rc_mean);
    Rc_NPS_u = sqrt( ( 10./(log(10)*Rc_mean) ).^2 .* Rc_u.^2 );
end
