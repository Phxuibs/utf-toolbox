%% utf_airborne_ins_plot %%

% plot dos resultados de medição de isolamento aéreo. Salva o plot em .pdf.

% Autor: Luis Henrique Sant'Ana

% input:
% plot_setup = struct que deve conter um objeto string 'filename' para nome
% do arquivo, tuplet 'plot_size' com dimensões para corte do plot e string 
% 'title' para título do plot;
% freq_range = limites da faixa de frequência de interesse
% SRI (Sound Reduction Index) = struct com valores base e incerteza padrão
% ('dB' e 'dB_u');
% TR = valores médios de TR;
% NPS = struct com objetos que identificam o "delta NPS" de cada
% medição ("delta NPS" é declarado aqui como a diferença entre NPS medido e
% de ruído de fundo para cada frequência, de modo geral para cada posição
% de fonte); isto é, deve ter um objeto '.t6' e outro '.t6_15';
% V = volume da sala receptora [m^3]

function [] = utf_airborne_ins_plot(plot_setup,freq_range,SRI,TR,NPS,V)
    %% verifica o struct plot_setup
    if(~isempty(plot_setup))
        % verifica o título do plot
        if(~isstring(plot_setup.title) && ~ischar(plot_setup.title))
                error("plot_setup.title deve ser char ou string.");
        end
        % verifica o nome pro arquivo
        if(~isstring(plot_setup.filename) && ~ischar(plot_setup.filename))
                error("plot_setup.filename deve ser char ou string.");
        end
        % verifica o tamanho de corte
        if(~isnumeric(plot_setup.plot_size))
            error("plot_setup.plot_size deve ser numérico.");
        else
            if(~isequal(size(plot_setup.plot_size),[1 2]) && ...
               ~isequal(size(plot_setup.plot_size),[2 1]))
                error("plot_setup.plot_size deve ser um duplet " +...
                      "(e.g. [num1 num2]).");
            end
        end
    end
    %% pegar vetor de fequências
    freq_vector = utf_center_third_octave_bands(freq_range);
    %%
    h = figure();
    % plot base
    plot(freq_vector,SRI.dB,'Color',utf_color('azul_claro'),...
        'LineWidth',1.5);
    hold on;

    % verificar o "delta NPS"
    t6 = false(1,length(freq_vector));
    t6_15 = false(1,length(freq_vector));
    for k = 1 : size(NPS.t6,3)
        t6(1,:) = t6 | ~NPS.t6(:,:,k);
        t6_15(1,:) = t6_15 | NPS.t6_15(:,:,k);
    end

    % plots específicos dependendo do valor de TR e "delta NPS"
    TR_max = 2 * (V/50)^(2/3); % ISO 10140-5 2010, página 3, para
    % frequências a partir de 100Hz
    for j = 1 : length(freq_vector)
        if ((TR(j) <= TR_max) && (TR(j) >= 1))
            if(t6(j))
                errorbar(freq_vector(j),SRI.dB(j),SRI.dB_u(j),...
                    '-^','Color',utf_color('roxo'),'LineWidth',2.0,...
                    'MarkerFaceColor',utf_color('roxo'),'MarkerSize',8);
                    hold on;
            else
                if(t6_15(j))
                    errorbar(freq_vector(j),SRI.dB(j),SRI.dB_u(j),...
                        '-^','Color',utf_color('azul'),'LineWidth',2.0,...
                        'MarkerFaceColor',utf_color('azul'),...
                        'MarkerSize',8); hold on;
                else
                    errorbar(freq_vector(j),SRI.dB(j),SRI.dB_u(j),...
                        '-^','Color',utf_color('verde'),'LineWidth',2.0,...
                        'MarkerFaceColor',utf_color('verde'),...
                        'MarkerSize',8); hold on;
                end
            end
        else
            if(t6(j))
                errorbar(freq_vector(j),SRI.dB(j),SRI.dB_u(j),...
                    '-o','Color',[0 0 0],'LineWidth',2.0,...
                    'MarkerFaceColor', [0 0 0],'MarkerSize',8); hold on;
            else
                if(t6_15(j))
                    errorbar(freq_vector(j),SRI.dB(j),SRI.dB_u(j),...
                        '-o','Color',utf_color('cinza'),'LineWidth',2.0,...
                        'MarkerFaceColor',utf_color('cinza'),...
                        'MarkerSize',8); hold on;
                else
                    errorbar(freq_vector(j),SRI.dB(j),SRI.dB_u(j),...
                        '-o','Color',utf_color('vermelho'),...
                        'LineWidth',2.0,'MarkerFaceColor',...
                        utf_color('vermelho'),'MarkerSize',8); hold on;
                end
            end
        end
    end
    %% configurar plot
    ax = gca;
    ax.FontSize = 24;
    ax.FontWeight = 'normal';
    ax.XScale = 'log';
    ax.XTick = freq_vector;
    ax.XTickLabelRotation = 45;
    ax.XLim = [freq_vector(1) freq_vector(end)];
    % ax.YLim = [10 50];
    grid on;
    ylabel('Índice de Redução Sonora [dB]');
    xlabel('Bandas de 1/3 oitava [Hz]');
    title(plot_setup.title);
    %% legenda
    p(1) = plot(-1,SRI.dB(j),'o','Color',[0 0 0],...
        'MarkerFaceColor',[0 0 0],'MarkerSize',8); hold on;
    p(2) = plot(-1,SRI.dB(j),'o','Color',utf_color('cinza'),...
        'MarkerFaceColor',utf_color('cinza'),'MarkerSize',8); hold on;
    p(3) = plot(-1,SRI.dB(j),'o','Color',utf_color('vermelho'),...
        'MarkerFaceColor',utf_color('vermelho'),'MarkerSize',8); hold on;
    p(4) = plot(-1,SRI.dB(j),'^','Color',utf_color('roxo'),...
        'MarkerFaceColor',utf_color('roxo'),'MarkerSize',8); hold on;
    p(5) = plot(-1,SRI.dB(j),'^','Color',utf_color('azul'),...
        'MarkerFaceColor',utf_color('azul'),'MarkerSize',8); hold on;
    p(6) = plot(-1,SRI.dB(j),'^','Color',utf_color('verde'),...
        'MarkerFaceColor',utf_color('verde'),'MarkerSize',8); hold off;
    legend(p(:),{...
        '$ (TR < 1s\ \lor\ TR > TR_{max})\ \land\ \Delta NPS \leq 6dB $',...
        '$ (TR < 1s\ \lor\ TR > TR_{max})\ \land\ 6dB < \Delta NPS < 15dB $',...
        '$ (TR < 1s\ \lor\ TR > TR_{max})\ \land\ \Delta NPS \geq 15dB $',...
        '$ 1s \leq TR \leq TR_{max}\ \land\ \Delta NPS \leq 6dB $',...
        '$ 1s \leq TR \leq TR_{max}\ \land\ 6dB < \Delta NPS < 15dB $',...
        '$ 1s \leq TR \leq TR_{max}\ \land\ \Delta NPS \geq 15dB $'},...
        'Location','Northwest','Interpreter','latex','FontSize',20);
    legend('boxoff');
    %% salvar
    % força filename para string
    if(ischar(plot_setup.filename))
        plot_setup.filename = string(plot_setup.filename);
    end
    % adiciona prefixo
%     plot_setup.filename = "[resultado] - " + plot_setup.filename;
    % ajusta tamanho e salva em pdf
    utf_adjust_plot_fine(h,plot_setup.filename,plot_setup.plot_size);
end