%% utf_airborne_ins_main %%

% Rotina para análise de isolamento acústico aéreo em laboratório,
% como descrito pela norma ISO 10140, a partir de dados de NPS e TR

% Autores: Luis H. Sant'Ana e Rodrigo Scoczynski Ribeiro

% input
% BN = struct com objeto 'NPS' com dados de NPS da medição de ruído de
% fundo;
% E = struct com objeto 'NPS' com dados de NPS da medição da sala emissora;
% R = struct com objeto 'NPS' com dados de NPS da medição da sala
% receptora;
% TR = struct com objeto '.mean' com valores médios de tempos de
% reverberação ao longo da frequência e objeto '.std' com desvio
% padrão dos tempos de reverberação
% k = constante k;

% obs.:
% os arrays E.NPS e R.NPS devem possuir o mesmo tamanho (size());
% a quantidade de colunas também será verificada de acordo com a quantidade
% de centros de bandas de terço de oitava entre os limites especificados
% por freq_range;
% espera-se o formato: diferentes posições de microfones dispostas em
% linhas, frequências em colunas, posições de fonte em níveis;

% output:
% Rma = SRI pelo método da melhor aproximação;
% contém objetos '.p' e '.p_u' (média e incerteza padrão na pressão) e
% '.dB' e '.dB_u' (valor base do SRI e sua incerteza padrão);
% Riso = SRI de acordo com a norma;
% contém objetos '.dB' e '.dB_u' (valor base do SRI e sua incerteza
% padrão); seus valores de pressão são equivalentes aos de 'RR.p' e
% 'RR.p_u';
% NPS = struct com objeto '.difference' que corresponde à diferença entre
% os níveis da sala receptora e ruído de fundo, além de índices lógicos
% para identificar diferenças maiores do que 6dB, 15dB e entre 6dB e 15dB;
% BN = struct de medições de ruído de fundo;
% E = struct de medições da sala emissora;
% R = struct de medições da sala receptora;
% possuem objetos '.p', '.NPS_mean' e '.NPS_u', correpondentes aos valores
% na pressão, a média de NPS e a incerteza padrão de NPS.
% RR = diferença entre o nível da sala emissora e da sala receptora (com
% correção de ruído de fundo);
% possui objetos '.dB' e '.dB_u', correpondentes ao valor base da
% diferença e da incerteza padrão;
% Rc = valores da sala receptora com correção de ruído de fundo;
% contém objetos '.NPS_mean' e '.NPS_u', correpondentes ao valor base da
% correção e da incerteza padrão;

function [Rma,Riso,NPS,BN,E,R,RR,Rc] = utf_airborne_ins_main(BN,E,R,TR,k)
%% validação dos parâmetros
utf_airborne_ins_validation(E,R,TR.mean);
%% pressão, média e desvio padrão da média
% calcula o valor em pressão, a média e desvio padrão da média para cada
% posição de fonte sonora
[BN.p,BN.NPS_mean,BN.NPS_u] = utf_NPS_mean(BN.NPS);
[E.p,E.NPS_mean,E.NPS_u] = utf_NPS_mean(E.NPS);
[R.p,R.NPS_mean,R.NPS_u] = utf_NPS_mean(R.NPS);
%% diferença em relação ao ruído de fundo e correção
% calcula a diferença de NPS na sala receptora com o ruído de fundo
% (tamanho do array BN ajustado)
NPS.difference = R.NPS_mean - repmat(BN.NPS_mean,1,1,size(R.NPS_mean,3));
% cria um array para armazenar os valores lógicos de checagem do
% threshold
NPS.t15 = false(size(NPS.difference));
NPS.t6 = false(size(NPS.difference));
% verifica se a diferença é maior ou igual a 15dB e maior do que 6dB (ISO
% 10140-4 2010, página 04)
NPS.t15(NPS.difference >= 15) = true;
NPS.t6(NPS.difference > 6) = true;
% verifica diferenças entre 6dB e 15 dB
NPS.t6_15 = NPS.t6 & ~NPS.t15;
% realiza a correção, retornando também o desvio padrão
[Rc.NPS_mean,Rc.NPS_u] = utf_airborne_ins_BN_correction(R.p,BN.p,...
    NPS.t6_15);
%% cálculo de R (aqui RR)
% cálculo de cada RR
RR.dB = E.NPS_mean - Rc.NPS_mean + 10*log10(k*...
    repmat(TR.mean,1,1,size(TR.mean,3)));
% incerteza combinada de cada R (k está sendo declarado com incerteza 0)
RR.dB_u = sqrt(E.NPS_u.^2 + Rc.NPS_u.^2 + ...
    repmat( ( 10./(log(10).*TR.mean) ).^2 .* ...
            ( TR.std / sqrt( size(TR.std,1) ) ).^2,...
              1,1,size(E.NPS_u,3) ));
%% cálculo final       
% passar os valores de RR para pressão e os desvios padrão para pressão
RR.p = 10.^(-RR.dB/10);
RR.p_u = sqrt((-log(10)*10.^(-RR.dB/10 - 1)).^2 .* RR.dB_u.^2);
% cálculo final de isolamento (R) segundo a ISO 10140 (aqui Riso)
Riso.dB = -10*log10(mean(RR.p,3));
Riso.dB_u = sqrt( ( -10./(log(10)*mean(RR.p,3)) ).^2 .* ...
                     mean(RR.p_u,3).^2 );
% cálculo final de isolamento (R) com o método da melhor aproximação (Rma)
Rma.p = sum(1./RR.p_u.^2 .* RR.p,3) ./ sum(1./RR.p_u.^2,3);
Rma.p_u = sqrt(1./sum(1./RR.p_u.^2,3));
% passar R(ma) e o desvio padrão de R(ma) para dB
% o uso correto dos quadrados e raiz tornou-se necessário, devido ao sinal
% negativo da derivada. Pegar o valor absoluto seria equivalente, ou
% remover o sinal, já que está sendo calculado erro.
Rma.dB = -10*log10(Rma.p);
Rma.dB_u = sqrt((-10./(log(10)*Rma.p)).^2 .* Rma.p_u.^2);
end