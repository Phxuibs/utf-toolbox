%% BK_ITA_analysis %%

% análise de isolamento com planilha BK para NPS e ITA para TR

%% setup
% código da medição (e nome da pasta)
code = "02_OSB25_2019_05_06";
% faixa de frequência de interesse (limites devem ser compatíveis com a
% planilha)
freq_range = [100 3150];
% temperatura durante a medição
temp = 20;
% setup de plot
plot_setup.filename = code;
plot_setup.plot_size = utf_plot_size("luis_desktop_large");
plot_setup.title = "OSB 25mm";
%% calcular constante k
% carregar dados da câmara
load("câmara.mat");
% calcular constante k
k = utf_airborne_ins_k_constant(V,dim,S,temp);
%% dados de NPS
% prefixo da planilha
prefix = "[NPS] - ";
% extensão da planilha
ext = ".xls";
% pegar dados da planilha na pasta 'code'
cd(code);
BN.NPS = utf_LZeq_BK(prefix + code + ext,'bn','',1,3,freq_range);
E.NPS = utf_LZeq_BK(prefix + code + ext,'e','f',2,5,freq_range);
R.NPS = utf_LZeq_BK(prefix + code + ext,'r','f',2,5,freq_range);
cd('../');
clear prefix ext
%% TR
cd(code + "/TR");
% cálculo de TR cálculo da média e desvio padrão de TR
% Caveat: o primeiro comando trava a execução quando em "Run Section",
% mas funciona normalmente quando em "Run" ou por comando;
TR.data = utf_TR_ITA("",[nan 3.1],freq_range);
TR.mean = mean(TR.data);
TR.std = std(TR.data);
cd('../../');
%% cálculo de isolamento
[Rma,Riso,NPS,BN,E,R,RR,Rc] = utf_airborne_ins_main(BN,E,R,TR,k);
%% salvar e plotar resultados
freq_vector = utf_center_third_octave_bands(freq_range);
cd(code)
% .mat
save(code+".mat",'freq_vector','Rma','Riso','NPS','BN','E','R','RR','Rc',...
    'TR');
% .xls
results = [{'freq'}        num2cell(freq_vector);...
           {'Riso.dB'}     num2cell(Riso.dB);...
           {'Riso.dB_std'} num2cell(Riso.dB_std);...
           {'Rma.dB'}      num2cell(Rma.dB);...
           {'Rma.dB_std'}  num2cell(Rma.dB_std);...
           {'TR.mean'}     num2cell(TR.mean);...
           {'TR.std'}      num2cell(TR.std)];
writecell(results,code + ".xls");
% plot
utf_airborne_ins_plot(plot_setup,freq_range,Rma,TR.mean,NPS,V);