%% utf_airborne_ins_k_constant %%

% cálculo da constante k, que acompanha TR na equação de DP

% Autor: Luis Henrique Sant'Ana

% input:
% V = volume da sala receptora, em emtros cúbicos [m^(3)]; se desejado,
% pode-se passar como vetor vazio (V = []) para cálculo a partir de dim;
% dim = triplet com as dimensões da sala para cálculo do volume, se V for
% vetor vazio; exemplo: dim = [4.8 3.97 2.71];
% S = área superficial da abertura, em metros quadrados [m^(2)]. exemplo:
% S = 9.66;
% temp = temperatura, em graus Celsius [°C]

% output:
% k = constante k

function [k] = utf_airborne_ins_k_constant(V,dim,S,temp)
    if(isempty(V))
        V = dim(1)*dim(2)*dim(3);
    end
    % velocidade do som
    c = utf_speed_of_sound(temp,[]);
    % constante k
    k = S * c / 55.2552 / V;
end