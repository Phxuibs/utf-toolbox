%% utf_airborne_ins_validation %%

% verifica possíveis problemas nos dados passados pelo usuário

% Autor: Luis Henrique Sant'Ana

% input:
% E = struct com objeto 'NPS' com dados de NPS da medição da sala emissora;
% R = struct com objeto 'NPS' com dados de NPS da medição da sala
% receptora;
% TR = tempos de reverberação

function [] = utf_airborne_ins_validation(E,R,TR)
    
    % verifica compatibilidade entre as medições da sala emissora e
    % receptora
    if(~isequal(size(E.NPS),size(R.NPS)))
        error("Quantidade de medições em emissora e receptora não "+...
              "conferem!");
    end
    
    % verifica compatibilidade entre as medições da sala emissora e de
    % tempo de reverberação
    if(~isequal(size(E.NPS,2),size(TR,2)))
        error("Faixa de frequência de medições de NPS e de TR não "+...
              "correspondem!");
    end
    
end