%% utf_ISO_2631 %%
% Gráficos relacionados à ISO 2631 (1978)
% input:
% eixo = "longitudinal", "transversal" ou "transverso" (string)
% name-value pairs:
% freq = limites de frequência de interesse (e.g. [50 80] para faixa de
% 50Hz a 80Hz, ou [50 50] para apenas 50Hz (tuplet; double)
% exposureTime = tempo de exposição (array; string). Valores possíveis:
% "24h", "16h", "8h", "4h", "2.5h", "1h", "25min", "16min", "1min"
% plot = se é desejado plot (booleano)
% flagLegenda = se devem ser apresentados os valores de tempo de exposição
% ao longo das curvas (a apresentação é predeterminada e não configurável);
% necessário se plot for verdadeiro.
% output:
% Y = tabela com valores RMS máximos de acordo com frequência (primeira
% coluna) e tempos de exposição (demais colunas) escolhidos
function Y = utf_ISO_2631(eixo,varargin)
%% parsing
defaultFreq = [1 80];
defaultExposureTime = ["24h", "16h", "8h", "4h", "2.5h", "1h", "25min", "16min", "1min"];
defaultPlot = false;
defaultFlagLegenda = false;

p = inputParser;
addRequired(p,'eixo',@(x) isscalar(x) && isstring(x));
addParameter(p,'freq',defaultFreq,@(x) isnumeric(x) && isvector(x) && length(x)==2);
addParameter(p,'exposureTime',defaultExposureTime,@(x) isstring(x) && isvector(x));
addParameter(p,'plot',defaultPlot,@(x) islogical(x));
addParameter(p,'flagLegenda',defaultFlagLegenda,@(x) islogical(x));
parse(p,eixo,varargin{:});

freq = p.Results.freq;
exposureTime = p.Results.exposureTime;
plot = p.Results.plot;
flagLegenda = p.Results.flagLegenda;
%% Lê valores de interesse da tabela
% Selelciona planilha de acordo com o eixo de interesse
if(eixo == "longitudinal")
    sheet = "Tabela 1 - longitudinal";
else
    if(sum(matches(["transversal","transverso"],eixo)))
        sheet = "Tabela 2 - transversa";
    end
end
% Carrega em forma de tabela
T = readtable("ISO_2631.xlsx","Sheet",sheet);
freqIdx(1) = find(T.frequency==freq(1));
freqIdx(2) = find(T.frequency==freq(2));
exposureTimeIdx = matches(T.Properties.VariableDescriptions, exposureTime);
exposureTimeIdx(1) = true; % adiciona a coluna de frequências
Y = T(freqIdx(1):freqIdx(2),exposureTimeIdx);
%% Plot, se desejado
if(plot)
    utf_ISO_2631_plots(Y,eixo,exposureTimeIdx(2:end),flagLegenda);
end
end