%% utf_ISO_2631_plots %%
% Gera figura de acordo com Tabela 1 (longitudinal) e Tabela 2 (transversa)
% da norma ISO 2631 (semelhante às figuras 2a e 3a da norma).
% input:
% T = tabela com valores RMS máximos de acordo com frequência (primeira
% coluna) e tempos de exposição (demais colunas)
% eixo = "longitudinal", "transversal" ou "transverso" (string)
% idx = índices de tempo de exposição selecionados (ver utf_ISO_2631)
% flagLegenda = se devem ser apresentados os valores de tempo de exposição
% ao longo das curvas (a apresentação é predeterminada e não configurável)
function utf_ISO_2631_plots(T,eixo,idx,flagLegenda)
h = figure();
hold on
for i = 1 : width(T)-1
    plot(T.frequency,T{:,i+1},'-.','linewidth',1.5,'color',[0 0 0]);
end
hold off
ax = utf_axes(gca,32);
ax.YScale = 'log';
ax.YLim = [0.09 10];
ax.YTick = [0.1 0.5 1 5 10];
ax.XScale = 'log';
ax.XLim = [T.frequency(1) T.frequency(end)];
ax.XTick = T.frequency;
ax.XTickLabel = T.frequency;
ax.XTickLabelRotation = 45;
ylabel("Acelera\c{c}\~{a}o (m/s$^2$)");
xlabel("Frequ\^{e}ncia (Hz)");
legenda = ["24h","16h","8h","4h","2,5h","1h","25min","16min","1min"];
legenda = legenda(idx);
if(flagLegenda)
    if(eixo=="longitudinal")
        xt = repmat(5.1,[width(T)-1 1]);
        yt = 1.14*T{8,2:end};
        if(sum(matches(legenda,"25min")))
            xt(end-2) = 6.3;
            yt(end-2) = 0.8*yt(matches(legenda,"25min"));
        end
        text(xt,yt,legenda,'fontsize',16);
    else
        if(sum(matches(["transversal","transverso"],eixo)))
            xt = repmat(1.3,[width(T)-1 1]);
            yt = 1.14*T{3,2:end};
            if(sum(matches(legenda,"25min")))
                xt(end-2) = 1.6;
                yt(end-2) = 0.8*yt(matches(legenda,"25min"));
            end
            text(xt,yt,legenda,'FontSize',16);
        end
    end
end
yyaxis right
ax2 = utf_axes(gca,32);
ax2.YLim = [19.0849 60];
ax2.YTick = [20 40 60];
ylabel("Acelera\c{c}\~{a}o (dB)");
ax2.YColor = [0 0 0];
ax = utf_minor_grid(ax);
ax.XMinorTick = 'off';
ax.XMinorGrid = 'off';
ax.YMinorTick = 'off';
savefig("ISO_2631.fig");
end
%% Figura de acordo com Tabela 3
% clear
% close all
% clc
% T = readtable("Tabelas.xlsx","Sheet","Tabela 3 - ponderação");
% h = figure(1);
% hold on
% p(1) = plot(T.frequency,T.longitudinal_dB*-1,...
%             '-','linewidth',2,'color',utf_color("vermelho"));
% p(2) = plot(T.frequency,T.transversa_dB*-1,...
%             '--','linewidth',2,'color',utf_color("azul"));
% plot(T.frequency,20*log10(T.transversa_m_s2)*-1);
% plot(T.frequency,20*log10(T.longitudinal_m_s2)*-1);
% hold off
% ax = utf_axes(gca,32);
% ax.XScale = 'log';
% ax.XLim = [T.frequency(1) T.frequency(end)];
% ax.XTick = T.frequency;
% ax.XTickLabel = T.frequency;
% ax = utf_minor_grid(ax);
% ax.XMinorTick = 'off';
% ax.XMinorGrid = 'off';
% ax.XTickLabelRotation = 45;
% ylabel("Limite (dB)");
% xlabel("Frequ\^{e}ncia (Hz)");
% L = legend(p,["Longitudinal","Transversa"],...
%            'Location','northwest','interpreter','latex');